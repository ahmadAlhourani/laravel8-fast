<?php

use Modules\AccountManager\Http\Controllers\API\AuthlessController;
use Modules\AccountManager\Http\Controllers\API\GuestController;
use Modules\AccountManager\Http\Controllers\API\LoginController;
use Modules\AccountManager\Http\Controllers\API\ProfileControler;
use Modules\AccountManager\Http\Controllers\API\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('account-manager')->group(function() {

    Route::post('/guest/login',[AuthlessController::class,'guestLogin']);
    Route::group(['middleware' => ['auth:api','api_accessible']], function() {
        Route::post('/email/exist',[GuestController::class,'checkEmailExist']);
        Route::post('/email/register',[RegisterController::class,'registerEmail']);



        //phone
        Route::post('/phone/exist',[GuestController::class,'checkPhoneExist']);
        Route::post('/phone/register',[RegisterController::class,'registerPhone']);
        Route::post('/phone/login',[LoginController::class,'loginPhone']);


        //profile
        Route::get('/my_profile',[ProfileControler::class,'myProfileInfo']);
        Route::post('/user/update_lang',[GuestController::class,'updateLang']);


    });
});
