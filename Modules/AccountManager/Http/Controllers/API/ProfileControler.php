<?php

namespace Modules\AccountManager\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Http\Controllers\AppBaseController;

use App\Models\Directory\Alert;
use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Modules\AccountManager\Repositories\DeviceRepository;
use App\Models\Directory\Log;


class ProfileControler extends AppBaseController
{

    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(DeviceRepository $deviceRepository, RoleRepository $roleRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->userRepository = $userRepo;
        $this->deviceRepository = $deviceRepository;
    }


    /**
     * @OA\Get (
     *     path="/account-manager/my_profile",
     *     summary="get profile info ",
     *     tags={"User Profile"},
     *     security={{"Bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="profile info",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Invalid profile",
     *      @OA\JsonContent()
     *     )
     * )
     */

    public function myProfileInfo(Request $request)
    {
        try {

            $user = Auth::user();
            $user = $this->userRepository->find($user->id, ['*'], ["lang", "country"]);

//            $user=User::with("lang")->find($user->id);
            $user->setAppends([]);;
//            $user->roles=null;
//            $user->role_data=null;
//            unset($user["roles"]);
//            unset($user->id);
            return $this->sendTrue($user);

        } catch (\Exception $ex) {
            return $this->serverError($ex);
        }
    }

}
