<?php

namespace Modules\AccountManager\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Role;
use App\Models\User;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\AccountManager\Repositories\DeviceRepository;


class AuthlessController extends AppBaseController
{
    /**
     * @OA\Info(title="Services API", version="0.1")
     * Services API.
     *      *@OA\Server(
     *         description="Local",
     *         url="/api/",
     *     )
     */
    /**
     * @SWG\SecurityScheme(
     *          securityDefinition="default",
     *          type="apiKey",
     *          in="header",
     *          name="Authorization"
     *      )
     **/

    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(DeviceRepository $deviceRepository, RoleRepository $roleRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->userRepository = $userRepo;
        $this->deviceRepository = $deviceRepository;
    }


    /**
     * @OA\Post(
     *     path="/account-manager/guest/login",
     *     summary=" - Login as a guest",
     *     tags={"Guest User"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                          "device_token":"XxXx-XxXx-XxXx",
     *                          "device_os":"Android",
     *                          "device_os_version":"9.2",
     *                          "brand":"OnePlus",
     *                          "type":"phone",
     *                          "model":"7pro",
     *                          "app_version":"15.7.9",
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function guestLogin(Request $request)
    {
        try {
            $data = $request->json()->all();
            $validator = Validator::make($data, [
                'device_token' => 'required'
            ]);
            $deviceId = $this->deviceRepository->addDevice($data);

            $guestUser = $this->userRepository->assignGuestUserToDevice($deviceId);
            if ($guestUser) {

                $roleGuestAPI = $this->roleRepository->getRoleBYName(Role::GUEST_API);
                $guestUser->assignRole($roleGuestAPI);

//                if(auth()->attempt($login_credentials)){
//                    //generate the token for the user
//                    $user_login_token= auth()->user()->createToken('PassportExample@Section.io')->accessToken;
//                    //now return this token on success login attempt
//                    return response()->json(['token' => $user_login_token], 200);
//                }
                $accessToken = $guestUser->createToken('authToken')->accessToken;
//                $accessToken = $guestUser->createToken('PassportExample')->accessToken;
                //generate guest token

                $data = [];
                $filename='services/images/iT4y10u0CgZzYQECOdHyx6YZG7HKyBgavrTNMpmv.jpg';
                $data["access_token"] = $accessToken;
                $data["user_id"] = $guestUser->id;

                return $this->sendResponse($data, "Success");
            }

            return $this->sendError("Error", 404);


        } catch (\Exception $ex) {
            return $this->sendError($ex->getMessage(), 500);
        }
    }

}
