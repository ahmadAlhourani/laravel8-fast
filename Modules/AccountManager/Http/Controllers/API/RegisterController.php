<?php

namespace Modules\AccountManager\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Http\Controllers\AppBaseController;

use App\Models\Directory\Alert;
use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Modules\AccountManager\Repositories\DeviceRepository;
use App\Models\Directory\Log;


class RegisterController extends AppBaseController
{

    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(DeviceRepository $deviceRepository, RoleRepository $roleRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->userRepository = $userRepo;
        $this->deviceRepository = $deviceRepository;
    }


    /**
     * @OA\Post(
     *     path="/account-manager/email/register",
     *     summary=" register email in system ",
     *     tags={"User Email"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 example={
     *                          "email":"test@shaadoow.com",
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="",
     *      @OA\JsonContent()
     *          )
     * )
     */
    public function registerEmail(Request $request)
    {
        try {
            $user = Auth::user();
            $user->checkUserPermission();
            $data = $request->json()->all();
            $validator = Validator::make($data, [
                'email' => 'email|required|unique:users'
            ]);

            $res = [];
            if ($validator->passes()) {
                $res["status"] = "signup";
                //send otp to email


                return $this->sendTrue($res, "the email is an available");
            }
            //email is founded before
            $res["status"] = "login";
            return $this->sendTrue($res, "the email is not an available");

        } catch (\Exception $ex) {
            return $this->serverError($ex);
        }
    }


    /**
     * @OA\Post(
     *     path="/account-manager/phone/register",
     *     summary="Add a new user by phone Note gender: 1:male, 2:female, 3:other",
     *     tags={"User Phone"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                required={"firebase_token","name","password","password_confirmation"},
     *                 @OA\Property(
     *                     property="firebase_token",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="lang_id",
     *                     type="integer"
     *                 ),
     *                @OA\Property(
     *                     property="country_id",
     *                     type="integer"
     *                 ),
     *              @OA\Property(
     *                     property="gender",
     *                     type="integer"
     *                 ),
     *                @OA\Property(
     *                     property="birthday",
     *                     type="date"
     *                 ),
     *                 example={
     *                          "firebase_token":"eyJhbGciOiJSUzI1NiIsImtpZCI6ImYwNTM4MmFlMTgxYWJlNjFiOTYwYjA1Yzk3ZmE0MDljNDdhNDQ0ZTciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vc2VydmljZXMtYTlkZGQiLCJhdWQiOiJzZXJ2aWNlcy1hOWRkZCIsImF1dGhfdGltZSI6MTYzNDI0MzcxOCwidXNlcl9pZCI6ImpWbTZsNUtHNFpZRUdvYUp2dlZPRnhraVZzaDEiLCJzdWIiOiJqVm02bDVLRzRaWUVHb2FKdnZWT0Z4a2lWc2gxIiwiaWF0IjoxNjM0MjQzNzE5LCJleHAiOjE2MzQyNDczMTksInBob25lX251bWJlciI6Iis5NzE1NTY4NjAxMjkiLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7InBob25lIjpbIis5NzE1NTY4NjAxMjkiXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwaG9uZSJ9fQ.BbdsodZlRe9QXAMWNCuf8JhohE4zA8IYppCOK9nS1uf7ys7ooLP4T9VhVAduM_a4AZs7ipHT5apoTjPUMUIaV3epIsOm38-d3xDnsDTmbug4lTkIHKl9c3T6-3lQu7wD2x6m4LlGeBD5xu5qJuq_FrhPsbXmxNZ_oz_eHNY-4M7W6T-CiJMcvYd5MpRUPwUhl6jHJ-7MGf156LRqYfZrp_o_tlC2DHwEx7G1-PjVsWiBS2Y1cO39kE9sN4g3zabUsVEY9kgW5ZlBPZDWS8K3z69YybDmDcZg6T8HksFuo4oD9s5crgKZ8wY4nQQmXrGAM6LMsMLNEnVd8xisxHGYig",
     *                          "name":"ahmad",
     *                          "password":"1234",
     *                          "password_confirmation":"1234",
     *                          "gender":1,
     *                          "lang_id":1,
     *                          "country_id":"1",
     *                          "birthday":"1988-03-19"
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *          )
     * )
     */
    public function registerPhone(Request $request)
    {
        try {

            $user = Auth::user();
            $user->checkUserPermission();
            $data = $request->json()->all();
//            $user1=$this->userRepository->find($user->id);
//            return $this->sendTrue($user->test());
            $validator = Validator::make($data, [
                'firebase_token' => 'required',
                'name' => 'required',
                'password' => 'required|confirmed',
            ]);


            if ($validator->passes()) {

                $firebaseId = FirebaseHelper::validateFirebaseToken($request->firebase_token);
                if ($firebaseId === 0) {
                    $phoneNum = "+971554430743";
                    //general Error
//                    $msg=$this->msg($user->defaultLang(),1);
//                    return $this->sendFalse([],$msg);
                } else {
                    $firebaseData = FirebaseHelper::getTokenData($request->firebase_token);
                    $phoneNum = $firebaseData->phoneNumber;
                }
                // to remove + and anything not number

                $phoneNum = preg_replace("/[^0-9.]/", "", $phoneNum);

                //validate phone
                if ($this->userRepository->allQuery(["phone" => $phoneNum])->count()) {
                    //Duplicate phone number
                    $msg = $this->msg($user->defaultLang(), 2);
                    return $this->sendFalse([], $msg);
                }

                $data['phone'] = $phoneNum;
                $data['phone_verified_at'] = Carbon::now();
                if (!isset($data['lang_id'])) {
                    $data['lang_id'] = $user->defaultLang();
                }
                $data['status'] = User::LOGGED_USER;
                $data['type'] = User::PHONE_TYPE;
                $data['verified'] = User::VERIFIED;

                if (!isset($data['gender']) or $data['gender'] == NULL or !in_array($data['gender'], [0, 1, 2, 3])) {
                    $data['gender'] = 0;
                }

                $registeredUser = $this->userRepository->create($data);
//                return $this->sendTrue($registeredUser);
//                return $this->sendTrue($registeredUser);
                $user->cloneTo($registeredUser);


                //assign logged api Role
                $loggedUserAPIRole = $this->roleRepository->getRoleBYName(Role::LOGGED_USER_API);
                $registeredUser->assignRole($loggedUserAPIRole);

                $accessToken = $registeredUser->createToken('authToken')->accessToken;
                $data = [];
                $data["access_token"] = $accessToken;
                $data["user_id"] = $registeredUser->id;


                return $this->sendTrue($data);
            }

            return $this->invalidInput($validator->errors());


        } catch (\Exception $ex) {
            return $this->serverError($ex);
        }
    }

}
