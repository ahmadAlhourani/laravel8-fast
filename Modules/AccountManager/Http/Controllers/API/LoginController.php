<?php

namespace Modules\AccountManager\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Http\Controllers\AppBaseController;

use App\Models\Directory\Alert;
use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Modules\AccountManager\Repositories\DeviceRepository;
use App\Models\Directory\Log;


class LoginController extends AppBaseController
{

    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(DeviceRepository $deviceRepository, RoleRepository $roleRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->userRepository = $userRepo;
        $this->deviceRepository = $deviceRepository;
    }


    /**
     * @OA\Post(
     *     path="/account-manager/phone/login",
     *     summary="login by phone && password",
     *     tags={"User Phone"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={
     *                          "phone":"971554430743",
     *                          "password":"1234"
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Logged in Successfully",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid info",
     *      @OA\JsonContent()
     *     )
     * )
     */

    public function loginPhone(Request $request)
    {
        try {

            $user = Auth::user();
            $data = $request->json()->all();

            $validator = Validator::make($data, [
                'phone' => 'required',
                'password' => 'required',
            ]);


            if ($validator->passes()) {
                $phoneNum = preg_replace("/[^0-9.]/", "", $request->phone);
                $loggedUser = $this->userRepository->allQuery(["phone" => $phoneNum])->first();
//                return $this->sendFalse($loggedUser);
                if (!$loggedUser) {
                    //Invalid Phone Number
                    $msg = $this->msg($user->defaultLang(), 3);
                    return $this->sendFalse([], $msg);
                }
                // check if the user is suspended
                if ($loggedUser->active == 0) {
                    //general Error
                    $msg = $this->msg($user->defaultLang(), 1);
                    return $this->sendFalse([], $msg);
                }

                if (Hash::check($request->password, $loggedUser->password, [])) {

//                    if (!$loggedUser->loginUsingId($loggedUser->id)) {
//                        //general Error
//                        $msg=$this->msg($user->defaultLang(),1);
//                        return $this->sendFalse([],$msg);
//                    }
                    // check verification status to send the code
                    $accessToken = $loggedUser->createToken('authToken')->accessToken;
                    $data = [];
                    $data["access_token"] = $accessToken;
                    $data["user_id"] = $loggedUser->id;
                    return $this->sendTrue($data);
                }


                //Invalid Password
                $msg = $this->msg($user->defaultLang(), 4);
                return $this->sendFalse([], $msg);
            }

            return $this->sendFalse($validator->errors());

        } catch (\Exception $ex) {
            return $this->serverError($ex);
        }
    }

}
