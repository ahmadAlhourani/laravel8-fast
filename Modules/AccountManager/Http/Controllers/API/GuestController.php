<?php

namespace Modules\AccountManager\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\AccountManager\Repositories\DeviceRepository;
use App\Models\Directory\Log;


class GuestController extends AppBaseController
{

    /** @var  DeviceRepository */
    private $deviceRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(DeviceRepository $deviceRepository, RoleRepository $roleRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->userRepository = $userRepo;
        $this->deviceRepository = $deviceRepository;
    }


    /**
     * @OA\Post(
     *     path="/account-manager/email/exist",
     *     summary=" Check the availablity of the email",
     *     tags={"User Email"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 example={
     *                          "email":"test@shaadoow.com",
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="available",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="unavailable",
     *      @OA\JsonContent()
     *          )
     * )
     */
    public function checkEmailExist(Request $request)
    {
        try {
            $user = Auth::user();
            $user->checkUserPermission();
            $data = $request->json()->all();
            $validator = Validator::make($data, [
                'email' => 'email|required|unique:users'
            ]);

            $res = [];
            if ($validator->passes()) {
                $res["exist"] = false;
                $res["status"] = "signup";

                return $this->sendTrue($res, "the email is an available");
            }
            $res["exist"] = true;
            $res["status"] = "login";

            return $this->sendTrue($res, "the email is not an available");

        } catch (\Exception $ex) {
            return $this->serverError($ex);
        }
    }


    /**
     * @OA\Post(
     *     path="/account-manager/phone/exist",
     *     summary=" Check the availablity of the phone",
     *     tags={"User Phone"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 example={
     *                          "phone":"971503450332",
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="available",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="unavailable",
     *      @OA\JsonContent()
     *          )
     * )
     */
    public function checkPhoneExist(Request $request)
    {
        try {
            $user = Auth::user();
            $user->checkUserPermission();
            $data = $request->json()->all();
            $validator = Validator::make($data, [
                'phone' => 'string|required|unique:users'
            ]);

            $res = [];
            if ($validator->passes()) {
                $res["exist"] = false;
                $res["status"] = "signup";


                return $this->sendTrue($res, "the phone is an available");
            }
            $res["exist"] = true;
            $res["status"] = "login";
            return $this->sendTrue($res, "the phone is not an available");

        } catch (\Exception $ex) {
            return $this->serverError($ex);

        }
    }

    /**
     * @OA\Post(
     *     path="/account-manager/user/update_lang",
     *     summary="update User Lang ",
     *     tags={"User Profile"},
     *     security={{"Bearer":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                 @OA\Property(
     *                     property="lang_id",
     *                     type="integer"
     *                 ),
     *                 ),
     *                 example={
     *                          "lang_id":"1",
     *                          }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="available",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="unavailable",
     *      @OA\JsonContent()
     *          )
     * )
     */
    public function updateLang(Request $request)
    {
        try {

            $user = Auth::user();
            $data = $request->json()->all();
            $validator = Validator::make($data, [
                'lang_id' => 'required'
            ]);

            if ($validator->passes()) {
                $this->userRepository->update(["lang_id" => $request->lang_id], $user->id);

                return $this->sendTrue();
            }
            return $this->invalidInput($validator->errors());

        } catch (\Exception $ex) {
            return $this->serverError($ex);

        }
    }

}
