<?php

namespace Modules\AccountManager\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserVerification extends Model
{
    use HasFactory;
    protected $table="user_verification";

    protected $guarded = [];

    //types
    public const   EMAIL_TYPE = 1;
    public const   PHONE_TYPE = 2;


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
