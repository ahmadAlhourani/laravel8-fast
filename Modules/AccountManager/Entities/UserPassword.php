<?php

namespace Modules\AccountManager\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserPassword extends Model
{
    use HasFactory;
//    protected $table="user_password";
    protected $hidden = [
        'password',
    ];
    protected $guarded = [];


}
