<?php

namespace Modules\AccountManager\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Directory\Entities\Log;
use Modules\Profile\Entities\Profile;

class Device extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }


}
