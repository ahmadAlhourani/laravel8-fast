<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVerificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verification', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('type')->default(1)
                ->comment("1=>email,2:phone");
            $table->string('user_logged_method')->default(1)->comment("user_logged_method will be email /phone");
            $table->string('code');
            $table->tinyInteger('status')->default(0)
                ->comment("0=>not verified,1: verified");
            $table->tinyInteger('active')->default(1)
                ->comment("0=>inactive,1: active");
            $table->timestamp('valid_to')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_verification');
    }
}
