<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('device_token');
            $table->string('device_os',63)->nullable();
            $table->string('device_os_version',63)->nullable();
            $table->string('brand',63)->nullable();
            $table->string('type',63)->nullable();
            $table->string('model',63)->nullable();
            $table->string('app_version',63)->nullable();

            $table->tinyInteger('status')->default(1)->comment("1=>active, 0=>Inactive");
//            $table->unique(["device_token"]);
            $table->index('type');
//            $table->index('device_token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
