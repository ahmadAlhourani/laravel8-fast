<?php

namespace Modules\AccountManager\Repositories;

use App\Repositories\BaseRepository;
use Modules\AccountManager\Entities\UserPassword;


/**
 * Class DeviceRepository
 * @package AccountManager\Repositories
 */

class UserPasswordRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];


    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserPassword::class;
    }
}
