<?php

namespace Modules\AccountManager\Repositories;

use App\Repositories\BaseRepository;
use Modules\AccountManager\Entities\UserDevice;


/**
 * Class DeviceRepository
 * @package AccountManager\Repositories
 */

class UserDeviceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];


    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserDevice::class;
    }

}
