<?php

namespace Modules\AccountManager\Repositories;

use App\Repositories\BaseRepository;
use Modules\AccountManager\Entities\Device;


/**
 * Class DeviceRepository
 * @package AccountManager\Repositories
 */

class DeviceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Device::class;
    }


    /**
     * @param array $data
     * @return int
     */
    public  function addDevice(array $data = []):int
    {
        try {
            $device = $this->model->where('device_token', $data['device_token'])->first();


            if ($device) {
                $this->update($data,$device->id);
                return $device->id;
            }
            $device= $this->create($data);
            return $device->id;
        } catch (\Exception $ex) {
            return 0;
        }
    }
}
