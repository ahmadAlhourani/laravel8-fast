<?php

use Illuminate\Http\Request;
use Modules\Utility\Http\Controllers\API\UtilityController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('utility')->group(function() {

    Route::get('/seed',[UtilityController::class,'seed']);
    Route::get('/swagger',[UtilityController::class,'swagger']);
});
Route::prefix('utility')->group(function() {

    Route::group(['middleware' => ['auth:api','api_accessible']], function() {
        Route::get('/countries/list',[UtilityController::class,'countriesList']);
        Route::get('/langs/list',[UtilityController::class,'languagesList']);



    });
});
