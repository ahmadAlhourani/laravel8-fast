<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Modules\Utility\Http\Controllers\LogController;

Route::prefix('utility')->group(function() {
    Route::get('/', 'UtilityController@index');
//    Route::resource('logs',LogController::class);
});
//Route::resource('logs',LogController::class);

//Route::get('logs',function (){
//    var_dump("ddddd");
//});
//Route::get('logs.index',function (){
//    var_dump("ddddd");
//});
