<?php

namespace Modules\Utility\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Repositories\Directory\CountryRepository;
use App\Repositories\Directory\LangRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Modules\AccountManager\Repositories\DeviceRepository;


class UtilityController extends AppBaseController
{


    /** @var  CountryRepository */
    private $countryRepository;

    /** @var  LangRepository */
    private $langRepository;

    /** @var  UserRepository */
    private $userRepository;

    public function __construct(CountryRepository $countryRepository, LangRepository $langRepository, UserRepository $userRepo)
    {
        $this->countryRepository = $countryRepository;
        $this->userRepository = $userRepo;
        $this->langRepository = $langRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('utility::index');
    }


    /**
     * @OA\Get(
     *     path="/utility/countries/list",
     *     summary="Get countries list ",
     *     tags={"Utility Data"},
     *    security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page number",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=1
     *     ),
     *     @OA\Parameter(
     *         description="limit of the item",
     *         in="query",
     *         name="limit",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=5
     *     ),
     *       @OA\Parameter(
     *         description="search value ",
     *         in="query",
     *         name="search",
     *
     *         @OA\Schema(
     *             type="string"
     *         ),
     *     example=""
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function countriesList(Request $request)
    {
        $this->pager($request);
        $data = $this->countryRepository->search([], $this->search, $this->skip, $this->limit);
        return $this->sendTrue($data);
    }

    /**
     * @OA\Get(
     *     path="/utility/langs/list",
     *     summary="Get langs list ",
     *     tags={"Utility Data"},
     *    security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page number",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=1
     *     ),
     *     @OA\Parameter(
     *         description="limit of the item",
     *         in="query",
     *         name="limit",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=5
     *     ),
     *       @OA\Parameter(
     *         description="search value ",
     *         in="query",
     *         name="search",
     *
     *         @OA\Schema(
     *             type="string"
     *         ),
     *     example=""
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function languagesList(Request $request)
    {

        $this->pager($request);
        $data = $this->langRepository->search([], $this->search, $this->skip, $this->limit);
        return $this->sendTrue($data);
    }


    /**
     * @OA\Get(
     *     path="/utility/seed",
     *     summary="seed ",
     *     tags={"Z Comands"},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function seed(Request $request)
    {

        try {
            Artisan::call('migrate');
            Artisan::call('db:seed');
        } catch (\Exception $e) {
            return $this->sendFalse([], $e->getMessage());

        }
        return $this->sendTrue();
    }

    /**
     * @OA\Get(
     *     path="/utility/swagger",
     *     summary="swagger ",
     *     tags={"Z Comands"},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function swagger(Request $request)
    {



        Artisan::call('l5-swagger:generate');
        return $this->sendTrue();
    }


}
