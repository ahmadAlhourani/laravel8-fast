<?php

namespace Modules\ServiceManager\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Services\Service;
use App\Models\Services\ServiceCategory;
use App\Models\Services\ServiceType;
use App\Models\Services\ServiceTypeQuestion;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ServiceController extends AppBaseController
{


    /**
     * @OA\Get(
     *     path="/service-manager/{service_category_id}/services/list",
     *     summary="Get services list by category ",
     *     tags={"Service Manager"},
     *    security={{"Bearer":{}}},
     *    @OA\Parameter(
     *         description="ID of the service category",
     *         in="path",
     *         name="service_category_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="page number",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=1
     *     ),
     *     @OA\Parameter(
     *         description="limit of the item",
     *         in="query",
     *         name="limit",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=5
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */

    public function serviceList(Request $request,$categoryId)
    {
        $this->pager($request);
        if($categoryId){
            $data =Service::where("service_category_id", intval($categoryId))->where("status", 1)->orderby("rank", "desc")->take($this->limit)->skip($this->skip)->get();

        }else{
            $data =Service::where("status", 1)->orderby("rank", "desc")->take($this->limit)->skip($this->skip)->get();

        }
        return $this->sendTrue($data);
    }



    /**
     * @OA\Get(
     *     path="/service-manager/{service_id}/service",
     *     summary="Get service Details ",
     *     tags={"Service Manager"},
     *    security={{"Bearer":{}}},
     *    @OA\Parameter(
     *         description="ID of the service",
     *         in="path",
     *         name="service_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */

    public function serviceDetails(Request $request,$serviceId)
    {
//        $service =Service::with('current_service_lang')->where("id", intval($serviceId))->where("status", 1)->orderby("rank", "desc")->take($this->limit)->skip($this->skip)->get();
        $service =Service::find(intval($serviceId));
        if(!$service)
            return $this->sendFalse();

        $service->service_types=ServiceType::where("service_id", $service->id)->where("status", 1)->orderby("rank", "desc")->get();
        return $this->sendTrue($service);
    }
}
