<?php

namespace Modules\ServiceManager\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Services\Service;
use App\Models\Services\ServiceCategory;
use App\Repositories\Services\ServiceCategoryRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ServiceCategoryController extends AppBaseController
{

    /** @var  ServiceCategoryRepository */
    private $serviceCategoryRepository;

    public function __construct(ServiceCategoryRepository $serviceCategoryRepository)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepository;

    }

    /**
     * @OA\Get(
     *     path="/service-manager/category/list",
     *     summary="Get service category list ",
     *     tags={"Service Manager"},
     *    security={{"Bearer":{}}},
     *     @OA\Parameter(
     *         description="page number",
     *         in="query",
     *         name="page",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=1
     *     ),
     *     @OA\Parameter(
     *         description="limit of the item",
     *         in="query",
     *         name="limit",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         ),
     *     example=5
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *      @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Could Not Find Resource",
     *      @OA\JsonContent()
     *     )
     * )
     */
    public function serviceCategoryList(Request $request)
    {
        $this->pager($request);
        $data = ServiceCategory::where("status", 1)->orderby("rank", "desc")
            ->take($this->limit)->skip($this->skip)->get();

        foreach ($data as $item) {
            $item->services = Service::where("status", 1)->orderby("rank", "desc")->take(5)->get();
        }
        return $this->sendTrue($data);
    }

}
