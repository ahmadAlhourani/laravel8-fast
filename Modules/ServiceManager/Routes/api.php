<?php

use Illuminate\Http\Request;
use Modules\ServiceManager\Http\Controllers\API\ServiceCategoryController;
use Modules\ServiceManager\Http\Controllers\API\ServiceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('service-manager')->group(function() {

    Route::group(['middleware' => ['auth:api','api_accessible']], function() {
        Route::get('/category/list',[ServiceCategoryController::class,'serviceCategoryList']);

        Route::get('/{service_category_id}/services/list',[ServiceController::class,'serviceList']);

        Route::get('/{service_id}/service',[ServiceController::class,'serviceDetails']);




    });
});
