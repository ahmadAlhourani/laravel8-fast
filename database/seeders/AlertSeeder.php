<?php

namespace Database\Seeders;

use App\Models\Directory\Lang;
use App\Repositories\Directory\AlertRepository;
use App\Repositories\Directory\LangRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;


class AlertSeeder extends Seeder
{
    /** @var  LangRepository */
    private $langRepository;

    /** @var  AlertRepository */
    private $alertRepository;

    public function __construct(LangRepository $langRepository,AlertRepository $alertRepository)
    {
        $this->langRepository = $langRepository;
        $this->alertRepository = $alertRepository;

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $allLangs=$this->langRepository->all();

        $alerts = [];

        //1
        $alert=[];
        $alert['state'] = 1;
        $alert['module'] = "General";
        $alert['name'] = "general error";


        $alertLangs=[];
        foreach ($allLangs as $lang){
            $alertLang=[];
            $alertLang["lang_id"]=$lang->id;
            switch ($lang->iso){

                case Lang::ARABIC_IOS://Arabic Lang
                      $alertLang["name"]="حدث خطأ مؤقت في الخادم, سيتم اصلاحه قريبا";
                      $alertLangs[]=$alertLang;
                    break;
                case  Lang::ENGLISH_IOS://English Lang
                    $alertLang["name"]="A temporary server error has occurred, it will be fixed soon";
                    $alertLangs[]=$alertLang;
                    break;
            }
        }

        $alert["alert_langs"]=$alertLangs;
        $alerts[]=$alert;

        //2
        $alert=[];
        $alert['state'] = 2;
        $alert['module'] = "General";
        $alert['name'] = "Duplicate phone number";


        $alertLangs=[];
        foreach ($allLangs as $lang){
            $alertLang=[];
            $alertLang["lang_id"]=$lang->id;
            switch ($lang->iso){

                case Lang::ARABIC_IOS://Arabic Lang
                    $alertLang["name"]="رقم الهاتف مكرر";
                    $alertLangs[]=$alertLang;
                    break;
                case  Lang::ENGLISH_IOS://English Lang
                    $alertLang["name"]="Duplicate phone number";
                    $alertLangs[]=$alertLang;
                    break;
            }
        }

        $alert["alert_langs"]=$alertLangs;
        $alerts[]=$alert;

        //3
        $alert=[];
        $alert['state'] = 3;
        $alert['module'] = "General";
        $alert['name'] = "Invalid Phone Number";


        $alertLangs=[];
        foreach ($allLangs as $lang){
            $alertLang=[];
            $alertLang["lang_id"]=$lang->id;
            switch ($lang->iso){

                case Lang::ARABIC_IOS://Arabic Lang
                    $alertLang["name"]="رقم الهاتف غير صحيح";
                    $alertLangs[]=$alertLang;
                    break;
                case  Lang::ENGLISH_IOS://English Lang
                    $alertLang["name"]="Invalid Phone Number";
                    $alertLangs[]=$alertLang;
                    break;
            }
        }

        $alert["alert_langs"]=$alertLangs;
        $alerts[]=$alert;


        //4
        $alert=[];
        $alert['state'] = 4;
        $alert['module'] = "Account";
        $alert['name'] = "Invalid Password";


        $alertLangs=[];
        foreach ($allLangs as $lang){
            $alertLang=[];
            $alertLang["lang_id"]=$lang->id;
            switch ($lang->iso){

                case Lang::ARABIC_IOS://Arabic Lang
                    $alertLang["name"]="كلمة المرور غير صحيحة";
                    $alertLangs[]=$alertLang;
                    break;
                case  Lang::ENGLISH_IOS://English Lang
                    $alertLang["name"]="Invalid Password";
                    $alertLangs[]=$alertLang;
                    break;
            }
        }

        $alert["alert_langs"]=$alertLangs;
        $alerts[]=$alert;

        //5
        $alert=[];
        $alert['state'] = 5;
        $alert['module'] = "General Error";
        $alert['name'] = "Invalid Input";


        $alertLangs=[];
        foreach ($allLangs as $lang){
            $alertLang=[];
            $alertLang["lang_id"]=$lang->id;
            switch ($lang->iso){

                case Lang::ARABIC_IOS://Arabic Lang
                    $alertLang["name"]="خطأ في المدخلات";
                    $alertLangs[]=$alertLang;
                    break;
                case  Lang::ENGLISH_IOS://English Lang
                    $alertLang["name"]="Invalid Input";
                    $alertLangs[]=$alertLang;
                    break;
            }
        }

        $alert["alert_langs"]=$alertLangs;
        $alerts[]=$alert;


        foreach ($alerts as $alert){
            try{
                DB::transaction(function() use ($alert)  {
                    $this->alertRepository->create($alert);
                });

            }
            catch (\Illuminate\Database\QueryException $e) {
                $this->command->error("SQL Error: " . $e->getMessage() . "\n");

            }

        }


    }
}
