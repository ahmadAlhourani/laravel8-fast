<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(LangSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);

        try {
            Artisan::call('passport:install');
        } catch (\Exception $e) {

        }
    }
}
