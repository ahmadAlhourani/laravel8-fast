<?php

namespace Database\Seeders;

use App\Models\User;
use App\Repositories\Directory\LangRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /** @var  PermissionRepository */
    private $permissionRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;
    /** @var  UserRepository */
    private $langRepository;

    public function __construct(RoleRepository $roleRepo, PermissionRepository $permissionRepo,
                                UserRepository $userRepo, LangRepository $langRepository)
    {
        $this->roleRepository = $roleRepo;
        $this->permissionRepository = $permissionRepo;
        $this->userRepository = $userRepo;
        $this->langRepository = $langRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('config:clear');
        $exitCode = Artisan::call('router:permission');

        $defaultLang = $this->langRepository->getLangByISO();
        // Supper Admin  //id =1
        try {
            $roleSupperAdmin = $this->roleRepository->create([
                'name' => Role::SUPPER_ADMIN,
                'title' => 'Supper Admin',
                'guard_name' => 'web'
            ]);

            $userSupperAdmin = $this->userRepository->create([
                'name' => 'Administrator',
                'email' => 'admin@fastservice.com',
                'password' => '123@12',
                'user_type' => User::ADMIN_USER_TYPE,
                //User types
                'lang_id' => $defaultLang->id,
            ]);
            $userSupperAdmin->assignRole($roleSupperAdmin);

            $userSupperAdmin = $this->userRepository->create([
                'name' => 'Administrator',
                'email' => 'ahmad@gmail.com',
                'password' => '1234',
                'user_type' => User::ADMIN_USER_TYPE,
                'lang_id' => $defaultLang->id,
            ]);
            $userSupperAdmin->assignRole($roleSupperAdmin);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->command->error("SQL Error: " . $e->getMessage() . "\n");

        }

        // Sub Admin
        try {
            $roleSubAdmin= $this->roleRepository->create([
                'name' => Role::SUB_ADMIN,
                'title' => 'Leader',
                'guard_name' => 'web'
            ]);
            $usersPermissions = $this->permissionRepository->getListByName('users.%');
            $roleSubAdmin->givePermissionTo($usersPermissions);
            $userSubAdmin= $this->userRepository->create([
                'name' => 'Leader',
                'email' => 'subadmin@fastservice.com',
                'password' => '123@123',
                'user_type' => User::ADMIN_USER_TYPE,
                'lang_id' => $defaultLang->id,
            ]);
            $userSubAdmin->assignRole($roleSubAdmin);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->command->error("SQL Error: " . $e->getMessage() . "\n");

        }

        // GUEST_API
        try {
            $this->roleRepository->create([
                'name' => Role::GUEST_API,
                'title' => 'GUEST API',
                'guard_name' => 'api'
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->command->error("SQL Error: " . $e->getMessage() . "\n");

        }


        // LOGGED_USER_API
        try {
           $this->roleRepository->create([
                'name' => Role::LOGGED_USER_API,
                'title' => 'LOGGED USER API API',
                'guard_name' => 'api'
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->command->error("SQL Error: " . $e->getMessage() . "\n");

        }
    }
}
