<?php

namespace Database\Seeders;

use App\Models\Directory\Alert;
use App\Models\Directory\Lang;
use App\Repositories\Directory\LangRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use PHPUnit\Exception;


class LangSeeder extends Seeder
{
    /** @var  LangRepository */
    private $langRepository;


    public function __construct(LangRepository $langRepository)
    {
        $this->langRepository = $langRepository;

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('config:clear');


        $langs = [];

        $lang=[];
        $lang['name'] = "English";
        $lang['iso'] = Lang::ENGLISH_IOS;

        $langs[]=$lang;

        $lang=[];
        $lang['name'] = "العربية";
        $lang['iso'] = Lang::ARABIC_IOS;

        $langs[]=$lang;
        foreach ($langs as $lang){
            try{
                $this->langRepository->create($lang);
            }
            catch (\Illuminate\Database\QueryException $e) {
                $this->command->error("SQL Error: " . $e->getMessage() . "\n");

            }

        }


        $this->call(AlertSeeder::class);


    }
}
