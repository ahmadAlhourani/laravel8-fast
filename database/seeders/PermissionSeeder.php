<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\UserRepository;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\This;

class PermissionSeeder extends Seeder
{
    /** @var  PermissionRepository */
    private $permissionRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(RoleRepository $roleRepo, PermissionRepository $permissionRepo, UserRepository $userRepo)
    {
        $this->roleRepository = $roleRepo;
        $this->permissionRepository = $permissionRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('config:clear');


        $permissions = [];


        //Guest Users Permissions
        $permission=[];
        $permission['name'] = "api/account-manager/email/exist";
        $permission['title'] = " check Email Exist";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;


        $permission=[];
        $permission['name'] = "api/account-manager/phone/exist";
        $permission['title'] = " check phone Exist";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;


        $permission=[];
        $permission['name'] = "api/account-manager/phone/register";
        $permission['title'] = "register by  phone ";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;

        $permission=[];
        $permission['name'] = "api/account-manager/phone/login";
        $permission['title'] = "login by  phone ";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;

        $permission=[];
        $permission['name'] = "api/account-manager/phone/email";
        $permission['title'] = "register by  email ";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;





        $permission=[];
        $permission['name'] = "api/utility/countries/list";
        $permission['title'] = "Countries List";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;

        $permission=[];
        $permission['name'] = "api/utility/langs/list";
        $permission['title'] = "Langs List";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;

        $permission=[];
        $permission['name'] = "api/account-manager/user/update_lang";
        $permission['title'] = "update User Lang";
        $permission['module'] = Permission::GUEST_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;



        //Logged Users Permissions

        $permission=[];
        $permission['name'] = "api/account-manager/my_profile";
        $permission['title'] = "my profile info  ";
        $permission['module'] = Permission::LOGGED_USER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;




        //Service Manager Permissions
        $permission=[];
        $permission['name'] = "api/service-manager/category/list";
        $permission['title'] = "Category Service List";
        $permission['module'] = Permission::SERVICE_MANAGER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;


        $permission=[];
        $permission['name'] = "api/service-manager/{service_category_id}/services/list";
        $permission['title'] = "Get services list by category ";
        $permission['module'] = Permission::SERVICE_MANAGER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;


        $permission=[];
        $permission['name'] = "api/service-manager/{service_id}/service";
        $permission['title'] = "Get service Details";
        $permission['module'] = Permission::SERVICE_MANAGER_API;
        $permission['guard_name'] = "api";
        $permissions[]=$permission;

        foreach ($permissions as $p){

            try{
                $this->permissionRepository->create($p);
            }
            catch (\Illuminate\Database\QueryException $e) {
                $this->command->error("SQL Error: " . $e->getMessage() . "\n");

            }
        }


        //assign permission

        $guestPermissionsIds= $this->permissionRepository->allQuery(['module'=> Permission::GUEST_USER_API])->pluck( 'id')->toArray();


        //add permissions to  Guest API Role
        $guestAPIRole=$this->roleRepository->getRoleBYName(Role::GUEST_API);
        $guestAPIRole->syncPermissions($guestPermissionsIds);

        //add permissions to  Logged User API Role
        $loggedPermissionsIds= $this->permissionRepository->allQuery(['module'=> Permission::LOGGED_USER_API])->pluck( 'id')->toArray();
        $serviceManagerPermissionsIds= $this->permissionRepository->allQuery(['module'=> Permission::SERVICE_MANAGER_API])->pluck( 'id')->toArray();
        $mergedLoggedPermissionsIds=array_merge($loggedPermissionsIds,$guestPermissionsIds);
        $mergedLoggedPermissionsIds=array_merge($mergedLoggedPermissionsIds,$serviceManagerPermissionsIds);

        $loggedUserAPIRole=$this->roleRepository->getRoleBYName(Role::LOGGED_USER_API);
        $loggedUserAPIRole->syncPermissions($mergedLoggedPermissionsIds);

    }
}
