<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('social_id')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->tinyInteger('status')->default(1)
                ->comment("0=> guest user,1:logged user");

            $table->tinyInteger('user_type')->default(3)
                ->comment("1=>admin,2:partner,3:normal users");

            $table->tinyInteger('type')->default(1)
                ->comment("1=>email,2:phone,3:google, 4:facebook,5:apple");




            $table->tinyInteger('verified')->default(0)//for login by social id
                ->comment("0=> Not verified,1:verified");

            $table->unsignedBigInteger('lang_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->tinyInteger('gender')->default(0)
                ->comment('0:not detected - 1:male -2:female -3:other');
            $table->date('birthday')->nullable();
            $table->text('image_url')->nullable();

            $table->tinyInteger('active')->default(1)
                ->comment("0=> Inactive,1:Active");


            $table->index('lang_id');
            $table->index('status');
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
