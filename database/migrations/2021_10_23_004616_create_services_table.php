<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('service_category_id');
            $table->text('icon')->nullable();
            $table->integer('rank');
            $table->integer('status')->default(0)->comment('0 =>inactive, 1 active');
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->longText('html_description')->nullable();
            $table->longText('question_service_type')->nullable();
            $table->longText('types_difference')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('service_category_id')->references('id')->on('service_categories')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
