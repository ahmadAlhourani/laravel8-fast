<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateIndexServiceTypeQuestionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('service_type_questions', function (Blueprint $table) {

                $table->dropUnique(['service_type_id', 'question_type_id']);
//            $table->unique(['service_type_id','question']);

            });
        } catch (\Exception $e) {

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_type_questions');
    }
}
