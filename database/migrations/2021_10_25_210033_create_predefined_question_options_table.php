<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePredefinedQuestionOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predefined_question_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('predefined_question_id');
            $table->unsignedBigInteger('question_type_option_id')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['predefined_question_id','question_type_option_id'],"p_q_id_with_q_t_o_id_unique");
            $table->index(['predefined_question_id','question_type_option_id'],"p_q_id_with_q_t_o_id_index");

            $table->foreign('predefined_question_id')->references('id')->on('predefined_questions')
                ->onDelete('CASCADE');
            $table->foreign('question_type_option_id')->references('id')->on('question_type_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('predefined_question_options');
    }
}
