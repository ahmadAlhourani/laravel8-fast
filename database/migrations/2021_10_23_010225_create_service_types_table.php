<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_id')->unsigned();
            $table->string('name');
            $table->text('icon')->nullable();
            $table->integer('rank');
            $table->tinyInteger("price_type")->default(1)->comment('1=>per hour,2=>per service,3=>expectation'
            );
            $table->double('price')->nullable();
            $table->integer('show_price')->default(0)->comment('0 =>inactive, 1 active');

            $table->integer('status')->default(0)->comment('0 =>inactive, 1 active');
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->longText('html_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_types');
    }
}
