<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('iso')->unique();
            $table->string('name')->unique();
            $table->string('nice_name')->unique();
            $table->string('iso3')->unique()->nullable();
            $table->string('num_code')->nullable();
            $table->string('phone_code')->nullable();

            $table->index('iso');
            $table->index('name');
            $table->index('nice_name');
            $table->index('iso3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
