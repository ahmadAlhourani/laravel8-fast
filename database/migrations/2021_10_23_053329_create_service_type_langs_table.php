<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypeLangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_type_id');
            $table->unsignedBigInteger('lang_id');
            $table->string('name');
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->longText('html_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(["service_type_id","lang_id"]);
            $table->foreign('service_type_id')->references('id')->on('service_types')
                ->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_type_langs');
    }
}
