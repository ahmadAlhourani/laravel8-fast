<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertLangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('alert_id');
            $table->unsignedBigInteger('lang_id');
            $table->string('name');
            $table->timestamps();



            $table->index('alert_id');
            $table->index('lang_id');
            $table->unique(["alert_id","lang_id"]);
            $table->foreign('alert_id')->references('id')->on('alerts')->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alert_langs');
    }
}
