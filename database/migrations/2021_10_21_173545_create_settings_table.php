<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('setting_category_id');
            $table->string('key');
            $table->string('operator')->default("==");
            $table->string('value');
            $table->longText('description')->nullable();

            $table->index('key');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('setting_category_id')->references('id')->on('setting_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
