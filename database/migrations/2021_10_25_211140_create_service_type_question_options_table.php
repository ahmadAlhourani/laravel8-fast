<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypeQuestionOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type_question_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_type_question_id');
            $table->unsignedBigInteger('question_type_option_id')->nullable();
            $table->longText('description')->nullable();

            $table->unique(['service_type_question_id','question_type_option_id'],'s_t_q_id_with_q_t_o_id_unique');
            $table->index(['service_type_question_id','question_type_option_id'],'s_t_q_id_with_q_t_o_id_index');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('service_type_question_id')->references('id')->on('service_type_questions')
                ->onDelete('CASCADE');
            $table->foreign('question_type_option_id')->references('id')->on('question_type_options');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_type_question_options');
    }
}
