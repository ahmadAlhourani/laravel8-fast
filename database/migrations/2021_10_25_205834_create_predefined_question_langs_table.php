<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePredefinedQuestionLangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predefined_question_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('predefined_question_id');
            $table->unsignedBigInteger('lang_id');
            $table->longText('question');
            $table->longText('note')->nullable();
            $table->longText('answer_example')->nullable();
            $table->longText('instructions')->nullable();
            $table->longText('default_answer')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['predefined_question_id','lang_id']);
            $table->index(['predefined_question_id','lang_id']);
            $table->foreign('predefined_question_id')->references('id')->on('predefined_questions')
                ->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('langs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('predefined_question_langs');
    }
}
