<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypeQuestionLangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type_question_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('question');
            $table->unsignedBigInteger('service_type_question_id');
            $table->unsignedBigInteger('lang_id');
            $table->longText('note')->nullable();
            $table->longText('answer_example')->nullable();
            $table->longText('instructions')->nullable();
            $table->longText('default_answer')->nullable();

            $table->unique(['service_type_question_id','lang_id'],'s_t_q_with_lang_id_unique');
            $table->index(['service_type_question_id','lang_id'],'s_t_q_with_lang_id_index');

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('service_type_question_id')->references('id')->on('service_type_questions')
                ->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('langs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_type_question_langs');
    }
}
