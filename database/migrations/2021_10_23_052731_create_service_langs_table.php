<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceLangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('lang_id');
            $table->string('name');
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();;
            $table->longText('html_description')->nullable();;
            $table->longText('question_service_type')->nullable();;
            $table->longText('types_difference')->nullable();;
            $table->unique(["service_id","lang_id"]);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('service_id')->references('id')->on('services')
                ->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('langs')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_langs');
    }
}
