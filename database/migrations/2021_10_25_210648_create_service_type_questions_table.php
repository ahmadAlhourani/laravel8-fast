<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypeQuestionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('question');
            $table->unsignedBigInteger('service_type_id');
            $table->unsignedBigInteger('question_type_id');
            $table->tinyInteger('required')->default(0)->comment('0 =>Optional, 1 Required');
            $table->longText('description')->nullable();

            $table->unique(['service_type_id','question_type_id']);
            $table->index(['service_type_id','question_type_id']);

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('service_type_id')->references('id')->on('service_types')
                ->onDelete('CASCADE');
            ;
            $table->foreign('question_type_id')->references('id')->on('question_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_type_questions');
    }
}
