<?php

use App\Http\Controllers\Directory\AlertController;
use App\Http\Controllers\Directory\AlertLangController;
use App\Http\Controllers\Directory\CountryController;
use App\Http\Controllers\Directory\LangController;
use App\Http\Controllers\Directory\LogController;
use App\Http\Controllers\Services\ServiceController;
use App\Http\Controllers\Settings\SettingCategoryController;
use App\Http\Controllers\Settings\SettingController;
use Illuminate\Support\Facades\Route;






Route::get('/', [
    App\Http\Controllers\HomeController::class, 'index'
])->name('home');

Route::group(['middleware' => ['web_accessible']], function () {

    Route::get('/dashboard', [
        App\Http\Controllers\DashboardController::class, 'index'
    ])->name('dashboard');

    Route::resource('permissions', App\Http\Controllers\PermissionController::class);
    Route::post('permissions/loadFromRouter', [App\Http\Controllers\PermissionController::class, 'LoadPermission'])->name('permissions.load-router');

    Route::resource('roles', App\Http\Controllers\RoleController::class);

    Route::get('profile', [App\Http\Controllers\UserController::class, 'showProfile'])->name('users.profile');
    Route::patch('profile', [App\Http\Controllers\UserController::class, 'updateProfile'])->name('users.updateProfile');



    Route::resource('admins', App\Http\Controllers\AdminController::class);
    Route::resource('partners', App\Http\Controllers\PartnerController::class);
    Route::resource('users', App\Http\Controllers\UserController::class);
//    Route::get('users.normal', [App\Http\Controllers\UserController::class,"index2"])->name('users.normal');

    Route::resource('attendances', App\Http\Controllers\AttendanceController::class);

    Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('generator_builder.index');

    Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('generator_builder.field_template');

    Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('generator_builder.relation_field_template');

    Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('generator_builder.generate');

    Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('generator_builder.rollback');

    Route::post(
        'generator_builder/generate-from-file',
        '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
    )->name('generator_builder.from_file');



    Route::group(['prefix' => 'directory','middleware' => ['web_accessible']], function () {
        Route::resource('langs', LangController::class, ["as" => 'directory']);
        Route::resource('alerts', AlertController::class, ["as" => 'directory']);
        Route::resource('alertLangs', AlertLangController::class, ["as" => 'directory']);
        Route::resource('countries', CountryController::class, ["as" => 'directory']);
        Route::resource('logs', LogController::class,["as" => 'directory']);
    });




    Route::group(['prefix' => 'settings'], function () {
        Route::resource('settingCategories', SettingCategoryController::class, ["as" => 'settings']);
        Route::resource('settings', SettingController::class, ["as" => 'settings']);

    });
});























Route::group(['prefix' => 'services'], function () {
    Route::resource('serviceCategories', App\Http\Controllers\Services\ServiceCategoryController::class, ["as" => 'services']);


    Route::resource('serviceCategoryLangs', App\Http\Controllers\Services\ServiceCategoryLangController::class, ["as" => 'services']);
    Route::get('serviceCategoryLangs/index/{ServiceCategoryId}', ['uses' => 'App\Http\Controllers\Services\ServiceCategoryLangController@index', 'as' => 'services.categoryLangs']);


    Route::resource('services', App\Http\Controllers\Services\ServiceController::class, ["as" => 'services']);
    Route::get('services/index/{ServiceCategoryId}', ['uses' => 'App\Http\Controllers\Services\ServiceController@index', 'as' => 'services.categoryServices']);


    Route::resource('serviceLangs', App\Http\Controllers\Services\ServiceLangController::class, ["as" => 'services']);
    Route::get('services.serviceLangs/index/{ServiceId}', ['uses' => 'App\Http\Controllers\Services\ServiceLangController@index', 'as' => 'services.serviceLangs']);


    Route::resource('serviceTypes', App\Http\Controllers\Services\ServiceTypeController::class, ["as" => 'services']);
    Route::get('serviceTypes/index/{ServiceId}', ['uses' => 'App\Http\Controllers\Services\ServiceTypeController@index', 'as' => 'services.serviceTypes']);


    Route::resource('serviceTypeLangs', App\Http\Controllers\Services\ServiceTypeLangController::class, ["as" => 'services']);
    Route::get('serviceTypeLangs/index/{ServiceTypeId}', ['uses' => 'App\Http\Controllers\Services\ServiceTypeLangController@index', 'as' => 'services.serviceTypeLangs']);


    Route::resource('serviceTypeQuestions', App\Http\Controllers\Services\ServiceTypeQuestionController::class, ["as" => 'services']);
    Route::get('serviceTypeQuestions/index/{ServiceTypeId}', ['uses' => 'App\Http\Controllers\Services\ServiceTypeQuestionController@index', 'as' => 'services.serviceTypeQuestions']);



    Route::resource('serviceTypeQuestionLangs', App\Http\Controllers\Services\ServiceTypeQuestionLangController::class, ["as" => 'services']);
    Route::get('serviceTypeQuestionLangs/index/{ServiceTypeQuestionId}', ['uses' => 'App\Http\Controllers\Services\ServiceTypeQuestionLangController@index', 'as' => 'services.serviceTypeQuestionLangs']);



    Route::resource('serviceTypeQuestionOptions', App\Http\Controllers\Services\ServiceTypeQuestionOptionController::class, ["as" => 'services']);
    Route::get('serviceTypeQuestionOptions/index/{ServiceTypeQuestionId}', ['uses' => 'App\Http\Controllers\Services\ServiceTypeQuestionOptionController@index', 'as' => 'services.serviceTypeQuestionOptions']);


});



Route::group(['prefix' => 'questions'], function () {
    Route::resource('questionTypes', App\Http\Controllers\Questions\QuestionTypeController::class, ["as" => 'questions']);


    Route::resource('questionTypeOptions', App\Http\Controllers\Questions\QuestionTypeOptionController::class, ["as" => 'questions']);
    Route::get('questionTypeOptions/index/{QuestionTypeId}', ['uses' => 'App\Http\Controllers\Questions\QuestionTypeOptionController@index', 'as' => 'questions.questionTypeOptions']);

    Route::resource('predefinedQuestions', App\Http\Controllers\Questions\PredefinedQuestionController::class, ["as" => 'questions']);
    Route::get('predefinedQuestions/index/{QuestionTypeId}', ['uses' => 'App\Http\Controllers\Questions\PredefinedQuestionController@index', 'as' => 'questions.predefinedQuestions']);


    Route::resource('predefinedQuestionLangs', App\Http\Controllers\Questions\PredefinedQuestionLangController::class, ["as" => 'questions']);
    Route::get('predefinedQuestionLangs/index/{PredefinedQuestionId}', ['uses' => 'App\Http\Controllers\Questions\PredefinedQuestionLangController@index', 'as' => 'questions.predefinedQuestionLangs']);


    Route::resource('predefinedQuestionOptions', App\Http\Controllers\Questions\PredefinedQuestionOptionController::class, ["as" => 'questions']);
    Route::get('predefinedQuestionOptions/index/{PredefinedQuestionId}', ['uses' => 'App\Http\Controllers\Questions\PredefinedQuestionOptionController@index', 'as' => 'questions.predefinedQuestionOptions']);

});



