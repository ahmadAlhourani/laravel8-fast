<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/questionTypes.fields.id').':') !!}
    <p>{{ $questionType->id }}</p>
</div>

<!-- Key Field -->
<div class="col-sm-12">
    {!! Form::label('key', __('models/questionTypes.fields.key').':') !!}
    <p>{{ $questionType->key }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/questionTypes.fields.name').':') !!}
    <p>{{ $questionType->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/questionTypes.fields.description').':') !!}
    <p>{{ $questionType->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/questionTypes.fields.created_at').':') !!}
    <p>{{ $questionType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/questionTypes.fields.updated_at').':') !!}
    <p>{{ $questionType->updated_at }}</p>
</div>

