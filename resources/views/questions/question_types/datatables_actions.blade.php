{!! Form::open(['route' => ['questions.questionTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('questions.questionTypeOptions', $id) }}"  class='btn btn-default btn-xs' title="questionTypeOptions">
        <i class="fas fa-sitemap"></i>
    </a>

    <a href="{{ route('questions.predefinedQuestions', $id) }}"  class='btn btn-default btn-xs' title="predefinedQuestions">
        <i class="fas fa-sitemap" alt="predefinedQuestions"></i>
    </a>
    <a href="{{ route('questions.questionTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('questions.questionTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
