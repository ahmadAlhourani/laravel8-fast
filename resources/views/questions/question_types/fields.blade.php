<!-- Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('key', __('models/questionTypes.fields.key').':') !!}
    {!! Form::text('key', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/questionTypes.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/questionTypes.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>