<!-- Question Type Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('question_type_id', __('models/questionTypeOptions.fields.question_type_id').':') !!}
    {!! Form::select('question_type_id', $questionTypeItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', __('models/predefinedQuestions.fields.question').':') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/predefinedQuestions.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
