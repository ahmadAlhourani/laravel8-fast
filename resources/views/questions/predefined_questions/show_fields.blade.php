<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/predefinedQuestions.fields.id').':') !!}
    <p>{{ $predefinedQuestion->id }}</p>
</div>

<!-- Question Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_type_id', __('models/predefinedQuestions.fields.question_type_id').':') !!}
    <p>{{ $predefinedQuestion->question_type_id }}</p>
</div>

<!-- Question Field -->
<div class="col-sm-12">
    {!! Form::label('question', __('models/predefinedQuestions.fields.question').':') !!}
    <p>{{ $predefinedQuestion->question }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/predefinedQuestions.fields.description').':') !!}
    <p>{{ $predefinedQuestion->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/predefinedQuestions.fields.created_at').':') !!}
    <p>{{ $predefinedQuestion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/predefinedQuestions.fields.updated_at').':') !!}
    <p>{{ $predefinedQuestion->updated_at }}</p>
</div>

