<!-- Question Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_type_id', 'Question Type') !!}
    {!! Form::select('question_type_id', $questionTypeItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Option Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option', __('models/questionTypeOptions.fields.option').':') !!}
    {!! Form::text('option', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/questionTypeOptions.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
