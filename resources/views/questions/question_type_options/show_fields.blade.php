<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/questionTypeOptions.fields.id').':') !!}
    <p>{{ $questionTypeOption->id }}</p>
</div>

<!-- Question Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_type_id', __('models/questionTypeOptions.fields.question_type_id').':') !!}
    <p>{{ $questionTypeOption->question_type_id }}</p>
</div>

<!-- Option Field -->
<div class="col-sm-12">
    {!! Form::label('option', __('models/questionTypeOptions.fields.option').':') !!}
    <p>{{ $questionTypeOption->option }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/questionTypeOptions.fields.description').':') !!}
    <p>{{ $questionTypeOption->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/questionTypeOptions.fields.created_at').':') !!}
    <p>{{ $questionTypeOption->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/questionTypeOptions.fields.updated_at').':') !!}
    <p>{{ $questionTypeOption->updated_at }}</p>
</div>

