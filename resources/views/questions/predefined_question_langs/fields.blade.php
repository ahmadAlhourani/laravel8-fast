<!-- Predefined Question Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('predefined_question_id', 'Predefined Question') !!}
    {!! Form::select('predefined_question_id', $predefinedَQuestionItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Lang Id Field -->
<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id','Lang') !!}
    {!! Form::select('lang_id',  $langItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', __('models/predefinedQuestionLangs.fields.question').':') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', __('models/predefinedQuestionLangs.fields.note').':') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Answer Example Field -->
<div class="form-group col-sm-6">
    {!! Form::label('answer_example', __('models/predefinedQuestionLangs.fields.answer_example').':') !!}
    {!! Form::text('answer_example', null, ['class' => 'form-control']) !!}
</div>

<!-- Instructions Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instructions', __('models/predefinedQuestionLangs.fields.instructions').':') !!}
    {!! Form::text('instructions', null, ['class' => 'form-control']) !!}
</div>

<!-- Default Answer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default_answer', __('models/predefinedQuestionLangs.fields.default_answer').':') !!}
    {!! Form::text('default_answer', null, ['class' => 'form-control']) !!}
</div>
