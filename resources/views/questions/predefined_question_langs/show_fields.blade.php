<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/predefinedQuestionLangs.fields.id').':') !!}
    <p>{{ $predefinedQuestionLang->id }}</p>
</div>

<!-- Predefined Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('predefined_question_id', __('models/predefinedQuestionLangs.fields.predefined_question_id').':') !!}
    <p>{{ $predefinedQuestionLang->predefined_question_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/predefinedQuestionLangs.fields.lang_id').':') !!}
    <p>{{ $predefinedQuestionLang->lang_id }}</p>
</div>

<!-- Question Field -->
<div class="col-sm-12">
    {!! Form::label('question', __('models/predefinedQuestionLangs.fields.question').':') !!}
    <p>{{ $predefinedQuestionLang->question }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', __('models/predefinedQuestionLangs.fields.note').':') !!}
    <p>{{ $predefinedQuestionLang->note }}</p>
</div>

<!-- Answer Example Field -->
<div class="col-sm-12">
    {!! Form::label('answer_example', __('models/predefinedQuestionLangs.fields.answer_example').':') !!}
    <p>{{ $predefinedQuestionLang->answer_example }}</p>
</div>

<!-- Instructions Field -->
<div class="col-sm-12">
    {!! Form::label('instructions', __('models/predefinedQuestionLangs.fields.instructions').':') !!}
    <p>{{ $predefinedQuestionLang->instructions }}</p>
</div>

<!-- Default Answer Field -->
<div class="col-sm-12">
    {!! Form::label('default_answer', __('models/predefinedQuestionLangs.fields.default_answer').':') !!}
    <p>{{ $predefinedQuestionLang->default_answer }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/predefinedQuestionLangs.fields.created_at').':') !!}
    <p>{{ $predefinedQuestionLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/predefinedQuestionLangs.fields.updated_at').':') !!}
    <p>{{ $predefinedQuestionLang->updated_at }}</p>
</div>

