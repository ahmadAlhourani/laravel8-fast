<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/predefinedQuestionOptions.fields.id').':') !!}
    <p>{{ $predefinedQuestionOption->id }}</p>
</div>

<!-- Predefined Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('predefined_question_id', __('models/predefinedQuestionOptions.fields.predefined_question_id').':') !!}
    <p>{{ $predefinedQuestionOption->predefined_question_id }}</p>
</div>

<!-- Question Type Option Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_type_option_id', __('models/predefinedQuestionOptions.fields.question_type_option_id').':') !!}
    <p>{{ $predefinedQuestionOption->question_type_option_id }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/predefinedQuestionOptions.fields.description').':') !!}
    <p>{{ $predefinedQuestionOption->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/predefinedQuestionOptions.fields.created_at').':') !!}
    <p>{{ $predefinedQuestionOption->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/predefinedQuestionOptions.fields.updated_at').':') !!}
    <p>{{ $predefinedQuestionOption->updated_at }}</p>
</div>

