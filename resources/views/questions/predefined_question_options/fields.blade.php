<!-- Predefined Question Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('predefined_question_id', 'Predefined Question') !!}
    {!! Form::select('predefined_question_id', $predefinedَQuestionItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Question Type Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_type_option_id', __('models/predefinedQuestionOptions.fields.question_type_option_id').':') !!}
    {!! Form::select('question_type_option_id',$questionTypeOptionItems??[], null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/predefinedQuestionOptions.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
