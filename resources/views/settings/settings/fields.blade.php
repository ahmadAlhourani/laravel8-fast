<!-- Setting Category Id Field -->
<div class="form-group col-sm-6">
    {{--    {{print_r($langItems)}}--}}
    {!! Form::label('setting_category_id', __('models/settings.fields.setting_category_id').':') !!}
    {!! Form::select('setting_category_id',  $settingCategoryItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('key', __('models/settings.fields.key').':') !!}
    {!! Form::text('key', null, ['class' => 'form-control']) !!}
</div>

<!-- Operator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('operator', __('models/settings.fields.operator').':') !!}
    {!! Form::text('operator', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', __('models/settings.fields.value').':') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/settings.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

