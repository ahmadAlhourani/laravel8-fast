<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/settings.fields.id').':') !!}
    <p>{{ $setting->id }}</p>
</div>

<!-- Key Field -->
<div class="col-sm-12">
    {!! Form::label('key', __('models/settings.fields.key').':') !!}
    <p>{{ $setting->key }}</p>
</div>

<!-- Operator Field -->
<div class="col-sm-12">
    {!! Form::label('operator', __('models/settings.fields.operator').':') !!}
    <p>{{ $setting->operator }}</p>
</div>

<!-- Value Field -->
<div class="col-sm-12">
    {!! Form::label('value', __('models/settings.fields.value').':') !!}
    <p>{{ $setting->value }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/settings.fields.description').':') !!}
    <p>{{ $setting->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/settings.fields.created_at').':') !!}
    <p>{{ $setting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/settings.fields.updated_at').':') !!}
    <p>{{ $setting->updated_at }}</p>
</div>

<!-- Setting Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('setting_category_id', __('models/settings.fields.setting_category_id').':') !!}
    <p>{{ $setting->setting_category_id }}</p>
</div>

