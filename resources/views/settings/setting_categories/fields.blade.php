<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/settingCategories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', __('models/settingCategories.fields.value').':') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/settingCategories.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>