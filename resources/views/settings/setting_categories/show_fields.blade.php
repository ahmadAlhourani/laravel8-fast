<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/settingCategories.fields.id').':') !!}
    <p>{{ $settingCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/settingCategories.fields.name').':') !!}
    <p>{{ $settingCategory->name }}</p>
</div>

<!-- Value Field -->
<div class="col-sm-12">
    {!! Form::label('value', __('models/settingCategories.fields.value').':') !!}
    <p>{{ $settingCategory->value }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/settingCategories.fields.description').':') !!}
    <p>{{ $settingCategory->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/settingCategories.fields.created_at').':') !!}
    <p>{{ $settingCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/settingCategories.fields.updated_at').':') !!}
    <p>{{ $settingCategory->updated_at }}</p>
</div>

