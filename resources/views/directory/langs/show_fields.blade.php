<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/langs.fields.id').':') !!}
    <p>{{ $lang->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/langs.fields.name').':') !!}
    <p>{{ $lang->name }}</p>
</div>

<!-- Iso Field -->
<div class="col-sm-12">
    {!! Form::label('iso', __('models/langs.fields.iso').':') !!}
    <p>{{ $lang->iso }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/langs.fields.created_at').':') !!}
    <p>{{ $lang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/langs.fields.updated_at').':') !!}
    <p>{{ $lang->updated_at }}</p>
</div>

