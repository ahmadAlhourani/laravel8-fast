<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/langs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso', __('models/langs.fields.iso').':') !!}
    {!! Form::text('iso', null, ['class' => 'form-control']) !!}
</div>