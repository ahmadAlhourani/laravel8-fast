<!-- Msg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('msg', __('models/logs.fields.msg').':') !!}
    {!! Form::text('msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Module Field -->
<div class="form-group col-sm-6">
    {!! Form::label('module', __('models/logs.fields.module').':') !!}
    {!! Form::text('module', null, ['class' => 'form-control']) !!}
</div>

<!-- Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('method', __('models/logs.fields.method').':') !!}
    {!! Form::text('method', null, ['class' => 'form-control']) !!}
</div>