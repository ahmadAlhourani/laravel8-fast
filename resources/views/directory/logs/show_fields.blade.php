<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/logs.fields.id').':') !!}
    <p>{{ $log->id }}</p>
</div>

<!-- Msg Field -->
<div class="col-sm-12">
    {!! Form::label('msg', __('models/logs.fields.msg').':') !!}
    <p>{{ $log->msg }}</p>
</div>

<!-- Module Field -->
<div class="col-sm-12">
    {!! Form::label('module', __('models/logs.fields.module').':') !!}
    <p>{{ $log->module }}</p>
</div>

<!-- Method Field -->
<div class="col-sm-12">
    {!! Form::label('method', __('models/logs.fields.method').':') !!}
    <p>{{ $log->method }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/logs.fields.created_at').':') !!}
    <p>{{ $log->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/logs.fields.updated_at').':') !!}
    <p>{{ $log->updated_at }}</p>
</div>

