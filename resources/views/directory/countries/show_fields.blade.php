<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/countries.fields.id').':') !!}
    <p>{{ $country->id }}</p>
</div>

<!-- Iso Field -->
<div class="col-sm-12">
    {!! Form::label('iso', __('models/countries.fields.iso').':') !!}
    <p>{{ $country->iso }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/countries.fields.name').':') !!}
    <p>{{ $country->name }}</p>
</div>

<!-- nice_name Field -->
<div class="col-sm-12">
    {!! Form::label('nice_name', __('models/countries.fields.nice_name').':') !!}
    <p>{{ $country->nice_name }}</p>
</div>

<!-- Iso3 Field -->
<div class="col-sm-12">
    {!! Form::label('iso3', __('models/countries.fields.iso3').':') !!}
    <p>{{ $country->iso3 }}</p>
</div>

<!-- num_code Field -->
<div class="col-sm-12">
    {!! Form::label('num_code', __('models/countries.fields.num_code').':') !!}
    <p>{{ $country->num_code }}</p>
</div>

<!-- phone_code Field -->
<div class="col-sm-12">
    {!! Form::label('phone_code', __('models/countries.fields.phone_code').':') !!}
    <p>{{ $country->phone_code }}</p>
</div>

