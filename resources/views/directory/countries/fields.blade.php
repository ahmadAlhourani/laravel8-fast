<!-- Iso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso', __('models/countries.fields.iso').':') !!}
    {!! Form::text('iso', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/countries.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- nice_name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nice_name', __('models/countries.fields.nice_name').':') !!}
    {!! Form::text('nice_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso3', __('models/countries.fields.iso3').':') !!}
    {!! Form::text('iso3', null, ['class' => 'form-control']) !!}
</div>

<!-- num_code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_code', __('models/countries.fields.num_code').':') !!}
    {!! Form::text('num_code', null, ['class' => 'form-control']) !!}
</div>

<!-- phone_code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_code', __('models/countries.fields.phone_code').':') !!}
    {!! Form::text('phone_code', null, ['class' => 'form-control']) !!}
</div>
