<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', __('models/alerts.fields.state').':') !!}
    {!! Form::number('state', null, ['class' => 'form-control']) !!}
</div>


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name','Name :') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<!-- Module Field -->
<div class="form-group col-sm-6">
    {!! Form::label('module', __('models/alerts.fields.module').':') !!}
    {!! Form::text('module', null, ['class' => 'form-control']) !!}
</div>

