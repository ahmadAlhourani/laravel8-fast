<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/alerts.fields.id').':') !!}
    <p>{{ $alert->id }}</p>
</div>

<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', __('models/alerts.fields.state').':') !!}
    <p>{{ $alert->state }}</p>
</div>

<!-- Module Field -->
<div class="col-sm-12">
    {!! Form::label('module', __('models/alerts.fields.module').':') !!}
    <p>{{ $alert->module }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/alerts.fields.created_at').':') !!}
    <p>{{ $alert->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/alerts.fields.updated_at').':') !!}
    <p>{{ $alert->updated_at }}</p>
</div>

