<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/alertLangs.fields.id').':') !!}
    <p>{{ $alertLang->id }}</p>
</div>

<!-- Alert Id Field -->
<div class="col-sm-12">
    {!! Form::label('alert_id', __('models/alertLangs.fields.alert_id').':') !!}
    <p>{{ $alertLang->alert_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/alertLangs.fields.lang_id').':') !!}
    <p>{{ $alertLang->lang_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/alertLangs.fields.name').':') !!}
    <p>{{ $alertLang->name }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/alertLangs.fields.created_at').':') !!}
    <p>{{ $alertLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/alertLangs.fields.updated_at').':') !!}
    <p>{{ $alertLang->updated_at }}</p>
</div>

