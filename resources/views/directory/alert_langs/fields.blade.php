<!-- Alert Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alert_id', __('models/alertLangs.fields.alert_id').':') !!}
    {!! Form::select('alert_id', $alertItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id', __('models/alertLangs.fields.lang_id').':') !!}
    {!! Form::select('lang_id',  $langItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/alertLangs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
