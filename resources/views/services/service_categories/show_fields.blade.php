<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceCategories.fields.id').':') !!}
    <p>{{ $serviceCategory->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceCategories.fields.name').':') !!}
    <p>{{ $serviceCategory->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceCategories.fields.description').':') !!}
    <p>{{ $serviceCategory->description }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/serviceCategories.fields.icon').':') !!}
    <p>{{ $serviceCategory->icon }}</p>
</div>

<!-- Rank Field -->
<div class="col-sm-12">
    {!! Form::label('rank', __('models/serviceCategories.fields.rank').':') !!}
    <p>{{ $serviceCategory->rank }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceCategories.fields.created_at').':') !!}
    <p>{{ $serviceCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceCategories.fields.updated_at').':') !!}
    <p>{{ $serviceCategory->updated_at }}</p>
</div>

