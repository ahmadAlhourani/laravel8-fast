<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceCategories.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => 'required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceCategories.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon', __('models/serviceCategories.fields.icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Rank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rank', __('models/serviceCategories.fields.rank').':') !!}
    {!! Form::number('rank', null, ['class' => 'form-control','required' => 'required']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', "Status") !!}
    {!! Form::checkbox('status',1,isset($serviceCategory)?( $serviceCategory->status ==1?true:null):null, ['class' => 'form-control']) !!}

</div>
