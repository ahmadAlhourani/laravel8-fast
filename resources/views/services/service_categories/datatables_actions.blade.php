{!! Form::open(['route' => ['services.serviceCategories.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('services.categoryServices', $id) }}" class='btn btn-default btn-xs'>
        <i class="fas fa-sitemap"></i>
    </a>

    <a href="{{ route('services.categoryLangs', $id) }}" class='btn btn-default btn-xs'>
        <i class="fas fa-language"></i>
    </a>



{{--    <a href="{{ route('services.services.index', $id) }}" class='btn btn-default btn-xs'>--}}
{{--        <i class="fas fa-sitemap"></i>--}}
{{--    </a>--}}
    <a href="{{ route('services.serviceCategories.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('services.serviceCategories.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
