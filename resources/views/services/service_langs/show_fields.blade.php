<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceLangs.fields.id').':') !!}
    <p>{{ $serviceLang->id }}</p>
</div>

<!-- Service Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_id', __('models/serviceLangs.fields.service_id').':') !!}
    <p>{{ $serviceLang->service_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/serviceLangs.fields.lang_id').':') !!}
    <p>{{ $serviceLang->lang_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceLangs.fields.name').':') !!}
    <p>{{ $serviceLang->name }}</p>
</div>

<!-- Short Description Field -->
<div class="col-sm-12">
    {!! Form::label('short_description', __('models/serviceLangs.fields.short_description').':') !!}
    <p>{{ $serviceLang->short_description }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceLangs.fields.description').':') !!}
    <p>{{ $serviceLang->description }}</p>
</div>

<!-- Html Description Field -->
<div class="col-sm-12">
    {!! Form::label('html_description', __('models/serviceLangs.fields.html_description').':') !!}
    <p>{{ $serviceLang->html_description }}</p>
</div>

<!-- Question Service Type Field -->
<div class="col-sm-12">
    {!! Form::label('question_service_type', __('models/serviceLangs.fields.question_service_type').':') !!}
    <p>{{ $serviceLang->question_service_type }}</p>
</div>

<!-- Types Difference Field -->
<div class="col-sm-12">
    {!! Form::label('types_difference', __('models/serviceLangs.fields.types_difference').':') !!}
    <p>{{ $serviceLang->types_difference }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceLangs.fields.created_at').':') !!}
    <p>{{ $serviceLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceLangs.fields.updated_at').':') !!}
    <p>{{ $serviceLang->updated_at }}</p>
</div>

