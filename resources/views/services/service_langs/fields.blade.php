<!-- Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_id', 'Service') !!}
    {!! Form::select('service_id', $serviceItems??[], null, ['class' => 'form-control']) !!}
</div>


<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id', "Lang") !!}
    {!! Form::select('lang_id',  $langItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceLangs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Short Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('short_description', __('models/serviceLangs.fields.short_description').':') !!}
    {!! Form::text('short_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceLangs.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Html Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('html_description', __('models/serviceLangs.fields.html_description').':') !!}
    {!! Form::text('html_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Service Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_service_type', __('models/serviceLangs.fields.question_service_type').':') !!}
    {!! Form::text('question_service_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Types Difference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('types_difference', __('models/serviceLangs.fields.types_difference').':') !!}
    {!! Form::text('types_difference', null, ['class' => 'form-control']) !!}
</div>
