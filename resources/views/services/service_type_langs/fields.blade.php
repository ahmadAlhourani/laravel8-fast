<!-- Service Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_type_id', 'Service Type') !!}
    {!! Form::select('service_type_id', $serviceTypeItems??[], null, ['class' => 'form-control']) !!}
</div>



<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id', "Lang") !!}
    {!! Form::select('lang_id',  $langItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceTypeLangs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Short Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('short_description', __('models/serviceTypeLangs.fields.short_description').':') !!}
    {!! Form::text('short_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceTypeLangs.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Html Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('html_description', __('models/serviceTypeLangs.fields.html_description').':') !!}
    {!! Form::text('html_description', null, ['class' => 'form-control']) !!}
</div>
