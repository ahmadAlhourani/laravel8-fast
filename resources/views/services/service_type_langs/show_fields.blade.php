<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceTypeLangs.fields.id').':') !!}
    <p>{{ $serviceTypeLang->id }}</p>
</div>

<!-- Service Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_type_id', __('models/serviceTypeLangs.fields.service_type_id').':') !!}
    <p>{{ $serviceTypeLang->service_type_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/serviceTypeLangs.fields.lang_id').':') !!}
    <p>{{ $serviceTypeLang->lang_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceTypeLangs.fields.name').':') !!}
    <p>{{ $serviceTypeLang->name }}</p>
</div>

<!-- Short Description Field -->
<div class="col-sm-12">
    {!! Form::label('short_description', __('models/serviceTypeLangs.fields.short_description').':') !!}
    <p>{{ $serviceTypeLang->short_description }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceTypeLangs.fields.description').':') !!}
    <p>{{ $serviceTypeLang->description }}</p>
</div>

<!-- Html Description Field -->
<div class="col-sm-12">
    {!! Form::label('html_description', __('models/serviceTypeLangs.fields.html_description').':') !!}
    <p>{{ $serviceTypeLang->html_description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypeLangs.fields.created_at').':') !!}
    <p>{{ $serviceTypeLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypeLangs.fields.updated_at').':') !!}
    <p>{{ $serviceTypeLang->updated_at }}</p>
</div>

