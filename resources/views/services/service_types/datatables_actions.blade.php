{!! Form::open(['route' => ['services.serviceTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('services.serviceTypeLangs', $id) }}" title="serviceTypeLangs" class='btn btn-default btn-xs'>
        <i class="fas fa-language"></i>
    </a>

    <a href="{{ route('services.serviceTypeQuestions', $id) }}" title="serviceTypeQuestions" class='btn btn-default btn-xs' >
        <i class="far fa-question-circle"></i>
    </a>
    <a href="{{ route('services.serviceTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('services.serviceTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => 'return confirm("'.__('crud.are_you_sure').'")'
    ]) !!}
</div>
{!! Form::close() !!}
