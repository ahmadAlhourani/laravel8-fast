

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceTypes.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => 'required']) !!}
</div>
<!-- Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_id', 'Service') !!}
    {!! Form::select('service_id', $serviceItems??[], null, ['class' => 'form-control','required' => 'required']) !!}
</div>
<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon', __('models/serviceTypes.fields.icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Rank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rank', __('models/serviceTypes.fields.rank').':') !!}
    {!! Form::number('rank', null, ['class' => 'form-control','required' => 'required']) !!}
</div>
<!-- Price Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_type', "Price Type")!!}
    {!! Form::select('price_type',[1=>"Per Hour",2=>"Per Service",3=>"expectation"], null, ['class' => 'form-control','required' => 'required']) !!}
{{--    {!! Form::select('service_id', $serviceItems??[], null, ['class' => 'form-control']) !!}--}}
</div>
<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', __('models/serviceTypes.fields.price').':') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Show Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('show_price', "Show Price") !!}
    {!! Form::checkbox('show_price',1,isset($serviceType)?( $serviceType->show_price ==1?true:null):null, ['class' => 'form-control']) !!}
</div>


<!-- Short Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('short_description', __('models/serviceTypes.fields.short_description').':') !!}
    {!! Form::text('short_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceTypes.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Html Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('html_description', __('models/serviceTypes.fields.html_description').':') !!}
    {!! Form::text('html_description', null, ['class' => 'form-control']) !!}
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', "Status") !!}
    {!! Form::checkbox('status',1,isset($serviceType)?( $serviceType->status ==1?true:null):null, ['class' => 'form-control']) !!}
</div>
