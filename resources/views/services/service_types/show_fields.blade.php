<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceTypes.fields.id').':') !!}
    <p>{{ $serviceType->id }}</p>
</div>

<!-- Service Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_id', __('models/serviceTypes.fields.service_id').':') !!}
    <p>{{ $serviceType->service_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceTypes.fields.name').':') !!}
    <p>{{ $serviceType->name }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/serviceTypes.fields.icon').':') !!}
    <p>{{ $serviceType->icon }}</p>
</div>

<!-- Rank Field -->
<div class="col-sm-12">
    {!! Form::label('rank', __('models/serviceTypes.fields.rank').':') !!}
    <p>{{ $serviceType->rank }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', __('models/serviceTypes.fields.price').':') !!}
    <p>{{ $serviceType->price }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', __('models/serviceTypes.fields.status').':') !!}
    <p>{{ $serviceType->status }}</p>
</div>

<!-- Short Description Field -->
<div class="col-sm-12">
    {!! Form::label('short_description', __('models/serviceTypes.fields.short_description').':') !!}
    <p>{{ $serviceType->short_description }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceTypes.fields.description').':') !!}
    <p>{{ $serviceType->description }}</p>
</div>

<!-- Html Description Field -->
<div class="col-sm-12">
    {!! Form::label('html_description', __('models/serviceTypes.fields.html_description').':') !!}
    <p>{{ $serviceType->html_description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypes.fields.created_at').':') !!}
    <p>{{ $serviceType->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypes.fields.updated_at').':') !!}
    <p>{{ $serviceType->updated_at }}</p>
</div>

