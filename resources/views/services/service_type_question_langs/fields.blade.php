

<!-- Service Type Question Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_type_question_id', 'serviceTypeQuestion') !!}
    {!! Form::select('service_type_question_id',$serviceTypeQuestionItems ?? [], null, ['class' => 'form-control']) !!}
</div>

<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id','Lang') !!}
    {!! Form::select('lang_id', $langItems ?? [],null, ['class' => 'form-control']) !!}
</div>




<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', __('models/serviceTypeQuestionLangs.fields.question').':') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>
<!-- Note Field -->
<div class="form-group col-sm-6">

        {!! Form::label('note', __('models/serviceTypeQuestionLangs.fields.note').':') !!}
        {!! Form::text('note', null, ['class' => 'form-control']) !!}

</div>


<!-- Answer Example Field -->
<div class="form-group col-sm-6">
    {!! Form::label('answer_example', __('models/serviceTypeQuestionLangs.fields.answer_example').':') !!}
    {!! Form::text('answer_example', null, ['class' => 'form-control']) !!}
</div>

<!-- Instructions Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instructions', __('models/serviceTypeQuestionLangs.fields.instructions').':') !!}
    {!! Form::text('instructions', null, ['class' => 'form-control']) !!}
</div>

<!-- Default Answer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default_answer', __('models/serviceTypeQuestionLangs.fields.default_answer').':') !!}
    {!! Form::text('default_answer', null, ['class' => 'form-control']) !!}
</div>
