<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceTypeQuestionLangs.fields.id').':') !!}
    <p>{{ $serviceTypeQuestionLang->id }}</p>
</div>

<!-- Question Field -->
<div class="col-sm-12">
    {!! Form::label('question', __('models/serviceTypeQuestionLangs.fields.question').':') !!}
    <p>{{ $serviceTypeQuestionLang->question }}</p>
</div>

<!-- Service Type Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_type_question_id', __('models/serviceTypeQuestionLangs.fields.service_type_question_id').':') !!}
    <p>{{ $serviceTypeQuestionLang->service_type_question_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/serviceTypeQuestionLangs.fields.lang_id').':') !!}
    <p>{{ $serviceTypeQuestionLang->lang_id }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', __('models/serviceTypeQuestionLangs.fields.note').':') !!}
    <p>{{ $serviceTypeQuestionLang->note }}</p>
</div>

<!-- Answer Example Field -->
<div class="col-sm-12">
    {!! Form::label('answer_example', __('models/serviceTypeQuestionLangs.fields.answer_example').':') !!}
    <p>{{ $serviceTypeQuestionLang->answer_example }}</p>
</div>

<!-- Instructions Field -->
<div class="col-sm-12">
    {!! Form::label('instructions', __('models/serviceTypeQuestionLangs.fields.instructions').':') !!}
    <p>{{ $serviceTypeQuestionLang->instructions }}</p>
</div>

<!-- Default Answer Field -->
<div class="col-sm-12">
    {!! Form::label('default_answer', __('models/serviceTypeQuestionLangs.fields.default_answer').':') !!}
    <p>{{ $serviceTypeQuestionLang->default_answer }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypeQuestionLangs.fields.created_at').':') !!}
    <p>{{ $serviceTypeQuestionLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypeQuestionLangs.fields.updated_at').':') !!}
    <p>{{ $serviceTypeQuestionLang->updated_at }}</p>
</div>

