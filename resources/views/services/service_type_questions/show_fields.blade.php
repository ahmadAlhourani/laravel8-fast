<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceTypeQuestions.fields.id').':') !!}
    <p>{{ $serviceTypeQuestion->id }}</p>
</div>

<!-- Question Field -->
<div class="col-sm-12">
    {!! Form::label('question', __('models/serviceTypeQuestions.fields.question').':') !!}
    <p>{{ $serviceTypeQuestion->question }}</p>
</div>

<!-- Service Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_type_id', __('models/serviceTypeQuestions.fields.service_type_id').':') !!}
    <p>{{ $serviceTypeQuestion->service_type_id }}</p>
</div>

<!-- Question Type Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_type_id', __('models/serviceTypeQuestions.fields.question_type_id').':') !!}
    <p>{{ $serviceTypeQuestion->question_type_id }}</p>
</div>

<!-- Required Field -->
<div class="col-sm-12">
    {!! Form::label('required', __('models/serviceTypeQuestions.fields.required').':') !!}
    <p>{{ $serviceTypeQuestion->required }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceTypeQuestions.fields.description').':') !!}
    <p>{{ $serviceTypeQuestion->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypeQuestions.fields.created_at').':') !!}
    <p>{{ $serviceTypeQuestion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypeQuestions.fields.updated_at').':') !!}
    <p>{{ $serviceTypeQuestion->updated_at }}</p>
</div>

