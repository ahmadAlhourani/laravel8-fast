

<!-- Service Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_type_id', 'Service Type') !!}
    {!! Form::select('service_type_id', $serviceTypeItems??[], null, ['class' => 'form-control']) !!}
</div>

<!-- Question Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_type_id', 'Question Type') !!}
    {!! Form::select('question_type_id', $questionTypeItems?? [], null, ['class' => 'form-control custom-select']) !!}
</div>
<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', __('models/serviceTypeQuestions.fields.question').':') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>


<!-- Required Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('required', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('required', '1', null, ['class' => 'form-check-input']) !!}
           {!! Form::label('required', __('models/serviceTypeQuestions.fields.required').':', ['class' => 'form-check-label'])!!}
    </div>
</div>


<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceTypeQuestions.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
