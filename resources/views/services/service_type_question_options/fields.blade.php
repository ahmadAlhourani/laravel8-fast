<!-- Service Type Question Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('service_type_question_id', 'serviceTypeQuestion') !!}
    {!! Form::select('service_type_question_id',$serviceTypeQuestionItems ?? [], null, ['class' => 'form-control']) !!}
</div>

<!-- Question Type Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_type_option_id', __('models/predefinedQuestionOptions.fields.question_type_option_id').':') !!}
    {!! Form::select('question_type_option_id',$questionTypeOptionItems??[], null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control','rows'=>5]) !!}
</div>
