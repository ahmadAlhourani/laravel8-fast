<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceTypeQuestionOptions.fields.id').':') !!}
    <p>{{ $serviceTypeQuestionOption->id }}</p>
</div>

<!-- Service Type Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_type_question_id', __('models/serviceTypeQuestionOptions.fields.service_type_question_id').':') !!}
    <p>{{ $serviceTypeQuestionOption->service_type_question_id }}</p>
</div>

<!-- Question Type Option Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_type_option_id', __('models/serviceTypeQuestionOptions.fields.question_type_option_id').':') !!}
    <p>{{ $serviceTypeQuestionOption->question_type_option_id }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceTypeQuestionOptions.fields.description').':') !!}
    <p>{{ $serviceTypeQuestionOption->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceTypeQuestionOptions.fields.created_at').':') !!}
    <p>{{ $serviceTypeQuestionOption->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceTypeQuestionOptions.fields.updated_at').':') !!}
    <p>{{ $serviceTypeQuestionOption->updated_at }}</p>
</div>

