<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/services.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control','required' => 'required']) !!}
</div>


<!-- Service Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_category_id',"Service Category") !!}
    {!! Form::select('service_category_id', $serviceCategoryItems??[], null, ['class' => 'form-control custom-select','required' => 'required']) !!}
</div>


<!-- Short Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('short_description', __('models/services.fields.short_description').':') !!}
    {!! Form::text('short_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon', __('models/services.fields.icon').':') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('icon', ['class' => 'custom-file-input']) !!}
            {!! Form::label('icon', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Rank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rank', __('models/services.fields.rank').':') !!}
    {!! Form::number('rank', null, ['class' => 'form-control','required' => 'required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/services.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Html Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('html_description', __('models/services.fields.html_description').':') !!}
    {!! Form::text('html_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Service Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_service_type', __('models/services.fields.question_service_type').':') !!}
    {!! Form::text('question_service_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Types Difference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('types_difference', __('models/services.fields.types_difference').':') !!}
    {!! Form::text('types_difference', null, ['class' => 'form-control']) !!}
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', "Status") !!}
    {!! Form::checkbox('status',1,isset($service)?( $service->status ==1?true:null):null, ['class' => 'form-control']) !!}
</div>
