<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/services.fields.id').':') !!}
    <p>{{ $service->id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/services.fields.name').':') !!}
    <p>{{ $service->name }}</p>
</div>

<!-- Service Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_category_id', __('models/services.fields.service_category_id').':') !!}
    <p>{{ $service->service_category_id }}</p>
</div>

<!-- Short Description Field -->
<div class="col-sm-12">
    {!! Form::label('short_description', __('models/services.fields.short_description').':') !!}
    <p>{{ $service->short_description }}</p>
</div>

<!-- Icon Field -->
<div class="col-sm-12">
    {!! Form::label('icon', __('models/services.fields.icon').':') !!}
    <p>{{ $service->icon }}</p>
</div>

<!-- Rank Field -->
<div class="col-sm-12">
    {!! Form::label('rank', __('models/services.fields.rank').':') !!}
    <p>{{ $service->rank }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/services.fields.description').':') !!}
    <p>{{ $service->description }}</p>
</div>

<!-- Html Description Field -->
<div class="col-sm-12">
    {!! Form::label('html_description', __('models/services.fields.html_description').':') !!}
    <p>{{ $service->html_description }}</p>
</div>

<!-- Question Service Type Field -->
<div class="col-sm-12">
    {!! Form::label('question_service_type', __('models/services.fields.question_service_type').':') !!}
    <p>{{ $service->question_service_type }}</p>
</div>

<!-- Types Difference Field -->
<div class="col-sm-12">
    {!! Form::label('types_difference', __('models/services.fields.types_difference').':') !!}
    <p>{{ $service->types_difference }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/services.fields.created_at').':') !!}
    <p>{{ $service->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/services.fields.updated_at').':') !!}
    <p>{{ $service->updated_at }}</p>
</div>

