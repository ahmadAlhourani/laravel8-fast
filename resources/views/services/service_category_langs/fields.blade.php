<!-- Service Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_category_id',"Service Category") !!}
    {!! Form::select('service_category_id', $serviceCategoryItems??[], null, ['class' => 'form-control custom-select']) !!}
</div>





<!-- Lang Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lang_id', "Lang") !!}
    {!! Form::select('lang_id',  $langItems ?? [], null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/serviceCategoryLangs.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/serviceCategoryLangs.fields.description').':') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
