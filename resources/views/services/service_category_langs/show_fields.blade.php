<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', __('models/serviceCategoryLangs.fields.id').':') !!}
    <p>{{ $serviceCategoryLang->id }}</p>
</div>

<!-- Service Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('service_category_id', __('models/serviceCategoryLangs.fields.service_category_id').':') !!}
    <p>{{ $serviceCategoryLang->service_category_id }}</p>
</div>

<!-- Lang Id Field -->
<div class="col-sm-12">
    {!! Form::label('lang_id', __('models/serviceCategoryLangs.fields.lang_id').':') !!}
    <p>{{ $serviceCategoryLang->lang_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', __('models/serviceCategoryLangs.fields.name').':') !!}
    <p>{{ $serviceCategoryLang->name }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/serviceCategoryLangs.fields.description').':') !!}
    <p>{{ $serviceCategoryLang->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', __('models/serviceCategoryLangs.fields.created_at').':') !!}
    <p>{{ $serviceCategoryLang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', __('models/serviceCategoryLangs.fields.updated_at').':') !!}
    <p>{{ $serviceCategoryLang->updated_at }}</p>
</div>

