@php
    $urlAdmin=config('fast.admin_prefix');
@endphp

@can('dashboard')
    @php
        $isDashboardActive = Request::is($urlAdmin);
    @endphp
    <li class="nav-item">
        <a href="{{ route('dashboard') }}" class="nav-link {{ $isDashboardActive ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>@lang('menu.dashboard')</p>
        </a>
    </li>
@endcan

@can('generator_builder.index')
    @php
        $isUserActive = Request::is($urlAdmin.'*generator_builder*');
    @endphp
    <li class="nav-item">
        <a href="{{ route('generator_builder.index') }}" class="nav-link {{ $isUserActive ? 'active' : '' }}">
            <i class="nav-icon fas fa-coins"></i>
            <p>@lang('menu.generator_builder.title')</p>
        </a>
    </li>
@endcan

@can('attendances.index')
    @php
        $isUserActive = Request::is($urlAdmin.'*attendances*');
    @endphp

    <li class="nav-item">
        <a href="{{ route('attendances.index') }}" class="nav-link {{ $isUserActive ? 'active' : '' }}">
            <i class="nav-icon fas fa-calendar-alt"></i>

            <p>@lang('menu.attendances.title')</p>
        </a>
    </li>
@endcan

@canany(['users.index','roles.index','permissions.index'])
    @php
        $isAdminActive = Request::is($urlAdmin.'*admins*');
        $isPartnerActive = Request::is($urlAdmin.'*partners*');
        $isUserActive = Request::is($urlAdmin.'*users*');
        $isRoleActive = Request::is($urlAdmin.'*roles*');
        $isPermissionActive = Request::is($urlAdmin.'*permissions*');


    @endphp
    <li class="nav-item {{($isAdminActive||$isPartnerActive||$isUserActive||$isRoleActive||$isPermissionActive)?'menu-open':''}} ">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-shield-virus"></i>
            <p>
                @lang('menu.user.title')
                <i class="nav-icon  fas  fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @can('admins.index')
                <li class="nav-item">
                    <a href="{{ route('admins.index') }}" class="nav-link {{ $isAdminActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Admins
                        </p>
                    </a>
                </li>
            @endcan
            @can('partners.index')
                <li class="nav-item">
                    <a href="{{ route('partners.index') }}" class="nav-link {{ $isPartnerActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-handshake"></i>
                        <p>
                            Partners
                        </p>
                    </a>
                </li>
            @endcan
            @can('users.index')
                <li class="nav-item">
                    <a href="{{ route('users.index') }}" class="nav-link {{ $isUserActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            @lang('menu.user.users')
                        </p>
                    </a>
                </li>
            @endcan

            @can('roles.index')
                <li class="nav-item">
                    <a href="{{ route('roles.index') }}" class="nav-link {{ $isRoleActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            @lang('menu.user.roles')
                        </p>
                    </a>
                </li>
            @endcan
            @can('permissions.index')
                <li class="nav-item ">
                    <a href="{{ route('permissions.index') }}"
                       class="nav-link {{ $isPermissionActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-shield-alt"></i>
                        <p>
                            @lang('menu.user.permissions')
                        </p>
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcan




{{--Directory--}}
@canany(['logs.index','directory.langs.index','directory.alerts.index','directory.alertLangs.index'
,'directory.countries.index'])
    @php

        $isLogsActive = Request::is($urlAdmin.'*directory/logs*');;
        $isLangsActive = Request::is($urlAdmin.'*directory/langs*');
        $isAlertsActive = Request::is($urlAdmin.'*directory/alerts*');
        $isAlertLangsActive = Request::is($urlAdmin.'*directory/alertLangs*');
        $isCountriesActive = Request::is($urlAdmin.'*directory/countries*');

    @endphp
    <li class="nav-item {{($isLangsActive||$isLogsActive||$isAlertsActive|| $isAlertLangsActive||$isCountriesActive)?'menu-open':''}} ">
        <a href="#" class="nav-link">
            {{--            <i class="nav-icon fas fa-shield-virus"></i>--}}
            <i class="nav-icon fas fa-sliders-h"></i>
            <p>
                Directory
                <i class="nav-icon  fas  fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">


            @can('logs.index')
                <li class="nav-item">
                    <a href="{{ route('directory.logs.index') }}" class="nav-link {{ $isLogsActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-bug"></i>
                        <p>@lang('models/logs.plural')</p>
                    </a>
                </li>


            @endcan
            @can('directory.langs.index')
                <li class="nav-item">
                    <a href="{{ route('directory.langs.index') }}"
                       class="nav-link {{ $isLangsActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-language"></i>
                        <p>@lang('models/langs.plural')  </p>
                    </a>
                </li>
            @endcan
            @can('directory.alerts.index')
                <li class="nav-item">
                    <a href="{{ route('directory.alerts.index') }}"
                       class="nav-link {{ $isAlertsActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-exclamation-triangle"></i>
                        <p>@lang('models/alerts.plural')</p>
                    </a>
                </li>
            @endcan
            @can('directory.alertLangs.index')
                <li class="nav-item">
                    <a href="{{ route('directory.alertLangs.index') }}"
                       class="nav-link {{ $isAlertLangsActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-exclamation-circle"></i>
                        <p>@lang('models/alertLangs.plural')</p>
                    </a>
                </li>
            @endcan
            @can('directory.countries.index')
                <li class="nav-item">
                    <a href="{{ route('directory.countries.index') }}"
                       class="nav-link {{ $isCountriesActive? 'active' : '' }}">
                        <i class="nav-icon fas fa-globe-americas"></i>
                        <p>@lang('models/countries.plural')</p>
                    </a>
                </li>
            @endcan

        </ul>
    </li>
@endcan

{{--Setting--}}
@canany(['settings.settings.index','settings.settingCategories.index'])
    @php


        $isSettingCategoriesActive = Request::is($urlAdmin.'*settings/settingCategories*');
        $isSettingsActive = Request::is($urlAdmin.'*settings/settings*');

    @endphp
    <li class="nav-item {{($isSettingCategoriesActive||$isSettingsActive)?'menu-open':''}} ">
        <a href="#" class="nav-link">
            {{--            <i class="nav-icon fas fa-sliders-h"></i>--}}
            <i class="nav-icon fas fa-wrench"></i>
            <p>
                General Settings
                <i class="nav-icon  fas  fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">


            @can('settings.settingCategories.index')
                <li class="nav-item">
                    <a href="{{ route('settings.settingCategories.index') }}"
                       class="nav-link {{ $isSettingCategoriesActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-toolbox"></i>
                        <p>@lang('models/settingCategories.plural')</p>
                    </a>
                </li>
            @endcan
            @can('settings.settingCategories.index')
                <li class="nav-item">
                    <a href="{{ route('settings.settings.index') }}"
                       class="nav-link {{ $isSettingsActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tools"></i>
                        <p>@lang('models/settings.plural')</p>
                    </a>
                </li>
            @endcan

        </ul>
    </li>
@endcan






{{--services--}}
@canany(['services.serviceCategories.index','services.serviceCategoryLangs.index'
,'services.services.index','services.services.serviceLangs.index','services.serviceTypes.index','services.serviceTypeLangs.index'
,'services.serviceTypeQuestions.index','services.serviceTypeQuestionLangs.index','services.serviceTypeQuestionOptions.index'

])
    @php


        $isServiceCategoriesActive = Request::is($urlAdmin.'*services/serviceCategories*');
        $isServiceCategoryLangsActive = Request::is($urlAdmin.'*services/serviceCategoryLangs*');
        $isServicesActive               = Request::is($urlAdmin.'*services/services*');
        $isServiceLangsActive = Request::is($urlAdmin.'*services/serviceLangs*');
        $isServiceTypesActive = Request::is($urlAdmin.'*services/serviceTypes*');
        $isServiceTypeLangsActive = Request::is($urlAdmin.'*services/serviceTypeLangs*');
        //update
        $isServiceTypeQuestionsActive = Request::is($urlAdmin.'*services/serviceTypeQuestions*');
        $isServiceTypeQuestionLangsActive = Request::is($urlAdmin.'*services/serviceTypeQuestionLangs*');
        $isServiceTypeQuestionOptionsActive = Request::is($urlAdmin.'*services/serviceTypeQuestionOptions*');


    @endphp
    <li class="nav-item {{($isServiceCategoriesActive||$isServiceCategoryLangsActive||
        $isServicesActive||$isServiceLangsActive||$isServiceTypesActive||$isServiceTypeLangsActive
        ||$isServiceTypeQuestionsActive||$isServiceTypeQuestionLangsActive||$isServiceTypeQuestionOptionsActive
        )?'menu-open':''}} ">
        <a href="#" class="nav-link">
            {{--            <i class="nav-icon fas fa-sliders-h"></i>--}}
            {{--            <i class="nav-icon fas fa-wrench"></i>--}}
            <i class="fav-icon fas fa-solar-panel"></i>
            <p>
                Services
                <i class="nav-icon  fas  fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">


            @can('services.serviceCategories.index')

                <li class="nav-item">
                    <a href="{{ route('services.serviceCategories.index') }}"
                       class="nav-link {{ $isServiceCategoriesActive ? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-solar-panel"></i>
                        <p>@lang('models/serviceCategories.plural')</p>
                    </a>
                </li>
            @endcan
            @can('services.serviceCategoryLangs.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceCategoryLangs.index') }}"
                       class="nav-link {{ $isServiceCategoryLangsActive? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-language"></i>
                        <p>@lang('models/serviceCategoryLangs.plural')</p>
                    </a>
                </li>

            @endcan
            @can('services.services.index')
                <li class="nav-item">
                    <a href="{{ route('services.services.index') }}"
                       class="nav-link {{ $isServicesActive? 'active' : '' }}">
                        <i class="fab fa-servicestack"></i>
                        <p>@lang('models/services.plural')</p>
                    </a>
                </li>
            @endcan
            @can('services.serviceLangs.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceLangs.index') }}"
                       class="nav-link {{ $isServiceLangsActive ? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-language"></i>
                        <p>@lang('models/serviceLangs.plural')</p>
                    </a>
                </li>
            @endcan
            @can('services.serviceTypes.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceTypes.index') }}"
                       class="nav-link {{ $isServiceTypesActive? 'active' : '' }}">
                        <i class="fab fa-usps"></i>
                        <p>@lang('models/serviceTypes.plural')</p>
                    </a>
                </li>
            @endcan
            @can('services.serviceTypeLangs.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceTypeLangs.index') }}"
                       class="nav-link {{ $isServiceTypeLangsActive ? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-language"></i>
                        <p>@lang('models/serviceTypeLangs.plural')</p>
                    </a>
                </li>
            @endcan



            @can('services.serviceTypeQuestions.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceTypeQuestions.index') }}"
                       class="nav-link {{ $isServiceTypeQuestionsActive ? 'active' : '' }}">
                        <i class="nav-icon far fa-question-circle"></i>
                        <p>@lang('models/serviceTypeQuestions.plural')</p>
                    </a>
                </li>
            @endcan

            @can('services.serviceTypeQuestionLangs.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceTypeQuestionLangs.index') }}"
                       class="nav-link {{ $isServiceTypeQuestionLangsActive ? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-language"></i>
                        <p>@lang('models/serviceTypeQuestionLangs.plural')</p>
                    </a>
                </li>
            @endcan
            @can('services.serviceTypeQuestionOptions.index')
                <li class="nav-item">
                    <a href="{{ route('services.serviceTypeQuestionOptions.index') }}"
                       class="nav-link {{$isServiceTypeQuestionOptionsActive ? 'active' : '' }}">
{{--                        <i class="nav-icon  fa fa-question"></i>--}}
                        <i class="nav-icon fab fa-optin-monster"></i>
                        <p>@lang('models/serviceTypeQuestionOptions.plural')</p>
                    </a>
                </li>

            @endcan


        </ul>
    </li>
@endcan







{{--Questions--}}
@canany(['questions.questionTypes.index','questions.questionTypeOptions.index',
'questions.predefinedQuestions.index','questions.predefinedQuestionOptions.index','questions.predefinedQuestionLangs.index'])
    @php

              $isQuestionTypesActive = Request::is($urlAdmin.'*questions/questionTypes*');
              $isQuestionTypeOptionsActive = Request::is($urlAdmin.'*questions/questionTypeOptions*');
              $isPredefinedQuestionsActive = Request::is($urlAdmin.'*questions/predefinedQuestions*');
              $isPredefinedQuestionOptionsActive = Request::is($urlAdmin.'*questions/predefinedQuestionOptions*');
              $isPredefinedQuestionLangsActive = Request::is($urlAdmin.'*questions/predefinedQuestionLangs*');



    @endphp
    <li class="nav-item {{($isQuestionTypesActive||$isQuestionTypeOptionsActive||
        $isPredefinedQuestionsActive||$isPredefinedQuestionOptionsActive||$isPredefinedQuestionLangsActive)?'menu-open':''}} ">
        <a href="#" class="nav-link">

            <i class="nav-icon  fa fa-question"></i>
            <p>
                Questions
                <i class="nav-icon  fas  fa-angle-left right"></i>

            </p>
        </a>
        <ul class="nav nav-treeview">


            @can('questions.questionTypes.index')
                <li class="nav-item">
                    <a href="{{ route('questions.questionTypes.index') }}"
                       class="nav-link {{$isQuestionTypesActive? 'active' : '' }}">
                        <i class="nav-icon far fa-question-circle"></i>
                        <p>@lang('models/questionTypes.plural')</p>
                    </a>
                </li>

            @endcan

            @can('questions.questionTypeOptions.index')
                <li class="nav-item">
                    <a href="{{ route('questions.questionTypeOptions.index') }}"
                       class="nav-link {{$isQuestionTypeOptionsActive? 'active' : '' }}">
                        <i class="nav-icon fab fa-optin-monster"></i>
                        <p>@lang('models/questionTypeOptions.plural')</p>
                    </a>
                </li>

            @endcan

            @can('questions.predefinedQuestions.index')
                <li class="nav-item">
                    <a href="{{ route('questions.predefinedQuestions.index') }}"
                       class="nav-link {{ $isPredefinedQuestionsActive? 'active' : '' }}">
                        <i class="nav-icon fas fa-question-circle"></i>
                        <p>@lang('models/predefinedQuestions.plural')</p>
                    </a>
                </li>

            @endcan
            @can('questions.predefinedQuestionOptions.index')
                <li class="nav-item">
                    <a href="{{ route('questions.predefinedQuestionOptions.index') }}"
                       class="nav-link {{ $isPredefinedQuestionOptionsActive? 'active' : '' }}">
                        <i class="nav-icon fab fa-optin-monster"></i>
                        <p>@lang('models/predefinedQuestionOptions.plural')</p>
                    </a>
                </li>
            @endcan
            @can('questions.predefinedQuestionLangs.index')
                <li class="nav-item">
                    <a href="{{ route('questions.predefinedQuestionLangs.index') }}"
                       class="nav-link {{$isPredefinedQuestionLangsActive? 'active' : '' }}">
                        <i class="nav-icon  fas  fa-language"></i>
                        <p>@lang('models/predefinedQuestionLangs.plural')</p>
                    </a>
                </li>

            @endcan

        </ul>
    </li>
@endcan































