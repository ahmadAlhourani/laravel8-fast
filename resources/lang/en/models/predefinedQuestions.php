<?php

return array (
  'singular' => 'PredefinedQuestion',
  'plural' => 'PredefinedQuestions',
  'fields' => 
  array (
    'id' => 'Id',
    'question_type_id' => 'Question Type Id',
    'question' => 'Question',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
