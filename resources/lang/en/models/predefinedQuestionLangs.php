<?php

return array (
  'singular' => 'PredefinedQuestionLang',
  'plural' => 'PredefinedQuestionLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'predefined_question_id' => 'Predefined Question Id',
    'lang_id' => 'Lang Id',
    'question' => 'Question',
    'note' => 'Note',
    'answer_example' => 'Answer Example',
    'instructions' => 'Instructions',
    'default_answer' => 'Default Answer',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
