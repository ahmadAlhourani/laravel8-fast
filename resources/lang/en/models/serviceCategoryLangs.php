<?php

return array (
  'singular' => 'ServiceCategoryLang',
  'plural' => 'ServiceCategoryLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'service_category_id' => 'Service Category Id',
    'lang_id' => 'Lang Id',
    'name' => 'Name',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
