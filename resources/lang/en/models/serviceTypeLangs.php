<?php

return array (
  'singular' => 'ServiceTypeLang',
  'plural' => 'ServiceTypeLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'service_id' => 'Service Id',
    'lang_id' => 'Lang Id',
    'name' => 'Name',
    'short_description' => 'Short Description',
    'description' => 'Description',
    'html_description' => 'Html Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
