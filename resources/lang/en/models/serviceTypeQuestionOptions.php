<?php

return array (
  'singular' => 'ServiceTypeQuestionOption',
  'plural' => 'ServiceTypeQuestionOptions',
  'fields' => 
  array (
    'id' => 'Id',
    'service_type_question_id' => 'Service Type Question Id',
    'question_type_option_id' => 'Question Type Option Id',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
