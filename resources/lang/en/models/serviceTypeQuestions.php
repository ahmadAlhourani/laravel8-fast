<?php

return array (
  'singular' => 'ServiceTypeQuestion',
  'plural' => 'ServiceTypeQuestions',
  'fields' => 
  array (
    'id' => 'Id',
    'question' => 'Question',
    'service_type_id' => 'Service Type Id',
    'question_type_id' => 'Question Type Id',
    'required' => 'Required',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
