<?php

return array (
  'singular' => 'ServiceLang',
  'plural' => 'ServiceLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'service_id' => 'Service Id',
    'lang_id' => 'Lang Id',
    'name' => 'Name',
    'short_description' => 'Short Description',
    'description' => 'Description',
    'html_description' => 'Html Description',
    'question_service_type' => 'Question Service Type',
    'types_difference' => 'Types Difference',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
