<?php

return array (
  'singular' => 'SettingCategory',
  'plural' => 'SettingCategories',
  'fields' => 
  array (
    'id' => 'Id',
    'key' => 'Key',
    'value' => 'Value',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
