<?php

return array (
  'singular' => 'SYSLog',
  'plural' => 'SYSLogs',
  'fields' => 
  array (
    'id' => 'Id',
    'msg' => 'Msg',
    'module' => 'Module',
    'function' => 'Function',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
