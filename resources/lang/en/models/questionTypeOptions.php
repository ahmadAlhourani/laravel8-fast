<?php

return array (
  'singular' => 'QuestionTypeOption',
  'plural' => 'QuestionTypeOptions',
  'fields' => 
  array (
    'id' => 'Id',
    'question_type_id' => 'Question Type Id',
    'option' => 'Option',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
