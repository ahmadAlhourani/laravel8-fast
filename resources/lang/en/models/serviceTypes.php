<?php

return array (
  'singular' => 'ServiceType',
  'plural' => 'ServiceTypes',
  'fields' => 
  array (
    'id' => 'Id',
    'service_id' => 'Service Id',
    'name' => 'Name',
    'icon' => 'Icon',
    'rank' => 'Rank',
    'price' => 'Price',
    'status' => 'Status',
    'short_description' => 'Short Description',
    'description' => 'Description',
    'html_description' => 'Html Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
