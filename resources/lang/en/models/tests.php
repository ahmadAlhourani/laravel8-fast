<?php

return array (
  'singular' => 'Test',
  'plural' => 'Tests',
  'fields' => 
  array (
    'id' => 'Id',
    'id:bigIncrements' => 'Id:bigincrements',
    'name:string' => 'Name:string',
    'lang_id' => 'Lang Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
