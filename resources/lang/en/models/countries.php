<?php

return array (
  'singular' => 'Country',
  'plural' => 'Countries',
  'fields' =>
  array (
    'id' => 'Id',
    'iso' => 'Iso',
    'name' => 'Name',
    'nice_name' => 'nice_name',
    'iso3' => 'Iso3',
    'num_code' => 'num_code',
    'phone_code' => 'phone_code',
  ),
);
