<?php

return array (
  'singular' => 'Book',
  'plural' => 'Books',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
