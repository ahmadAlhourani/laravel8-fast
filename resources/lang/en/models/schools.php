<?php

return array (
  'singular' => 'School',
  'plural' => 'Schools',
  'fields' => 
  array (
    'name' => 'Name',
    'id' => 'Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
