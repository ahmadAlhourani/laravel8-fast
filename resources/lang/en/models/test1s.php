<?php

return array (
  'singular' => 'Test1',
  'plural' => 'Test1s',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
