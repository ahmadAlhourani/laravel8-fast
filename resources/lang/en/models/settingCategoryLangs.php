<?php

return array (
  'singular' => 'SettingCategoryLang',
  'plural' => 'SettingCategoryLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'service_categry_id' => 'Service Categry Id',
    'lang_id' => 'Lang Id',
    'name' => 'Name',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
