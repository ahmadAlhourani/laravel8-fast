<?php

return array (
  'singular' => 'Ahmad',
  'plural' => 'Ahmads',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'lang_id' => 'Lang Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
