<?php

return array (
  'singular' => 'ServiceCategory',
  'plural' => 'ServiceCategories',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'description' => 'Description',
    'icon' => 'Icon',
    'rank' => 'Rank',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
