<?php

return array (
  'singular' => 'Student',
  'plural' => 'Students',
  'fields' => 
  array (
    'id' => 'Id',
    's_name' => 'S Name',
    'des' => 'Des',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
