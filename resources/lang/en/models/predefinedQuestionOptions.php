<?php

return array (
  'singular' => 'PredefinedQuestionOption',
  'plural' => 'PredefinedQuestionOptions',
  'fields' => 
  array (
    'id' => 'Id',
    'predefined_question_id' => 'Predefined Question Id',
    'question_type_option_id' => 'Question Type Option Id',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
