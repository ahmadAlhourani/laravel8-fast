<?php

return array (
  'singular' => 'Setting',
  'plural' => 'Settings',
  'fields' => 
  array (
    'id' => 'Id',
    'key' => 'Key',
    'operator' => 'Operator',
    'value' => 'Value',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'setting_category_id' => 'Setting Category Id',
  ),
);
