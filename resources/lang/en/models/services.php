<?php

return array (
  'singular' => 'Service',
  'plural' => 'Services',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'service_category_id' => 'Service Category Id',
    'short_description' => 'Short Description',
    'icon' => 'Icon',
    'rank' => 'Rank',
    'description' => 'Description',
    'html_description' => 'Html Description',
    'question_service_type' => 'Question Service Type',
    'types_difference' => 'Types Difference',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
