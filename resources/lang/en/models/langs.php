<?php

return array (
  'singular' => 'Lang',
  'plural' => 'Langs',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'iso' => 'Iso',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
