<?php

return array (
  'singular' => 'Alert',
  'plural' => 'Alerts',
  'fields' => 
  array (
    'id' => 'Id',
    'state' => 'State',
    'module' => 'Module',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
