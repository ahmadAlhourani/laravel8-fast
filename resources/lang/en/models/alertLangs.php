<?php

return array (
  'singular' => 'AlertLang',
  'plural' => 'AlertLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'alert_id' => 'Alert Id',
    'lang_id' => 'Lang Id',
    'name' => 'Name',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
