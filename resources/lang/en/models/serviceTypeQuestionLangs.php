<?php

return array (
  'singular' => 'ServiceTypeQuestionLang',
  'plural' => 'ServiceTypeQuestionLangs',
  'fields' => 
  array (
    'id' => 'Id',
    'question' => 'Question',
    'service_type_question_id' => 'Service Type Question Id',
    'lang_id' => 'Lang Id',
    'note' => 'Note',
    'answer_example' => 'Answer Example',
    'instructions' => 'Instructions',
    'default_answer' => 'Default Answer',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
