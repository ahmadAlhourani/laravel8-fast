<?php

return array (
  'singular' => 'Log',
  'plural' => 'Logs',
  'fields' =>
  array (
    'id' => 'Id',
    'msg' => 'Msg',
    'module' => 'Module',
    'function' => 'Function',
    'method' => 'Method',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
