<?php

return array (
  'singular' => 'QuestionType',
  'plural' => 'QuestionTypes',
  'fields' => 
  array (
    'id' => 'Id',
    'key' => 'Key',
    'name' => 'Name',
    'description' => 'Description',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
