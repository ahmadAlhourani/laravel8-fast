<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class UserAccessible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,...$guards)
    {
        $user = Auth::user();
        if(!$user->checkUserPermission("web")){
            return redirect(config('fast.admin_prefix').RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
