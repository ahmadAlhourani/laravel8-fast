<?php

namespace App\Http\Controllers;

use App\Models\Directory\Alert;
use App\Models\Directory\Log;
use App\Traits\PagerTrait;
use Doctrine\DBAL\Exception;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Utils\ResponseUtil;


use Response;


class AppBaseController extends Controller
{
    use PagerTrait;


    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message,
        ], 200);
    }

    public function sendTrue($data = [], $message = "Success", $code = 200)
    {
        return Response::json([
            'success' => true,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function sendFalse($data = [], $message = "", $code = 200)
    {
        if (!$message) {
            $message = $this->msg(Auth::user()->defaultLang(), 1);

        }
        return Response::json([
            'success' => false,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function invalidInput($validateErrors)
    {
        $e = new Exception();
        $trace = $e->getTrace();
        //position 0 would be the line that called this function so we ignore it
        $lastCall = $trace[1] ?? [];
        $last2Call = $trace[2] ?? [];
        $log = Log::saveLog(['message' => $validateErrors, "exception" => $last2Call], $lastCall["class"] ?? get_class($this), $lastCall["function"] ?? __FUNCTION__);
        //Invalid Input
        $msg = $this->msg(Auth::user()->defaultLang(), 5);
        return $this->sendFalse(["error_num" => $log->id], $msg);
    }

    public function serverError($exception)
    {
        $e = new Exception();
        $trace = $e->getTrace();
        //position 0 would be the line that called this function so we ignore it
        $lastCall = $trace[1] ?? [];
        $last2Call = $trace[2] ?? [];
        $log = Log::saveLog(['message' => $exception->getMessage(), "exception" => $last2Call], $lastCall["class"] ?? get_class($this), $lastCall["function"] ?? __FUNCTION__);

        //Server Error
        $msg = $this->msg(Auth::user()->defaultLang(), 1);
        return $this->sendFalse(["error_num" => $log->id], $msg, 500);

    }


    public function msg(int $langId = 1, int $state = 0)
    {

        $message = Alert::
        join('alert_langs', 'alerts.id', '=', 'alert_langs.alert_id')
            ->select('alert_langs.name')
            ->where('alerts.state', '=', $state)
            ->where('alert_langs.lang_id', '=', $langId)
            ->first();
        if ($message)
            return $message->name;

        return "Error";

    }


}
