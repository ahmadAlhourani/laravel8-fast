<?php

namespace App\Http\Controllers\Settings;

use App\DataTables\Settings\SettingCategoryDataTable;
use App\Http\Requests\Settings;
use App\Http\Requests\Settings\CreateSettingCategoryRequest;
use App\Http\Requests\Settings\UpdateSettingCategoryRequest;
use App\Repositories\Settings\SettingCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SettingCategoryController extends AppBaseController
{
    /** @var  SettingCategoryRepository */
    private $settingCategoryRepository;

    public function __construct(SettingCategoryRepository $settingCategoryRepo)
    {
        $this->settingCategoryRepository = $settingCategoryRepo;
    }

    /**
     * Display a listing of the SettingCategory.
     *
     * @param SettingCategoryDataTable $settingCategoryDataTable
     * @return Response
     */
    public function index(SettingCategoryDataTable $settingCategoryDataTable)
    {
        return $settingCategoryDataTable->render('settings.setting_categories.index');
    }

    /**
     * Show the form for creating a new SettingCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings.setting_categories.create');
    }

    /**
     * Store a newly created SettingCategory in storage.
     *
     * @param CreateSettingCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingCategoryRequest $request)
    {
        $input = $request->all();

        $settingCategory = $this->settingCategoryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/settingCategories.singular')]));

        return redirect(route('settings.settingCategories.index'));
    }

    /**
     * Display the specified SettingCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $settingCategory = $this->settingCategoryRepository->find($id);

        if (empty($settingCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/settingCategories.singular')]));

            return redirect(route('settings.settingCategories.index'));
        }

        return view('settings.setting_categories.show')->with('settingCategory', $settingCategory);
    }

    /**
     * Show the form for editing the specified SettingCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $settingCategory = $this->settingCategoryRepository->find($id);

        if (empty($settingCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/settingCategories.singular')]));

            return redirect(route('settings.settingCategories.index'));
        }

        return view('settings.setting_categories.edit')->with('settingCategory', $settingCategory);
    }

    /**
     * Update the specified SettingCategory in storage.
     *
     * @param  int              $id
     * @param UpdateSettingCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSettingCategoryRequest $request)
    {
        $settingCategory = $this->settingCategoryRepository->find($id);

        if (empty($settingCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/settingCategories.singular')]));

            return redirect(route('settings.settingCategories.index'));
        }

        $settingCategory = $this->settingCategoryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/settingCategories.singular')]));

        return redirect(route('settings.settingCategories.index'));
    }

    /**
     * Remove the specified SettingCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $settingCategory = $this->settingCategoryRepository->find($id);

        if (empty($settingCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/settingCategories.singular')]));

            return redirect(route('settings.settingCategories.index'));
        }

        $this->settingCategoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/settingCategories.singular')]));

        return redirect(route('settings.settingCategories.index'));
    }
}
