<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceTypeLangDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceTypeLangRequest;
use App\Http\Requests\Services\UpdateServiceTypeLangRequest;
use App\Repositories\Services\ServiceTypeLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceTypeLangController extends AppBaseController
{
    /** @var  ServiceTypeLangRepository */
    private $serviceTypeLangRepository;

    public function __construct(ServiceTypeLangRepository $serviceTypeLangRepo)
    {
        $this->serviceTypeLangRepository = $serviceTypeLangRepo;
    }

    /**
     * Display a listing of the ServiceTypeLang.
     *
     * @param ServiceTypeLangDataTable $serviceTypeLangDataTable
     * @return Response
     */
    public function index(ServiceTypeLangDataTable $serviceTypeLangDataTable,$serviceTypeId=0)
    {
        if($serviceTypeId){
            $serviceTypeLangDataTable->addFilters("service_type_id","=",$serviceTypeId);
        }
        return $serviceTypeLangDataTable->render('services.service_type_langs.index');
    }

    /**
     * Show the form for creating a new ServiceTypeLang.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_type_langs.create');
    }

    /**
     * Store a newly created ServiceTypeLang in storage.
     *
     * @param CreateServiceTypeLangRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeLangRequest $request)
    {
        $input = $request->all();

        $serviceTypeLang = $this->serviceTypeLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceTypeLangs.singular')]));
        return redirect(route('services.serviceTypeLangs',$serviceTypeLang->service_type_id));
        return redirect(route('services.serviceTypeLangs.index'));
    }

    /**
     * Display the specified ServiceTypeLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceTypeLang = $this->serviceTypeLangRepository->find($id);

        if (empty($serviceTypeLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeLangs.singular')]));

            return redirect(route('services.serviceTypeLangs.index'));
        }

        return view('services.service_type_langs.show')->with('serviceTypeLang', $serviceTypeLang);
    }

    /**
     * Show the form for editing the specified ServiceTypeLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceTypeLang = $this->serviceTypeLangRepository->find($id);

        if (empty($serviceTypeLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeLangs.singular')]));

            return redirect(route('services.serviceTypeLangs.index'));
        }

        return view('services.service_type_langs.edit')->with('serviceTypeLang', $serviceTypeLang);
    }

    /**
     * Update the specified ServiceTypeLang in storage.
     *
     * @param  int              $id
     * @param UpdateServiceTypeLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeLangRequest $request)
    {
        $serviceTypeLang = $this->serviceTypeLangRepository->find($id);

        if (empty($serviceTypeLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeLangs.singular')]));

            return redirect(route('services.serviceTypeLangs.index'));
        }

        $serviceTypeLang = $this->serviceTypeLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceTypeLangs.singular')]));

        return redirect(route('services.serviceTypeLangs',$serviceTypeLang->service_type_id));
        return redirect(route('services.serviceTypeLangs.index'));
    }

    /**
     * Remove the specified ServiceTypeLang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceTypeLang = $this->serviceTypeLangRepository->find($id);

        if (empty($serviceTypeLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeLangs.singular')]));

            return redirect(route('services.serviceTypeLangs.index'));
        }

        $this->serviceTypeLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceTypeLangs.singular')]));

        return redirect(route('services.serviceTypeLangs.index'));
    }
}
