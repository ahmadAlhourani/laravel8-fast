<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceTypeDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceTypeRequest;
use App\Http\Requests\Services\UpdateServiceTypeRequest;
use App\Repositories\Services\ServiceTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Response;

class ServiceTypeController extends AppBaseController
{
    /** @var  ServiceTypeRepository */
    private $serviceTypeRepository;

    public function __construct(ServiceTypeRepository $serviceTypeRepo)
    {
        $this->serviceTypeRepository = $serviceTypeRepo;
    }

    /**
     * Display a listing of the ServiceType.
     *
     * @param ServiceTypeDataTable $serviceTypeDataTable
     * @return Response
     */
    public function index(ServiceTypeDataTable $serviceTypeDataTable,$serviceId=0)
    {
        if($serviceId)
            $serviceTypeDataTable->addFilters("service_id","=",$serviceId);
        return $serviceTypeDataTable->render('services.service_types.index');
    }

    /**
     * Show the form for creating a new ServiceType.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_types.create');
    }

    /**
     * Store a newly created ServiceType in storage.
     *
     * @param CreateServiceTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeRequest $request)
    {
        $input = $request->all();
        if (isset($input['icon'] ) && $input['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $input['icon'] = config('global.service_images') . basename($coverImage);
        }

        $serviceType = $this->serviceTypeRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceTypes.singular')]));

        return redirect(route('services.serviceTypes',$serviceType->service_id));
        return redirect(route('services.serviceTypes.index'));
    }

    /**
     * Display the specified ServiceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('services.serviceTypes.index'));
        }

        return view('services.service_types.show')->with('serviceType', $serviceType);
    }

    /**
     * Show the form for editing the specified ServiceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('services.serviceTypes.index'));
        }

        return view('services.service_types.edit')->with('serviceType', $serviceType);
    }

    /**
     * Update the specified ServiceType in storage.
     *
     * @param  int              $id
     * @param UpdateServiceTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeRequest $request)
    {
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('services.serviceTypes.index'));
        }
        $data=$request->all();
        if (isset($data['icon'] ) && $data['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $data['icon'] = config('global.service_images') . basename($coverImage);
        }
        if(!isset($data["status"])){
            $data["status"]=0;
        }

        if(!isset($data["show_price"])){
            $data["show_price"]=0;
        }
        $serviceType = $this->serviceTypeRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceTypes.singular')]));
        return redirect(route('services.serviceTypes',$serviceType->service_id));
        return redirect(route('services.serviceTypes.index'));
    }

    /**
     * Remove the specified ServiceType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceType = $this->serviceTypeRepository->find($id);

        if (empty($serviceType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypes.singular')]));

            return redirect(route('services.serviceTypes.index'));
        }

        $this->serviceTypeRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceTypes.singular')]));

        return redirect(route('services.serviceTypes.index'));
    }
}
