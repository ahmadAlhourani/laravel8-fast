<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceTypeQuestionLangDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceTypeQuestionLangRequest;
use App\Http\Requests\Services\UpdateServiceTypeQuestionLangRequest;
use App\Repositories\Services\ServiceTypeQuestionLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceTypeQuestionLangController extends AppBaseController
{
    /** @var  ServiceTypeQuestionLangRepository */
    private $serviceTypeQuestionLangRepository;

    public function __construct(ServiceTypeQuestionLangRepository $serviceTypeQuestionLangRepo)
    {
        $this->serviceTypeQuestionLangRepository = $serviceTypeQuestionLangRepo;
    }

    /**
     * Display a listing of the ServiceTypeQuestionLang.
     *
     * @param ServiceTypeQuestionLangDataTable $serviceTypeQuestionLangDataTable
     * @return Response
     */
    public function index(ServiceTypeQuestionLangDataTable $serviceTypeQuestionLangDataTable, $serviceTypeQuestionId = null)
    {
        if ($serviceTypeQuestionId) {
            $serviceTypeQuestionLangDataTable->addFilters("service_type_question_id", "=", $serviceTypeQuestionId);
        }
        return $serviceTypeQuestionLangDataTable->render('services.service_type_question_langs.index');
    }

    /**
     * Show the form for creating a new ServiceTypeQuestionLang.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_type_question_langs.create');
    }

    /**
     * Store a newly created ServiceTypeQuestionLang in storage.
     *
     * @param CreateServiceTypeQuestionLangRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeQuestionLangRequest $request)
    {
        $input = $request->all();

        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

        return redirect(route('services.serviceTypeQuestionLangs',$serviceTypeQuestionLang->service_type_question_id));
        return redirect(route('services.serviceTypeQuestionLangs.index'));
    }

    /**
     * Display the specified ServiceTypeQuestionLang.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->find($id);

        if (empty($serviceTypeQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

            return redirect(route('services.serviceTypeQuestionLangs.index'));
        }

        return view('services.service_type_question_langs.show')->with('serviceTypeQuestionLang', $serviceTypeQuestionLang);
    }

    /**
     * Show the form for editing the specified ServiceTypeQuestionLang.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->find($id);

        if (empty($serviceTypeQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

            return redirect(route('services.serviceTypeQuestionLangs.index'));
        }

        return view('services.service_type_question_langs.edit')->with('serviceTypeQuestionLang', $serviceTypeQuestionLang);
    }

    /**
     * Update the specified ServiceTypeQuestionLang in storage.
     *
     * @param int $id
     * @param UpdateServiceTypeQuestionLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeQuestionLangRequest $request)
    {
        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->find($id);

        if (empty($serviceTypeQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

            return redirect(route('services.serviceTypeQuestionLangs.index'));
        }

        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceTypeQuestionLangs.singular')]));
        return redirect(route('services.serviceTypeQuestionLangs',$serviceTypeQuestionLang->service_type_question_id));
        return redirect(route('services.serviceTypeQuestionLangs.index'));
    }

    /**
     * Remove the specified ServiceTypeQuestionLang from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceTypeQuestionLang = $this->serviceTypeQuestionLangRepository->find($id);

        if (empty($serviceTypeQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

            return redirect(route('services.serviceTypeQuestionLangs.index'));
        }

        $this->serviceTypeQuestionLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceTypeQuestionLangs.singular')]));

        return redirect(route('services.serviceTypeQuestionLangs.index'));
    }
}
