<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceCategoryLangDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceCategoryLangRequest;
use App\Http\Requests\Services\UpdateServiceCategoryLangRequest;
use App\Repositories\Services\ServiceCategoryLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceCategoryLangController extends AppBaseController
{
    /** @var  ServiceCategoryLangRepository */
    private $serviceCategoryLangRepository;

    public function __construct(ServiceCategoryLangRepository $serviceCategoryLangRepo)
    {
        $this->serviceCategoryLangRepository = $serviceCategoryLangRepo;
    }

    /**
     * Display a listing of the ServiceCategoryLang.
     *
     * @param ServiceCategoryLangDataTable $serviceCategoryLangDataTable
     * @return Response
     */
    public function index(ServiceCategoryLangDataTable $serviceCategoryLangDataTable,$serviceCategoryId=null)
    {
//        var_dump($serviceCategoryId);
//        die();
        if($serviceCategoryId)
            $serviceCategoryLangDataTable->addFilters("service_category_id","=",$serviceCategoryId);
        return $serviceCategoryLangDataTable->render('services.service_category_langs.index');
    }

    /**
     * Show the form for creating a new ServiceCategoryLang.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_category_langs.create');
    }

    /**
     * Store a newly created ServiceCategoryLang in storage.
     *
     * @param CreateServiceCategoryLangRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceCategoryLangRequest $request)
    {
        $input = $request->all();

        $serviceCategoryLang = $this->serviceCategoryLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceCategoryLangs.singular')]));

        return redirect(route('services.categoryLangs',$serviceCategoryLang->service_category_id));
        return redirect(route('services.serviceCategoryLangs.index'));
    }

    /**
     * Display the specified ServiceCategoryLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceCategoryLang = $this->serviceCategoryLangRepository->find($id);

        if (empty($serviceCategoryLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategoryLangs.singular')]));

            return redirect(route('services.serviceCategoryLangs.index'));
        }

        return view('services.service_category_langs.show')->with('serviceCategoryLang', $serviceCategoryLang);
    }

    /**
     * Show the form for editing the specified ServiceCategoryLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceCategoryLang = $this->serviceCategoryLangRepository->find($id);

        if (empty($serviceCategoryLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategoryLangs.singular')]));

            return redirect(route('services.serviceCategoryLangs.index'));
        }

        return view('services.service_category_langs.edit')->with('serviceCategoryLang', $serviceCategoryLang);
    }

    /**
     * Update the specified ServiceCategoryLang in storage.
     *
     * @param  int              $id
     * @param UpdateServiceCategoryLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceCategoryLangRequest $request)
    {
        $serviceCategoryLang = $this->serviceCategoryLangRepository->find($id);

        if (empty($serviceCategoryLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategoryLangs.singular')]));

            return redirect(route('services.serviceCategoryLangs.index'));
        }

        $serviceCategoryLang = $this->serviceCategoryLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceCategoryLangs.singular')]));

        return redirect(route('services.categoryLangs',$serviceCategoryLang->service_category_id));
        return redirect(route('services.serviceCategoryLangs.index',$serviceCategoryLang->service_category_id));
    }

    /**
     * Remove the specified ServiceCategoryLang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceCategoryLang = $this->serviceCategoryLangRepository->find($id);

        if (empty($serviceCategoryLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategoryLangs.singular')]));

            return redirect(route('services.serviceCategoryLangs.index'));
        }

        $this->serviceCategoryLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceCategoryLangs.singular')]));

        return redirect(route('services.serviceCategoryLangs.index'));
    }
}
