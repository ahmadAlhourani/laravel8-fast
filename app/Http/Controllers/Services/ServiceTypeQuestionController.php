<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceTypeQuestionDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceTypeQuestionRequest;
use App\Http\Requests\Services\UpdateServiceTypeQuestionRequest;
use App\Repositories\Services\ServiceTypeQuestionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceTypeQuestionController extends AppBaseController
{
    /** @var  ServiceTypeQuestionRepository */
    private $serviceTypeQuestionRepository;

    public function __construct(ServiceTypeQuestionRepository $serviceTypeQuestionRepo)
    {
        $this->serviceTypeQuestionRepository = $serviceTypeQuestionRepo;
    }

    /**
     * Display a listing of the ServiceTypeQuestion.
     *
     * @param ServiceTypeQuestionDataTable $serviceTypeQuestionDataTable
     * @return Response
     */
    public function index(ServiceTypeQuestionDataTable $serviceTypeQuestionDataTable,$serviceTypeId=0)
    {
        if($serviceTypeId){
            $serviceTypeQuestionDataTable->addFilters("service_type_id","=",$serviceTypeId);
        }
        return $serviceTypeQuestionDataTable->render('services.service_type_questions.index');
    }

    /**
     * Show the form for creating a new ServiceTypeQuestion.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_type_questions.create');
    }

    /**
     * Store a newly created ServiceTypeQuestion in storage.
     *
     * @param CreateServiceTypeQuestionRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeQuestionRequest $request)
    {
        $input = $request->all();

        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceTypeQuestions.singular')]));

        return redirect(route('services.serviceTypeQuestions',$serviceTypeQuestion->service_type_id));
        return redirect(route('services.serviceTypeQuestions.index'));
    }

    /**
     * Display the specified ServiceTypeQuestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->find($id);

        if (empty($serviceTypeQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestions.singular')]));

            return redirect(route('services.serviceTypeQuestions.index'));
        }

        return view('services.service_type_questions.show')->with('serviceTypeQuestion', $serviceTypeQuestion);
    }

    /**
     * Show the form for editing the specified ServiceTypeQuestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->find($id);

        if (empty($serviceTypeQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestions.singular')]));

            return redirect(route('services.serviceTypeQuestions.index'));
        }

        return view('services.service_type_questions.edit')->with('serviceTypeQuestion', $serviceTypeQuestion);
    }

    /**
     * Update the specified ServiceTypeQuestion in storage.
     *
     * @param  int              $id
     * @param UpdateServiceTypeQuestionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeQuestionRequest $request)
    {
        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->find($id);

        if (empty($serviceTypeQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestions.singular')]));

            return redirect(route('services.serviceTypeQuestions.index'));
        }

        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceTypeQuestions.singular')]));
        return redirect(route('services.serviceTypeQuestions',$serviceTypeQuestion->service_type_id));
        return redirect(route('services.serviceTypeQuestions.index'));
    }

    /**
     * Remove the specified ServiceTypeQuestion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceTypeQuestion = $this->serviceTypeQuestionRepository->find($id);

        if (empty($serviceTypeQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestions.singular')]));

            return redirect(route('services.serviceTypeQuestions.index'));
        }

        $this->serviceTypeQuestionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceTypeQuestions.singular')]));

        return redirect(route('services.serviceTypeQuestions.index'));
    }
}
