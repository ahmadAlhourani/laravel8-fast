<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceCategoryDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceCategoryRequest;
use App\Http\Requests\Services\UpdateServiceCategoryRequest;
use App\Repositories\Services\ServiceCategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Response;

class ServiceCategoryController extends AppBaseController
{
    /** @var  ServiceCategoryRepository */
    private $serviceCategoryRepository;

    public function __construct(ServiceCategoryRepository $serviceCategoryRepo)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepo;
    }

    /**
     * Display a listing of the ServiceCategory.
     *
     * @param ServiceCategoryDataTable $serviceCategoryDataTable
     * @return Response
     */
    public function index(ServiceCategoryDataTable $serviceCategoryDataTable)
    {

        return $serviceCategoryDataTable->render('services.service_categories.index');
    }

    /**
     * Show the form for creating a new ServiceCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_categories.create');
    }

    /**
     * Store a newly created ServiceCategory in storage.
     *
     * @param CreateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceCategoryRequest $request)
    {
        $data = $request->all();

        if (isset($data['icon'] ) && $data['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $data['icon'] = config('global.service_images') . basename($coverImage);
        }

        $serviceCategory = $this->serviceCategoryRepository->create($data);

        Flash::success(__('messages.saved', ['model' => __('models/serviceCategories.singular')]));

        return redirect(route('services.serviceCategories.index'));
    }

    /**
     * Display the specified ServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('services.serviceCategories.index'));
        }

        return view('services.service_categories.show')->with('serviceCategory', $serviceCategory);
    }

    /**
     * Show the form for editing the specified ServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('services.serviceCategories.index'));
        }

        return view('services.service_categories.edit')->with('serviceCategory', $serviceCategory);
    }

    /**
     * Update the specified ServiceCategory in storage.
     *
     * @param  int              $id
     * @param UpdateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceCategoryRequest $request)
    {
        $serviceCategory = $this->serviceCategoryRepository->find($id);


        if (empty($serviceCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('services.serviceCategories.index'));
        }
        $data=$request->all();
        if (isset($data['icon'] ) && $data['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $data['icon'] = config('global.service_images') . basename($coverImage);
        }
        if(!isset($data["status"])){
            $data["status"]=0;
        }

        $serviceCategory = $this->serviceCategoryRepository->update($data, $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceCategories.singular')]));

        return redirect(route('services.serviceCategories.index'));
    }

    /**
     * Remove the specified ServiceCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceCategory = $this->serviceCategoryRepository->find($id);

        if (empty($serviceCategory)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceCategories.singular')]));

            return redirect(route('services.serviceCategories.index'));
        }

        $this->serviceCategoryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceCategories.singular')]));

        return redirect(route('services.serviceCategories.index'));
    }
}
