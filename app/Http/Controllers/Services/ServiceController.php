<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceRequest;
use App\Http\Requests\Services\UpdateServiceRequest;
use App\Models\Services\Service;
use App\Repositories\Services\ServiceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Response;
use function Illuminate\Events\queueable;

class ServiceController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepo)
    {
        $this->serviceRepository = $serviceRepo;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     * @return Response
     */
    public function index(ServiceDataTable $serviceDataTable,$serviceCategoryId=0)
    {

        if($serviceCategoryId)
            $serviceDataTable->addFilters("service_category_id","=",$serviceCategoryId);
        return $serviceDataTable->render('services.services.index');
    }
    public function categoryServicesList(ServiceDataTable $serviceDataTable,$serviceCategoryId)
    {
        if($serviceCategoryId)
            $serviceDataTable->addFilters("service_category_id","=",$serviceCategoryId);

        return $serviceDataTable->render('services.services.index');
    }


    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.services.create');
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceRequest $request)
    {
        $input = $request->all();

        if (isset($input['icon'] ) && $input['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $input['icon'] = config('global.service_images') . basename($coverImage);
        }

        $service = $this->serviceRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/services.singular')]));
        return redirect(route('services.categoryServices',$service->service_category_id));
        return redirect(route('services.services.index'));
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $service = $this->serviceRepository->find($id);

        if (empty($service)) {
            Flash::error(__('messages.not_found', ['model' => __('models/services.singular')]));

            return redirect(route('services.services.index'));
        }

        return view('services.services.show')->with('service', $service);
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $service = $this->serviceRepository->find($id);

        if (empty($service)) {
            Flash::error(__('messages.not_found', ['model' => __('models/services.singular')]));

            return redirect(route('services.services.index'));
        }

        return view('services.services.edit')->with('service', $service);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int              $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceRequest $request)
    {
        $service = $this->serviceRepository->find($id);

        if (empty($service)) {
            Flash::error(__('messages.not_found', ['model' => __('models/services.singular')]));

            return redirect(route('services.services.index'));
        }
        $data=$request->all();
        if (isset($data['icon'] ) && $data['icon'] != null) {
            $coverImage = Storage::disk(config('filesystems.default'))->put(config('global.service_images'), $request['icon']);
            $data['icon'] = config('global.service_images') . basename($coverImage);
        }
        if(!isset($data["status"])){
            $data["status"]=0;
        }
        $service = $this->serviceRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/services.singular')]));

        return redirect(route('services.categoryServices',$service->service_category_id));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $service = $this->serviceRepository->find($id);

        if (empty($service)) {
            Flash::error(__('messages.not_found', ['model' => __('models/services.singular')]));

            return redirect(route('services.services.index'));
        }

        $this->serviceRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/services.singular')]));


        return redirect(route('services.services.index'));
    }
}
