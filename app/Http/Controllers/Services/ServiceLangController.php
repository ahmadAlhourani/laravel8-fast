<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceLangDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceLangRequest;
use App\Http\Requests\Services\UpdateServiceLangRequest;
use App\Repositories\Services\ServiceLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceLangController extends AppBaseController
{
    /** @var  ServiceLangRepository */
    private $serviceLangRepository;

    public function __construct(ServiceLangRepository $serviceLangRepo)
    {
        $this->serviceLangRepository = $serviceLangRepo;
    }

    /**
     * Display a listing of the ServiceLang.
     *
     * @param ServiceLangDataTable $serviceLangDataTable
     * @return Response
     */
    public function index(ServiceLangDataTable $serviceLangDataTable,$serviceId=0)
    {
        if($serviceId)
            $serviceLangDataTable->addFilters("service_id","=",$serviceId);
        return $serviceLangDataTable->render('services.service_langs.index');
    }

    /**
     * Show the form for creating a new ServiceLang.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_langs.create');
    }

    /**
     * Store a newly created ServiceLang in storage.
     *
     * @param CreateServiceLangRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceLangRequest $request)
    {
        $input = $request->all();

        $serviceLang = $this->serviceLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceLangs.singular')]));

        return redirect(route('services.serviceLangs',$serviceLang->service_id));
        return redirect(route('services.serviceLangs.index'));
    }

    /**
     * Display the specified ServiceLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceLang = $this->serviceLangRepository->find($id);

        if (empty($serviceLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceLangs.singular')]));

            return redirect(route('services.serviceLangs.index'));
        }

        return view('services.service_langs.show')->with('serviceLang', $serviceLang);
    }

    /**
     * Show the form for editing the specified ServiceLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceLang = $this->serviceLangRepository->find($id);

        if (empty($serviceLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceLangs.singular')]));

            return redirect(route('services.serviceLangs.index'));
        }

        return view('services.service_langs.edit')->with('serviceLang', $serviceLang);
    }

    /**
     * Update the specified ServiceLang in storage.
     *
     * @param  int              $id
     * @param UpdateServiceLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceLangRequest $request)
    {
        $serviceLang = $this->serviceLangRepository->find($id);

        if (empty($serviceLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceLangs.singular')]));

            return redirect(route('services.serviceLangs.index'));
        }

        $serviceLang = $this->serviceLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceLangs.singular')]));
        return redirect(route('services.serviceLangs',$serviceLang->service_id));
        return redirect(route('services.serviceLangs.index'));
    }

    /**
     * Remove the specified ServiceLang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceLang = $this->serviceLangRepository->find($id);

        if (empty($serviceLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceLangs.singular')]));

            return redirect(route('services.serviceLangs.index'));
        }

        $this->serviceLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceLangs.singular')]));

        return redirect(route('services.serviceLangs.index'));
    }
}
