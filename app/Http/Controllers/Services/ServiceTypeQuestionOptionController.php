<?php

namespace App\Http\Controllers\Services;

use App\DataTables\Services\ServiceTypeQuestionOptionDataTable;
use App\Http\Requests\Services;
use App\Http\Requests\Services\CreateServiceTypeQuestionOptionRequest;
use App\Http\Requests\Services\UpdateServiceTypeQuestionOptionRequest;
use App\Repositories\Services\ServiceTypeQuestionOptionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceTypeQuestionOptionController extends AppBaseController
{
    /** @var  ServiceTypeQuestionOptionRepository */
    private $serviceTypeQuestionOptionRepository;

    public function __construct(ServiceTypeQuestionOptionRepository $serviceTypeQuestionOptionRepo)
    {
        $this->serviceTypeQuestionOptionRepository = $serviceTypeQuestionOptionRepo;
    }

    /**
     * Display a listing of the ServiceTypeQuestionOption.
     *
     * @param ServiceTypeQuestionOptionDataTable $serviceTypeQuestionOptionDataTable
     * @return Response
     */
    public function index(ServiceTypeQuestionOptionDataTable $serviceTypeQuestionOptionDataTable, $serviceTypeQuestionId = null)
    {
        if ($serviceTypeQuestionId) {
            $serviceTypeQuestionOptionDataTable->addFilters("service_type_question_id", "=", $serviceTypeQuestionId);
        }
        return $serviceTypeQuestionOptionDataTable->render('services.service_type_question_options.index');
    }

    /**
     * Show the form for creating a new ServiceTypeQuestionOption.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.service_type_question_options.create');
    }

    /**
     * Store a newly created ServiceTypeQuestionOption in storage.
     *
     * @param CreateServiceTypeQuestionOptionRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceTypeQuestionOptionRequest $request)
    {
        $input = $request->all();

        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

        return redirect(route('services.serviceTypeQuestionOptions',$serviceTypeQuestionOption->service_type_question_id));
        return redirect(route('services.serviceTypeQuestionOptions.index'));
    }

    /**
     * Display the specified ServiceTypeQuestionOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->find($id);

        if (empty($serviceTypeQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

            return redirect(route('services.serviceTypeQuestionOptions.index'));
        }

        return view('services.service_type_question_options.show')->with('serviceTypeQuestionOption', $serviceTypeQuestionOption);
    }

    /**
     * Show the form for editing the specified ServiceTypeQuestionOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->find($id);

        if (empty($serviceTypeQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

            return redirect(route('services.serviceTypeQuestionOptions.index'));
        }

        return view('services.service_type_question_options.edit')->with('serviceTypeQuestionOption', $serviceTypeQuestionOption);
    }

    /**
     * Update the specified ServiceTypeQuestionOption in storage.
     *
     * @param  int              $id
     * @param UpdateServiceTypeQuestionOptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceTypeQuestionOptionRequest $request)
    {
        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->find($id);

        if (empty($serviceTypeQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

            return redirect(route('services.serviceTypeQuestionOptions.index'));
        }

        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/serviceTypeQuestionOptions.singular')]));
        return redirect(route('services.serviceTypeQuestionOptions',$serviceTypeQuestionOption->service_type_question_id));
        return redirect(route('services.serviceTypeQuestionOptions.index'));
    }

    /**
     * Remove the specified ServiceTypeQuestionOption from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceTypeQuestionOption = $this->serviceTypeQuestionOptionRepository->find($id);

        if (empty($serviceTypeQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

            return redirect(route('services.serviceTypeQuestionOptions.index'));
        }

        $this->serviceTypeQuestionOptionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/serviceTypeQuestionOptions.singular')]));

        return redirect(route('services.serviceTypeQuestionOptions.index'));
    }
}
