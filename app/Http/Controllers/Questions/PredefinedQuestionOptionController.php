<?php

namespace App\Http\Controllers\Questions;

use App\DataTables\Questions\PredefinedQuestionOptionDataTable;
use App\Http\Requests\Questions;
use App\Http\Requests\Questions\CreatePredefinedQuestionOptionRequest;
use App\Http\Requests\Questions\UpdatePredefinedQuestionOptionRequest;
use App\Repositories\Questions\PredefinedQuestionOptionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PredefinedQuestionOptionController extends AppBaseController
{
    /** @var  PredefinedQuestionOptionRepository */
    private $predefinedQuestionOptionRepository;

    public function __construct(PredefinedQuestionOptionRepository $predefinedQuestionOptionRepo)
    {
        $this->predefinedQuestionOptionRepository = $predefinedQuestionOptionRepo;
    }

    /**
     * Display a listing of the PredefinedQuestionOption.
     *
     * @param PredefinedQuestionOptionDataTable $predefinedQuestionOptionDataTable
     * @return Response
     */
    public function index(PredefinedQuestionOptionDataTable $predefinedQuestionOptionDataTable,$predefinedQuestionId=0)
    {
        if($predefinedQuestionId){
            $predefinedQuestionOptionDataTable->addFilters("predefined_question_id","=",$predefinedQuestionId);
        }
        return $predefinedQuestionOptionDataTable->render('questions.predefined_question_options.index');
    }

    /**
     * Show the form for creating a new PredefinedQuestionOption.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions.predefined_question_options.create');
    }

    /**
     * Store a newly created PredefinedQuestionOption in storage.
     *
     * @param CreatePredefinedQuestionOptionRequest $request
     *
     * @return Response
     */
    public function store(CreatePredefinedQuestionOptionRequest $request)
    {
        $input = $request->all();

        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/predefinedQuestionOptions.singular')]));
        return redirect(route('questions.predefinedQuestionOptions',$predefinedQuestionOption->predefined_question_id));
        return redirect(route('questions.predefinedQuestionOptions.index'));
    }

    /**
     * Display the specified PredefinedQuestionOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->find($id);

        if (empty($predefinedQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionOptions.singular')]));

            return redirect(route('questions.predefinedQuestionOptions.index'));
        }

        return view('questions.predefined_question_options.show')->with('predefinedQuestionOption', $predefinedQuestionOption);
    }

    /**
     * Show the form for editing the specified PredefinedQuestionOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->find($id);

        if (empty($predefinedQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionOptions.singular')]));

            return redirect(route('questions.predefinedQuestionOptions.index'));
        }

        return view('questions.predefined_question_options.edit')->with('predefinedQuestionOption', $predefinedQuestionOption);
    }

    /**
     * Update the specified PredefinedQuestionOption in storage.
     *
     * @param  int              $id
     * @param UpdatePredefinedQuestionOptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePredefinedQuestionOptionRequest $request)
    {
        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->find($id);

        if (empty($predefinedQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionOptions.singular')]));

            return redirect(route('questions.predefinedQuestionOptions.index'));
        }

        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/predefinedQuestionOptions.singular')]));
        return redirect(route('questions.predefinedQuestionOptions',$predefinedQuestionOption->predefined_question_id));

        return redirect(route('questions.predefinedQuestionOptions.index'));
    }

    /**
     * Remove the specified PredefinedQuestionOption from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $predefinedQuestionOption = $this->predefinedQuestionOptionRepository->find($id);

        if (empty($predefinedQuestionOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionOptions.singular')]));

            return redirect(route('questions.predefinedQuestionOptions.index'));
        }

        $this->predefinedQuestionOptionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/predefinedQuestionOptions.singular')]));

        return redirect(route('questions.predefinedQuestionOptions.index'));
    }
}
