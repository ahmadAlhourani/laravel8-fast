<?php

namespace App\Http\Controllers\Questions;

use App\DataTables\Questions\PredefinedQuestionLangDataTable;
use App\Http\Requests\Questions;
use App\Http\Requests\Questions\CreatePredefinedQuestionLangRequest;
use App\Http\Requests\Questions\UpdatePredefinedQuestionLangRequest;
use App\Repositories\Questions\PredefinedQuestionLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PredefinedQuestionLangController extends AppBaseController
{
    /** @var  PredefinedQuestionLangRepository */
    private $predefinedQuestionLangRepository;

    public function __construct(PredefinedQuestionLangRepository $predefinedQuestionLangRepo)
    {
        $this->predefinedQuestionLangRepository = $predefinedQuestionLangRepo;
    }

    /**
     * Display a listing of the PredefinedQuestionLang.
     *
     * @param PredefinedQuestionLangDataTable $predefinedQuestionLangDataTable
     * @return Response
     */
    public function index(PredefinedQuestionLangDataTable $predefinedQuestionLangDataTable,$predefinedQuestionId=0)
    {
        if($predefinedQuestionId){
            $predefinedQuestionLangDataTable->addFilters("predefined_question_id","=",$predefinedQuestionId);
        }
        return $predefinedQuestionLangDataTable->render('questions.predefined_question_langs.index');
    }

    /**
     * Show the form for creating a new PredefinedQuestionLang.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions.predefined_question_langs.create');
    }

    /**
     * Store a newly created PredefinedQuestionLang in storage.
     *
     * @param CreatePredefinedQuestionLangRequest $request
     *
     * @return Response
     */
    public function store(CreatePredefinedQuestionLangRequest $request)
    {
        $input = $request->all();

        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/predefinedQuestionLangs.singular')]));
        return redirect(route('questions.predefinedQuestionLangs',$predefinedQuestionLang->predefined_question_id));
        return redirect(route('questions.predefinedQuestionLangs.index'));
    }

    /**
     * Display the specified PredefinedQuestionLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->find($id);

        if (empty($predefinedQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionLangs.singular')]));

            return redirect(route('questions.predefinedQuestionLangs.index'));
        }

        return view('questions.predefined_question_langs.show')->with('predefinedQuestionLang', $predefinedQuestionLang);
    }

    /**
     * Show the form for editing the specified PredefinedQuestionLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->find($id);

        if (empty($predefinedQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionLangs.singular')]));

            return redirect(route('questions.predefinedQuestionLangs.index'));
        }

        return view('questions.predefined_question_langs.edit')->with('predefinedQuestionLang', $predefinedQuestionLang);
    }

    /**
     * Update the specified PredefinedQuestionLang in storage.
     *
     * @param  int              $id
     * @param UpdatePredefinedQuestionLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePredefinedQuestionLangRequest $request)
    {
        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->find($id);

        if (empty($predefinedQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionLangs.singular')]));

            return redirect(route('questions.predefinedQuestionLangs.index'));
        }

        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/predefinedQuestionLangs.singular')]));

        return redirect(route('questions.predefinedQuestionLangs',$predefinedQuestionLang->predefined_question_id));
        return redirect(route('questions.predefinedQuestionLangs.index'));
    }

    /**
     * Remove the specified PredefinedQuestionLang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $predefinedQuestionLang = $this->predefinedQuestionLangRepository->find($id);

        if (empty($predefinedQuestionLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestionLangs.singular')]));

            return redirect(route('questions.predefinedQuestionLangs.index'));
        }

        $this->predefinedQuestionLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/predefinedQuestionLangs.singular')]));

        return redirect(route('questions.predefinedQuestionLangs.index'));
    }
}
