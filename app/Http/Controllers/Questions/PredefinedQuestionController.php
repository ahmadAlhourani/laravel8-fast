<?php

namespace App\Http\Controllers\Questions;

use App\DataTables\Questions\PredefinedQuestionDataTable;
use App\Http\Requests\Questions;
use App\Http\Requests\Questions\CreatePredefinedQuestionRequest;
use App\Http\Requests\Questions\UpdatePredefinedQuestionRequest;
use App\Repositories\Questions\PredefinedQuestionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PredefinedQuestionController extends AppBaseController
{
    /** @var  PredefinedQuestionRepository */
    private $predefinedQuestionRepository;

    public function __construct(PredefinedQuestionRepository $predefinedQuestionRepo)
    {
        $this->predefinedQuestionRepository = $predefinedQuestionRepo;
    }

    /**
     * Display a listing of the PredefinedQuestion.
     *
     * @param PredefinedQuestionDataTable $predefinedQuestionDataTable
     * @return Response
     */
    public function index(PredefinedQuestionDataTable $predefinedQuestionDataTable,$questionTypeId=0)
    {
        if($questionTypeId){
            $predefinedQuestionDataTable->addFilters("question_type_id","=",$questionTypeId);
        }
        return $predefinedQuestionDataTable->render('questions.predefined_questions.index');
    }

    /**
     * Show the form for creating a new PredefinedQuestion.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions.predefined_questions.create');
    }

    /**
     * Store a newly created PredefinedQuestion in storage.
     *
     * @param CreatePredefinedQuestionRequest $request
     *
     * @return Response
     */
    public function store(CreatePredefinedQuestionRequest $request)
    {
        $input = $request->all();

        $predefinedQuestion = $this->predefinedQuestionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/predefinedQuestions.singular')]));
        return redirect(route('questions.predefinedQuestions',$predefinedQuestion->question_type_id));
        return redirect(route('questions.predefinedQuestions.index'));
    }

    /**
     * Display the specified PredefinedQuestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $predefinedQuestion = $this->predefinedQuestionRepository->find($id);

        if (empty($predefinedQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestions.singular')]));

            return redirect(route('questions.predefinedQuestions.index'));
        }

        return view('questions.predefined_questions.show')->with('predefinedQuestion', $predefinedQuestion);
    }

    /**
     * Show the form for editing the specified PredefinedQuestion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $predefinedQuestion = $this->predefinedQuestionRepository->find($id);

        if (empty($predefinedQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestions.singular')]));

            return redirect(route('questions.predefinedQuestions.index'));
        }

        return view('questions.predefined_questions.edit')->with('predefinedQuestion', $predefinedQuestion);
    }

    /**
     * Update the specified PredefinedQuestion in storage.
     *
     * @param  int              $id
     * @param UpdatePredefinedQuestionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePredefinedQuestionRequest $request)
    {
        $predefinedQuestion = $this->predefinedQuestionRepository->find($id);

        if (empty($predefinedQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestions.singular')]));

            return redirect(route('questions.predefinedQuestions.index'));
        }

        $predefinedQuestion = $this->predefinedQuestionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/predefinedQuestions.singular')]));
        return redirect(route('questions.predefinedQuestions',$predefinedQuestion->question_type_id));
        return redirect(route('questions.predefinedQuestions.index'));
    }

    /**
     * Remove the specified PredefinedQuestion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $predefinedQuestion = $this->predefinedQuestionRepository->find($id);

        if (empty($predefinedQuestion)) {
            Flash::error(__('messages.not_found', ['model' => __('models/predefinedQuestions.singular')]));

            return redirect(route('questions.predefinedQuestions.index'));
        }

        $this->predefinedQuestionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/predefinedQuestions.singular')]));

        return redirect(route('questions.predefinedQuestions.index'));
    }
}
