<?php

namespace App\Http\Controllers\Questions;

use App\DataTables\Questions\QuestionTypeDataTable;
use App\Http\Requests\Questions;
use App\Http\Requests\Questions\CreateQuestionTypeRequest;
use App\Http\Requests\Questions\UpdateQuestionTypeRequest;
use App\Repositories\Questions\QuestionTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class QuestionTypeController extends AppBaseController
{
    /** @var  QuestionTypeRepository */
    private $questionTypeRepository;

    public function __construct(QuestionTypeRepository $questionTypeRepo)
    {
        $this->questionTypeRepository = $questionTypeRepo;
    }

    /**
     * Display a listing of the QuestionType.
     *
     * @param QuestionTypeDataTable $questionTypeDataTable
     * @return Response
     */
    public function index(QuestionTypeDataTable $questionTypeDataTable)
    {
        return $questionTypeDataTable->render('questions.question_types.index');
    }

    /**
     * Show the form for creating a new QuestionType.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions.question_types.create');
    }

    /**
     * Store a newly created QuestionType in storage.
     *
     * @param CreateQuestionTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateQuestionTypeRequest $request)
    {
        $input = $request->all();

        $questionType = $this->questionTypeRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/questionTypes.singular')]));

        return redirect(route('questions.questionTypes.index'));
    }

    /**
     * Display the specified QuestionType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $questionType = $this->questionTypeRepository->find($id);

        if (empty($questionType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypes.singular')]));

            return redirect(route('questions.questionTypes.index'));
        }

        return view('questions.question_types.show')->with('questionType', $questionType);
    }

    /**
     * Show the form for editing the specified QuestionType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $questionType = $this->questionTypeRepository->find($id);

        if (empty($questionType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypes.singular')]));

            return redirect(route('questions.questionTypes.index'));
        }

        return view('questions.question_types.edit')->with('questionType', $questionType);
    }

    /**
     * Update the specified QuestionType in storage.
     *
     * @param  int              $id
     * @param UpdateQuestionTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuestionTypeRequest $request)
    {
        $questionType = $this->questionTypeRepository->find($id);

        if (empty($questionType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypes.singular')]));

            return redirect(route('questions.questionTypes.index'));
        }

        $questionType = $this->questionTypeRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/questionTypes.singular')]));

        return redirect(route('questions.questionTypes.index'));
    }

    /**
     * Remove the specified QuestionType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $questionType = $this->questionTypeRepository->find($id);

        if (empty($questionType)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypes.singular')]));

            return redirect(route('questions.questionTypes.index'));
        }

        $this->questionTypeRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/questionTypes.singular')]));

        return redirect(route('questions.questionTypes.index'));
    }
}
