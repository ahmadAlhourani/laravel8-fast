<?php

namespace App\Http\Controllers\Questions;

use App\DataTables\Questions\QuestionTypeOptionDataTable;
use App\Http\Requests\Questions;
use App\Http\Requests\Questions\CreateQuestionTypeOptionRequest;
use App\Http\Requests\Questions\UpdateQuestionTypeOptionRequest;
use App\Repositories\Questions\QuestionTypeOptionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class QuestionTypeOptionController extends AppBaseController
{
    /** @var  QuestionTypeOptionRepository */
    private $questionTypeOptionRepository;

    public function __construct(QuestionTypeOptionRepository $questionTypeOptionRepo)
    {
        $this->questionTypeOptionRepository = $questionTypeOptionRepo;
    }

    /**
     * Display a listing of the QuestionTypeOption.
     *
     * @param QuestionTypeOptionDataTable $questionTypeOptionDataTable
     * @return Response
     */
    public function index(QuestionTypeOptionDataTable $questionTypeOptionDataTable,$questionTypeId=0)
    {
        if($questionTypeId){
            $questionTypeOptionDataTable->addFilters("question_type_id","=",$questionTypeId);
        }
        return $questionTypeOptionDataTable->render('questions.question_type_options.index');
    }

    /**
     * Show the form for creating a new QuestionTypeOption.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions.question_type_options.create');
    }

    /**
     * Store a newly created QuestionTypeOption in storage.
     *
     * @param CreateQuestionTypeOptionRequest $request
     *
     * @return Response
     */
    public function store(CreateQuestionTypeOptionRequest $request)
    {
        $input = $request->all();

        $questionTypeOption = $this->questionTypeOptionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/questionTypeOptions.singular')]));

        return redirect(route('questions.questionTypeOptions',$questionTypeOption->question_type_id));
    }

    /**
     * Display the specified QuestionTypeOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $questionTypeOption = $this->questionTypeOptionRepository->find($id);

        if (empty($questionTypeOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypeOptions.singular')]));

            return redirect(route('questions.questionTypeOptions.index'));
        }

        return view('questions.question_type_options.show')->with('questionTypeOption', $questionTypeOption);
    }

    /**
     * Show the form for editing the specified QuestionTypeOption.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $questionTypeOption = $this->questionTypeOptionRepository->find($id);

        if (empty($questionTypeOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypeOptions.singular')]));

            return redirect(route('questions.questionTypeOptions.index'));
        }

        return view('questions.question_type_options.edit')->with('questionTypeOption', $questionTypeOption);
    }

    /**
     * Update the specified QuestionTypeOption in storage.
     *
     * @param  int              $id
     * @param UpdateQuestionTypeOptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuestionTypeOptionRequest $request)
    {
        $questionTypeOption = $this->questionTypeOptionRepository->find($id);

        if (empty($questionTypeOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypeOptions.singular')]));

            return redirect(route('questions.questionTypeOptions.index'));
        }

        $questionTypeOption = $this->questionTypeOptionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/questionTypeOptions.singular')]));
        return redirect(route('questions.questionTypeOptions',$questionTypeOption->question_type_id));
        return redirect(route('questions.questionTypeOptions.index'));
    }

    /**
     * Remove the specified QuestionTypeOption from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $questionTypeOption = $this->questionTypeOptionRepository->find($id);

        if (empty($questionTypeOption)) {
            Flash::error(__('messages.not_found', ['model' => __('models/questionTypeOptions.singular')]));

            return redirect(route('questions.questionTypeOptions.index'));
        }

        $this->questionTypeOptionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/questionTypeOptions.singular')]));

        return redirect(route('questions.questionTypeOptions.index'));
    }
}
