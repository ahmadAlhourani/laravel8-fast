<?php

namespace App\Http\Controllers\Directory;

use App\DataTables\Directory\AlertDataTable;
use App\Http\Requests\Directory;
use App\Http\Requests\Directory\CreateAlertRequest;
use App\Http\Requests\Directory\UpdateAlertRequest;
use App\Repositories\Directory\AlertRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AlertController extends AppBaseController
{
    /** @var  AlertRepository */
    private $alertRepository;

    public function __construct(AlertRepository $alertRepo)
    {
        $this->alertRepository = $alertRepo;
    }

    /**
     * Display a listing of the Alert.
     *
     * @param AlertDataTable $alertDataTable
     * @return Response
     */
    public function index(AlertDataTable $alertDataTable)
    {
        return $alertDataTable->render('directory.alerts.index');
    }

    /**
     * Show the form for creating a new Alert.
     *
     * @return Response
     */
    public function create()
    {
        return view('directory.alerts.create');
    }

    /**
     * Store a newly created Alert in storage.
     *
     * @param CreateAlertRequest $request
     *
     * @return Response
     */
    public function store(CreateAlertRequest $request)
    {
        $input = $request->all();

        $alert = $this->alertRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/alerts.singular')]));

        return redirect(route('directory.alerts.index'));
    }

    /**
     * Display the specified Alert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alert = $this->alertRepository->find($id);

        if (empty($alert)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alerts.singular')]));

            return redirect(route('directory.alerts.index'));
        }

        return view('directory.alerts.show')->with('alert', $alert);
    }

    /**
     * Show the form for editing the specified Alert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alert = $this->alertRepository->find($id);

        if (empty($alert)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alerts.singular')]));

            return redirect(route('directory.alerts.index'));
        }

        return view('directory.alerts.edit')->with('alert', $alert);
    }

    /**
     * Update the specified Alert in storage.
     *
     * @param  int              $id
     * @param UpdateAlertRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlertRequest $request)
    {
        $alert = $this->alertRepository->find($id);

        if (empty($alert)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alerts.singular')]));

            return redirect(route('directory.alerts.index'));
        }

        $alert = $this->alertRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/alerts.singular')]));

        return redirect(route('directory.alerts.index'));
    }

    /**
     * Remove the specified Alert from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alert = $this->alertRepository->find($id);

        if (empty($alert)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alerts.singular')]));

            return redirect(route('directory.alerts.index'));
        }

        $this->alertRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/alerts.singular')]));

        return redirect(route('directory.alerts.index'));
    }
}
