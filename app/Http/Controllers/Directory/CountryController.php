<?php

namespace App\Http\Controllers\Directory;

use App\DataTables\Directory\CountryDataTable;
use App\Http\Requests\Directory;
use App\Http\Requests\Directory\CreateCountryRequest;
use App\Http\Requests\Directory\UpdateCountryRequest;
use App\Repositories\Directory\CountryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Response;

class CountryController extends AppBaseController
{
    /**
     * @OA\Info(title="Services API", version="0.1")
     * Services API.
     *      *@OA\Server(
     *         description="Local",
     *         url="/api/",
     *     )
     */
    /**
     *     @SWG\SecurityScheme(
     *          securityDefinition="default",
     *          type="apiKey",
     *          in="header",
     *          name="Authorization"
     *      )
     **/

    /** @var  CountryRepository */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepo)
    {
        $this->countryRepository = $countryRepo;

    }

    /**
     * Display a listing of the Country.
     *
     * @param CountryDataTable $countryDataTable
     * @return Response
     */
    public function index(CountryDataTable $countryDataTable)
    {
        $user=Auth::user();
        $res=$user->checkUserPermission("web");
//        print_r(json_encode($res));
////        print_r([$res]);
//        dd();

        return $countryDataTable->render('directory.countries.index');
    }

    /**
     * Show the form for creating a new Country.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        return view('directory.countries.create');
    }

    /**
     * Store a newly created Country in storage.
     *
     * @param CreateCountryRequest $request
     *
     * @return Response
     */
    public function store(CreateCountryRequest $request)
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        $input = $request->all();

        $country = $this->countryRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/countries.singular')]));

        return redirect(route('directory.countries.index'));
    }

    /**
     * Display the specified Country.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        $country = $this->countryRepository->find($id);

        if (empty($country)) {
            Flash::error(__('messages.not_found', ['model' => __('models/countries.singular')]));

            return redirect(route('directory.countries.index'));
        }

        return view('directory.countries.show')->with('country', $country);
    }

    /**
     * Show the form for editing the specified Country.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        $country = $this->countryRepository->find($id);

        if (empty($country)) {
            Flash::error(__('messages.not_found', ['model' => __('models/countries.singular')]));

            return redirect(route('directory.countries.index'));
        }

        return view('directory.countries.edit')->with('country', $country);
    }

    /**
     * Update the specified Country in storage.
     *
     * @param  int              $id
     * @param UpdateCountryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCountryRequest $request)
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        $country = $this->countryRepository->find($id);

        if (empty($country)) {
            Flash::error(__('messages.not_found', ['model' => __('models/countries.singular')]));

            return redirect(route('directory.countries.index'));
        }

        $country = $this->countryRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/countries.singular')]));

        return redirect(route('directory.countries.index'));
    }

    /**
     * Remove the specified Country from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user=Auth::user();
        $user->checkUserPermission("web");
        $country = $this->countryRepository->find($id);

        if (empty($country)) {
            Flash::error(__('messages.not_found', ['model' => __('models/countries.singular')]));

            return redirect(route('directory.countries.index'));
        }

        $this->countryRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/countries.singular')]));

        return redirect(route('directory.countries.index'));
    }


}
