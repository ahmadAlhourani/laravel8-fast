<?php

namespace App\Http\Controllers\Directory;

use App\DataTables\Directory\AlertLangDataTable;
use App\Http\Requests\Directory;
use App\Http\Requests\Directory\CreateAlertLangRequest;
use App\Http\Requests\Directory\UpdateAlertLangRequest;
use App\Repositories\Directory\AlertLangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AlertLangController extends AppBaseController
{
    /** @var  AlertLangRepository */
    private $alertLangRepository;

    public function __construct(AlertLangRepository $alertLangRepo)
    {
        $this->alertLangRepository = $alertLangRepo;
    }

    /**
     * Display a listing of the AlertLang.
     *
     * @param AlertLangDataTable $alertLangDataTable
     * @return Response
     */
    public function index(AlertLangDataTable $alertLangDataTable)
    {
//        dd("rrrr");
//        die();
        return $alertLangDataTable->render('directory.alert_langs.index');
    }

    /**
     * Show the form for creating a new AlertLang.
     *
     * @return Response
     */
    public function create()
    {
//die();
        return view('directory.alert_langs.create');
    }

    /**
     * Store a newly created AlertLang in storage.
     *
     * @param CreateAlertLangRequest $request
     *
     * @return Response
     */
    public function store(CreateAlertLangRequest $request)
    {
        $input = $request->all();

        $alertLang = $this->alertLangRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/alertLangs.singular')]));

        return redirect(route('directory.alertLangs.index'));
    }

    /**
     * Display the specified AlertLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alertLang = $this->alertLangRepository->find($id);

        if (empty($alertLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alertLangs.singular')]));

            return redirect(route('directory.alertLangs.index'));
        }

        return view('directory.alert_langs.show')->with('alertLang', $alertLang);
    }

    /**
     * Show the form for editing the specified AlertLang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alertLang = $this->alertLangRepository->find($id);

        if (empty($alertLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alertLangs.singular')]));

            return redirect(route('directory.alertLangs.index'));
        }

        return view('directory.alert_langs.edit')->with('alertLang', $alertLang);
    }

    /**
     * Update the specified AlertLang in storage.
     *
     * @param  int              $id
     * @param UpdateAlertLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlertLangRequest $request)
    {
        $alertLang = $this->alertLangRepository->find($id);

        if (empty($alertLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alertLangs.singular')]));

            return redirect(route('directory.alertLangs.index'));
        }

        $alertLang = $this->alertLangRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/alertLangs.singular')]));

        return redirect(route('directory.alertLangs.index'));
    }

    /**
     * Remove the specified AlertLang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alertLang = $this->alertLangRepository->find($id);

        if (empty($alertLang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/alertLangs.singular')]));

            return redirect(route('directory.alertLangs.index'));
        }

        $this->alertLangRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/alertLangs.singular')]));

        return redirect(route('directory.alertLangs.index'));
    }
}
