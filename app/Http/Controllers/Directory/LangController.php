<?php

namespace App\Http\Controllers\Directory;

use App\DataTables\Directory\LangDataTable;
use App\Http\Requests\Directory;
use App\Http\Requests\Directory\CreateLangRequest;
use App\Http\Requests\Directory\UpdateLangRequest;
use App\Repositories\Directory\LangRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LangController extends AppBaseController
{
    /** @var  LangRepository */
    private $langRepository;

    public function __construct(LangRepository $langRepo)
    {
        $this->langRepository = $langRepo;
    }

    /**
     * Display a listing of the Lang.
     *
     * @param LangDataTable $langDataTable
     * @return Response
     */
    public function index(LangDataTable $langDataTable)
    {
        return $langDataTable->render('directory.langs.index');
    }

    /**
     * Show the form for creating a new Lang.
     *
     * @return Response
     */
    public function create()
    {
        return view('directory.langs.create');
    }

    /**
     * Store a newly created Lang in storage.
     *
     * @param CreateLangRequest $request
     *
     * @return Response
     */
    public function store(CreateLangRequest $request)
    {
        $input = $request->all();

        $lang = $this->langRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/langs.singular')]));

        return redirect(route('directory.langs.index'));
    }

    /**
     * Display the specified Lang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $lang = $this->langRepository->find($id);

        if (empty($lang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/langs.singular')]));

            return redirect(route('directory.langs.index'));
        }

        return view('directory.langs.show')->with('lang', $lang);
    }

    /**
     * Show the form for editing the specified Lang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $lang = $this->langRepository->find($id);

        if (empty($lang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/langs.singular')]));

            return redirect(route('directory.langs.index'));
        }

        return view('directory.langs.edit')->with('lang', $lang);
    }

    /**
     * Update the specified Lang in storage.
     *
     * @param  int              $id
     * @param UpdateLangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLangRequest $request)
    {
        $lang = $this->langRepository->find($id);

        if (empty($lang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/langs.singular')]));

            return redirect(route('directory.langs.index'));
        }

        $lang = $this->langRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/langs.singular')]));

        return redirect(route('directory.langs.index'));
    }

    /**
     * Remove the specified Lang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $lang = $this->langRepository->find($id);

        if (empty($lang)) {
            Flash::error(__('messages.not_found', ['model' => __('models/langs.singular')]));

            return redirect(route('directory.langs.index'));
        }

        $this->langRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/langs.singular')]));

        return redirect(route('directory.langs.index'));
    }
}
