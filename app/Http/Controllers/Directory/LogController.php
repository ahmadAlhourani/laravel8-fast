<?php

namespace App\Http\Controllers\Directory;

use App\DataTables\Directory\LogDataTable;
use App\Http\Requests\Directory;
use App\Http\Requests\Directory\CreateLogRequest;
use App\Http\Requests\Directory\UpdateLogRequest;
use App\Repositories\Directory\LogRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LogController extends AppBaseController
{
    /** @var  LogRepository */
    private $logRepository;

    public function __construct(LogRepository $logRepo)
    {
        $this->logRepository = $logRepo;
    }

    /**
     * Display a listing of the Log.
     *
     * @param LogDataTable $logDataTable
     * @return Response
     */
    public function index(LogDataTable $logDataTable)
    {
        return $logDataTable->render('directory.logs.index');
    }

    /**
     * Show the form for creating a new Log.
     *
     * @return Response
     */
    public function create()
    {
        return view('directory.logs.create');
    }

    /**
     * Store a newly created Log in storage.
     *
     * @param CreateLogRequest $request
     *
     * @return Response
     */
    public function store(CreateLogRequest $request)
    {
        $input = $request->all();

        $log = $this->logRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/logs.singular')]));

        return redirect(route('directory.logs.index'));
    }

    /**
     * Display the specified Log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $log = $this->logRepository->find($id);

        if (empty($log)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logs.singular')]));

            return redirect(route('directory.logs.index'));
        }

        return view('directory.logs.show')->with('log', $log);
    }

    /**
     * Show the form for editing the specified Log.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $log = $this->logRepository->find($id);

        if (empty($log)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logs.singular')]));

            return redirect(route('directory.logs.index'));
        }

        return view('directory.logs.edit')->with('log', $log);
    }

    /**
     * Update the specified Log in storage.
     *
     * @param  int              $id
     * @param UpdateLogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLogRequest $request)
    {
        $log = $this->logRepository->find($id);

        if (empty($log)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logs.singular')]));

            return redirect(route('directory.logs.index'));
        }

        $log = $this->logRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/logs.singular')]));

        return redirect(route('directory.logs.index'));
    }

    /**
     * Remove the specified Log from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $log = $this->logRepository->find($id);

        if (empty($log)) {
            Flash::error(__('messages.not_found', ['model' => __('models/logs.singular')]));

            return redirect(route('directory.logs.index'));
        }

        $this->logRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/logs.singular')]));

        return redirect(route('directory.logs.index'));
    }
}
