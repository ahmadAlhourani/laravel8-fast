<?php

namespace App\Exceptions;
use Exception;
use Aws\S3\Exception\S3Exception as S3;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Routing\Router;
use Modules\Directory\Entities\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            return response()->json($e, 200);
//            return response(['error' => $e->getMessage()], $e->getCode() ?: 400);
//            return response(['success'=> false,'message'=> [],'data' => []],404);
        });
    }
//
//    public function render($request, Exception $exception)
//    {
////        if ($exception instanceof ErrorException) {
////            return response()->json(
////                [
////                    'success' => false,
////                    'message' => "Person not found"
////                ],
////                400
////            );
////        }
////        return parent::render($request, $exception);
//
////        return response(['success' => false, 'message' => [$e->getMessage()], 'data' => []], 500);
//        // turn $e into an array.
//        // this is sending status code of 500
//        // get headers from $request.
////        return response()->json($e, 500);
//    }


}
