<?php

namespace App\Traits;


use Illuminate\Http\Request;

trait PagerTrait {
    public $page=1;
    public $limit=5;
    public $skip= 0;
    public $search=null;

    public function pager(Request $request){
        $page = 1;
        $limit = 5;
        if ($request->has(['page', 'limit'])) {
            $page = intval($request->input('page'));
            $limit = intval($request->input('limit'));
        }
        $this->page=$page;
        $this->limit=$limit;
        $this->skip= ($this->page == 1 ? 0 : (($this->page - 1) * $this->limit));
        $this->search=$request->input('search')??null;
    }

}
