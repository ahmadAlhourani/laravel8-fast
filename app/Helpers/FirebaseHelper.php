<?php
namespace App\Helpers;

use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Factory;
use App\Models\Directory\Log;


class FirebaseHelper {

    public function thisClassName(){
        return get_class($this);
    }

    public static function validateFirebaseToken($token){

        $factory = (new Factory)->withServiceAccount(file_get_contents(config("firebase.projects.app.credentials.file")));
//        $factory = (new Factory)->withServiceAccount(file_get_contents(env('FIREBASE_CREDENTIALS')));

        $fireBaseAuth = $factory->createAuth();

        $idTokenString = $token;


        try{
            $verifiedIdToken = $fireBaseAuth->verifyIdToken($idTokenString,true);

        }
        catch(InvalidToken $e) {


            Log::saveLog(['message' => $e->getMessage()], (new FirebaseHelper)->thisClassName(), __FUNCTION__);

            return 0;
        }
        catch(\InvalidArgumentException $e) {

            Log::saveLog(['message' => 'The token could not be parsed: '.$e->getMessage()], (new FirebaseHelper)->thisClassName(), __FUNCTION__);

            return 0;
        }

//        $uid = $verifiedIdToken->getClaim('sub');
        $uid = $verifiedIdToken->claims()->get('sub');
        return $uid;


    }

    public static function getTokenData($token){

        $factory = (new Factory)->withServiceAccount(file_get_contents(config("firebase.projects.app.credentials.file")));
//        $factory = (new Factory)->withServiceAccount(file_get_contents(env('FIREBASE_CREDENTIALS')));
        $fireBaseAuth = $factory->createAuth();

        try{
            $verifiedIdToken = $fireBaseAuth->verifyIdToken($token,true);

        }
        catch(InvalidToken $e) {


            Log::saveLog(['message' => $e->getMessage()], (new FirebaseHelper)->thisClassName(), __FUNCTION__);

            return 0;
        }
        catch(\InvalidArgumentException $e) {

            Log::saveLog(['message' => 'The token could not be parsed: '.$e->getMessage()], (new FirebaseHelper)->thisClassName(), __FUNCTION__);

            return 0;
        }

//        $uid = $verifiedIdToken->getClaim('sub');
        $uid = $verifiedIdToken->claims()->get('sub');
        $data = $fireBaseAuth->getUser($uid);


        return $data;


    }


}

