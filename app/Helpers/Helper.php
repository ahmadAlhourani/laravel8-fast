<?php

namespace App\Helpers;


use Illuminate\Support\Facades\Storage;

class Helper
{

    public static function url($path)
    {
        switch (config('filesystems.default')){
            case "s3":
                return config('global.media_url').$path;
                break;

            case "public":
                return Storage::url($path);
                break;
        }

    }
}
