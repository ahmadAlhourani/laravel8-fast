<?php
//namespace App\Helpers;
//
//use Modules\Account\Entities\FirebaseTokens;
//use Modules\Message\Entities\Conversation;
//use Modules\Notification\Entities\UserTopics;
//
//class PushNotification {
//
//
//    public static function sendNotification($to,$title,$body,$data = [])
//    {
//        $apiUrl=config('shaadoowGlobal.pushNotificationApiUrl');
//        $headers=config('shaadoowGlobal.pushNotificationHeaders');
//
//        if($data&&isset($data['sender_id'])){
//            $type           = "message";
//            $type_id        = $data['sender_id'];
//            $badge_count  = 0;
//        }
//        else{
//            $type           =($data&&isset($data['type'])) ?$data['type']:"general";
//            $type_id        = ($data&&isset($data['type_id'])) ?$data['type_id']:1;;
//            $badge_count  = 0;
//        }
//        if($data && isset($data['receiver_id '])&&$data['receiver_id'] != null){
//            $badge_count    = Conversation::badgeCount($data['receiver_id']);
//        }
//
//        if(is_array($to)){
//
//            // array of curl handles
//            $multiCurl = array();
//            // data to be returned
//            $result = array();
//            // multi handle
//            $mh = curl_multi_init();
//
//
//            foreach ($to as $i => $id) {
//                // URL from which data will be fetched
//
//
//                $contentArray=[
//                    "to"=>$id,
//                    "notification"=>[
//                        "title"=>$title,
//                        "body"=>$body,
//                        "badge"=>1,
//                        "content-available" => 1,
//                        "sound"=>"default"
//                    ],
//                    "data"  =>  [
//                        "type"          => $type,
//                        "type_id"       => $type_id,
//                        "message_badge" => $badge_count
//                    ]
//                ];
//                $multiCurl[$i] = curl_init();
//                curl_setopt($multiCurl[$i], CURLOPT_URL,$apiUrl);
//                curl_setopt($multiCurl[$i], CURLOPT_POST, true);
//                curl_setopt($multiCurl[$i], CURLOPT_HTTPHEADER, $headers);
//                curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($multiCurl[$i], CURLOPT_SSL_VERIFYPEER, false);
//                curl_setopt($multiCurl[$i], CURLOPT_POSTFIELDS, json_encode($contentArray));
//
//
//                curl_multi_add_handle($mh, $multiCurl[$i]);
//            }
//            $index=null;
//            do {
//                curl_multi_exec($mh,$index);
//            } while($index > 0);
//            // get content and remove handles
//            foreach($multiCurl as $k => $ch) {
//                $result[$k] = curl_multi_getcontent($ch);
//                curl_multi_remove_handle($mh, $ch);
//            }
//            // close
//            curl_multi_close($mh);
//
//        }
//        else {
//
//
//            $contentArray=[
//                "to"=>$to,
//                "notification"=>[
//                    "title"=>$title,
//                    "body"=>$body,
//                    "badge"=>1,
//                    "aps"   => [
//                        "content-available" => 1,
//                    ],
//                    "sound"=>"default"
//                ],
//                "data"  =>  [
//                    "type"          => $type,
//                    "type_id"       => $type_id,
//                    "message_badge" => $badge_count
//                ]
//            ];
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL,$apiUrl);
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($contentArray));
//            $result = curl_exec($ch);
//            curl_close($ch);
//            FirebaseTokens::cleanOrKeepToken($result,$to);
//        }
//        return $result;
//    }
//
//    public static function  sendBroadcast($condition,$title,$body,$data=[])
//    {
//        $apiUrl=config('shaadoowGlobal.pushNotificationApiUrl');
//        $headers=config('shaadoowGlobal.pushNotificationHeaders');
//        $contentArray=[
//            "condition"=>$condition,
//            "notification"=>[
//                "title"=>$title,
//                "body"=>$body,
//                "badge"=>1,
//                "aps"   => [
//                    "content-available" => 1,
//                ],
//                "sound"=>"default"
//            ],
//            "data"  =>  $data
//        ];
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL,$apiUrl);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($contentArray));
//        $result = curl_exec($ch);
//        curl_close($ch);
//
//        return $result;
//    }
//
//}
//
