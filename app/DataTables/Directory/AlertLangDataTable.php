<?php

namespace App\DataTables\Directory;

use App\Models\Directory\AlertLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class AlertLangDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'directory.alert_langs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\AlertLang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AlertLang $model)
    {
        return $model->newQuery()->with(['lang','alert']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/alertLangs.fields.id'), 'data' => 'id','searchable' => false]),
//            'alert_id' => new Column(['title' => __('models/alertLangs.fields.alert_id'), 'data' => 'alert_id','searchable' => false]),
//            'lang_id' => new Column(['title' => __('models/alertLangs.fields.lang_id'), 'data' => 'lang_id','searchable' => false]),
//            'alert' => new Column(['title' => 'Alert', 'data' => 'alert.state', 'name' => 'alert.state']),
            'alert' => new Column(['title' => 'Alert','data' => 'alert.state', 'name' => 'alert.state','searchable' => true]),
            'lang' => new Column(['title' => 'Lang', 'data' => 'lang.name', 'name' => 'lang.name','searchable' => true]),
            'name' => new Column(['title' => __('models/alertLangs.fields.name'), 'data' => 'name','searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'alert_langs_datatable_' . time();
    }
}
