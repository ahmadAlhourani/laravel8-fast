<?php

namespace App\DataTables\Questions;

use App\DataTables\QueryFilter;
use App\Models\Questions\PredefinedQuestionOption;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PredefinedQuestionOptionDataTable extends DataTable
{
    use QueryFilter;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter) {
            $query->where($queryFilter[0], $queryFilter[1], $queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'questions.predefined_question_options.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PredefinedQuestionOption $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PredefinedQuestionOption $model)
    {
        return $model->newQuery()->with(['question_type_option','predefined_question']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/predefinedQuestionOptions.fields.id'), 'data' => 'id','searchable' => false]),
//            'predefined_question_id' => new Column(['title' => __('models/predefinedQuestionOptions.fields.predefined_question_id'), 'data' => 'predefined_question_id','searchable' => false]),
            'predefined_question' => new Column(['title' => __('models/predefinedQuestionOptions.fields.predefined_question_id'), 'data' => 'predefined_question.question','searchable' => true]),
             //'question_type_option_id' => new Column(['title' => __('models/predefinedQuestionOptions.fields.question_type_option_id'), 'data' => 'question_type_option_id','searchable' => false]),
             'question_type_option' => new Column(['title' => __('models/predefinedQuestionOptions.fields.question_type_option_id'), 'data' => 'question_type_option.option','searchable' => true]),
            'description' => new Column(['title' => __('models/predefinedQuestionOptions.fields.description'), 'data' => 'description','searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'predefined_question_options_datatable_' . time();
    }
}
