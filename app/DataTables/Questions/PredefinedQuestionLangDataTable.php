<?php

namespace App\DataTables\Questions;

use App\DataTables\QueryFilter;
use App\Models\Questions\PredefinedQuestionLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class PredefinedQuestionLangDataTable extends DataTable
{
    use QueryFilter;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter) {
            $query->where($queryFilter[0], $queryFilter[1], $queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'questions.predefined_question_langs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PredefinedQuestionLang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PredefinedQuestionLang $model)
    {
        return $model->newQuery()->with(['predefined_question', 'lang']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/predefinedQuestionLangs.fields.id'), 'data' => 'id', 'searchable' => false]),
            'predefined_question' => new Column(['title' => __('models/predefinedQuestionOptions.fields.predefined_question_id'), 'data' => 'predefined_question.question', 'searchable' => true]),
            'lang' => new Column(['title' => 'Lang', 'data' => 'lang.name', 'name' => 'lang.name', 'searchable' => true]),
            'question' => new Column(['title' => __('models/predefinedQuestionLangs.fields.question'), 'data' => 'question', 'searchable' => false]),
            'note' => new Column(['title' => __('models/predefinedQuestionLangs.fields.note'), 'data' => 'note', 'searchable' => false]),
            'answer_example' => new Column(['title' => __('models/predefinedQuestionLangs.fields.answer_example'), 'data' => 'answer_example', 'searchable' => false]),
            'instructions' => new Column(['title' => __('models/predefinedQuestionLangs.fields.instructions'), 'data' => 'instructions', 'searchable' => false]),
            'default_answer' => new Column(['title' => __('models/predefinedQuestionLangs.fields.default_answer'), 'data' => 'default_answer', 'searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'predefined_question_langs_datatable_' . time();
    }
}
