<?php

namespace App\DataTables;



trait QueryFilter {
    protected $queryFilters=[];


    /**
     * @param string $key
     * @param string $operation
     * @param $val
     */
    public function addFilters(string $key,string $operation, $val){
        $this->queryFilters[]=[$key,$operation,$val];
    }
}
