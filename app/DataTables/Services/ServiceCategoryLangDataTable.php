<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Models\Services\ServiceCategoryLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceCategoryLangDataTable extends DataTable
{
    use QueryFilter;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter){
            $query->where($queryFilter[0],$queryFilter[1],$queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'services.service_category_langs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceCategoryLang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceCategoryLang $model)
    {


        return $model->newQuery()->with(['lang','service_category']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceCategoryLangs.fields.id'), 'data' => 'id','searchable' => false]),
//            'service_category_id' => new Column(['title' => __('models/serviceCategoryLangs.fields.service_category_id'), 'data' => 'service_category_id','searchable' => false]),
//            'lang_id' => new Column(['title' => __('models/serviceCategoryLangs.fields.lang_id'), 'data' => 'lang_id','searchable' => false]),
            'service_category' => new Column(['title' => 'Category','data' => 'service_category.name', 'name' => 'service_category.name','searchable' => true]),
            'lang' => new Column(['title' => 'Lang', 'data' => 'lang.name', 'name' => 'lang.name','searchable' => true]),
            'name' => new Column(['title' => __('models/serviceCategoryLangs.fields.name'), 'data' => 'name','searchable' => true]),
            'description' => new Column(['title' => __('models/serviceCategoryLangs.fields.description'), 'data' => 'description','searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_category_langs_datatable_' . time();
    }
}
