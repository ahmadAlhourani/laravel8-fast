<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Models\Services\ServiceTypeQuestionLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceTypeQuestionLangDataTable extends DataTable
{
    use QueryFilter;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter){
            $query->where($queryFilter[0],$queryFilter[1],$queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'services.service_type_question_langs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceTypeQuestionLang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceTypeQuestionLang $model)
    {
        return $model->newQuery()->with(['service_type_question','lang']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.id'), 'data' => 'id','searchable' => false]),
            'question' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.question'), 'data' => 'question','searchable' => false]),
            'service_type_question' => new Column(['title' => 'serviceTypeQuestion', 'data' => 'service_type_question.question','searchable' => false]),
            'lang' => new Column(['title' => 'Lang', 'data' => 'lang.name', 'name' => 'lang.name','searchable' => true]),
            'note' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.note'), 'data' => 'note','searchable' => false]),
            'answer_example' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.answer_example'), 'data' => 'answer_example','searchable' => false]),
            'instructions' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.instructions'), 'data' => 'instructions','searchable' => false]),
            'default_answer' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.default_answer'), 'data' => 'default_answer','searchable' => false]),
            'created_at' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.created_at'), 'data' => 'created_at','searchable' => false]),
            'updated_at' => new Column(['title' => __('models/serviceTypeQuestionLangs.fields.updated_at'), 'data' => 'updated_at','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_type_question_langs_datatable_' . time();
    }
}
