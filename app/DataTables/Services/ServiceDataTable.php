<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Helpers\Helper;
use App\Models\Services\Service;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceDataTable extends DataTable
{
    use QueryFilter;




    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        foreach ($this->queryFilters as $queryFilter){
            $query->where($queryFilter[0],$queryFilter[1],$queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('icon', function ($row) {

                $url=Helper::url($row->icon);
                return '<img src="'.$url.'" border="0" width="50" class="img-rounded" align="center" />';

            })
            ->addColumn('status', function ($row) {

                if($row->status == 1){
                    return "<div class=\"btn btn-icon bg-success-lt\" title=\"Published\">&#10003;</i></div>";
                }
                else {
                    return "<div class=\"btn btn-icon bg-secondary-lt\" title=\"Not Published\">&#10006;</i></div>";
                }

            })
            ->addColumn('action', 'services.services.datatables_actions')
            ->rawColumns(['icon', 'status', 'action'])
            ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Service $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Service $model)
    {
//        return $model->newQuery()->with(['service_category']);
        return $model->newQuery()->with(['service_category']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/services.fields.id'), 'data' => 'id','searchable' => false]),
            'name' => new Column(['title' => __('models/services.fields.name'), 'data' => 'name','searchable' => false]),
//            'service_category_id' => new Column(['title' => __('models/services.fields.service_category_id'), 'data' => 'service_category_id','searchable' => false]),
            'service_category' => new Column(['title' => 'Category','data' => 'service_category.name', 'name' => 'service_category.name','searchable' => true]),

            'short_description' => new Column(['title' => __('models/services.fields.short_description'), 'data' => 'short_description','searchable' => false]),
            'icon' => new Column(['title' => __('models/services.fields.icon'), 'data' => 'icon','searchable' => false]),
            'rank' => new Column(['title' => __('models/services.fields.rank'), 'data' => 'rank','searchable' => false]),
            'status' => new Column(['title' => "Status", 'data' => 'status', 'searchable' => true]),
            'description' => new Column(['title' => __('models/services.fields.description'), 'data' => 'description','searchable' => false]),
            'html_description' => new Column(['title' => __('models/services.fields.html_description'), 'data' => 'html_description','searchable' => false]),
            'question_service_type' => new Column(['title' => __('models/services.fields.question_service_type'), 'data' => 'question_service_type','searchable' => false]),
            'types_difference' => new Column(['title' => __('models/services.fields.types_difference'), 'data' => 'types_difference','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'services_datatable_' . time();
    }
}
