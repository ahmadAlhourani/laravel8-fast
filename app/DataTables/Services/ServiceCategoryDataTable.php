<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Helpers\Helper;
use App\Models\Directory\Log;
use App\Models\Services\ServiceCategory;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceCategoryDataTable extends DataTable
{
    use QueryFilter;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter) {
            $query->where($queryFilter[0], $queryFilter[1], $queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('icon', function ($row) {

                $url = Helper::url($row->icon);
                return '<img src="' . $url . '" border="0" width="50" class="img-rounded" align="center" />';

            })
            ->addColumn('status', function ($row) {

                if ($row->status == 1) {
                    return "<div class=\"btn btn-icon bg-success-lt\" title=\"Published\">&#10003;</i></div>";
                } else {
                    return "<div class=\"btn btn-icon bg-secondary-lt\" title=\"Not Published\">&#10006;</i></div>";
                }

            })
            ->addColumn('action', 'services.service_categories.datatables_actions')
            ->rawColumns(['icon', 'status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceCategory $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceCategories.fields.id'), 'data' => 'id', 'searchable' => false]),
            'name' => new Column(['title' => __('models/serviceCategories.fields.name'), 'data' => 'name', 'searchable' => true]),
            'description' => new Column(['title' => __('models/serviceCategories.fields.description'), 'data' => 'description', 'searchable' => true]),
            'icon' => new Column(['title' => __('models/serviceCategories.fields.icon'), 'data' => 'icon', 'searchable' => false]),
            'rank' => new Column(['title' => __('models/serviceCategories.fields.rank'), 'data' => 'rank', 'searchable' => false]),
            'status' => new Column(['title' => "Status", 'data' => 'status', 'searchable' => true]),


        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_categories_datatable_' . time();
    }
}
