<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Models\Services\ServiceTypeQuestion;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceTypeQuestionDataTable extends DataTable
{
    use QueryFilter;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter){
            $query->where($queryFilter[0],$queryFilter[1],$queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'services.service_type_questions.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceTypeQuestion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceTypeQuestion $model)
    {
        return $model->newQuery()->with(['question_type','service_type']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceTypeQuestions.fields.id'), 'data' => 'id','searchable' => false]),
            'question' => new Column(['title' => __('models/serviceTypeQuestions.fields.question'), 'data' => 'question','searchable' => false]),
//            'service_type_id' => new Column(['title' => __('models/serviceTypeQuestions.fields.service_type_id'), 'data' => 'service_type_id','searchable' => false]),
            'service_type' => new Column(['title' => 'Service Type','data' => 'service_type.name', 'name' => 'service_type.name','searchable' => true]),

//            'question_type_id' => new Column(['title' => __('models/serviceTypeQuestions.fields.question_type_id'), 'data' => 'question_type_id','searchable' => false]),
            'question_type' => new Column(['title' => 'Question Type', 'data' => 'question_type.name','searchable' => false]),

            'required' => new Column(['title' => __('models/serviceTypeQuestions.fields.required'), 'data' => 'required','searchable' => false]),
            'description' => new Column(['title' => __('models/serviceTypeQuestions.fields.description'), 'data' => 'description','searchable' => false]),
            'created_at' => new Column(['title' => __('models/serviceTypeQuestions.fields.created_at'), 'data' => 'created_at','searchable' => false]),
            'updated_at' => new Column(['title' => __('models/serviceTypeQuestions.fields.updated_at'), 'data' => 'updated_at','searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_type_questions_datatable_' . time();
    }
}
