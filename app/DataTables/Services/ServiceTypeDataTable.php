<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Helpers\Helper;
use App\Models\Services\ServiceType;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceTypeDataTable extends DataTable
{
    use QueryFilter;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter) {
            $query->where($queryFilter[0], $queryFilter[1], $queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('icon', function ($row) {

                $url = Helper::url($row->icon);
                return '<img src="' . $url . '" border="0" width="50" class="img-rounded" align="center" />';

            })
            ->addColumn('status', function ($row) {

                if ($row->status == 1) {
                    return "<div class=\"btn btn-icon bg-success-lt\" title=\"Published\">&#10003;</i></div>";
                } else {
                    return "<div class=\"btn btn-icon bg-secondary-lt\" title=\"Not Published\">&#10006;</i></div>";
                }

            })
            ->addColumn('show_price', function ($row) {

                if ($row->show_price == 1) {
                    return "<div class=\"btn btn-icon bg-success-lt\" title=\"Published\">&#10003;</i></div>";
                } else {
                    return "<div class=\"btn btn-icon bg-secondary-lt\" title=\"Not Published\">&#10006;</i></div>";
                }

            })
            ->addColumn('price_type', function ($row) {
                switch ($row->price_type){
                    case 1:
                        return "Per Hour";
                        break;
                    case 2:
                        return "Per Service";
                        break;
                    case 3:
                        return "expectation";
                        break;

                }



            })
            ->addColumn('action', 'services.service_types.datatables_actions')
            ->rawColumns(['price_type','show_price','icon', 'status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceType $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceType $model)
    {
        return $model->newQuery()->with(['service']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceTypes.fields.id'), 'data' => 'id', 'searchable' => false]),
            'name' => new Column(['title' => __('models/serviceTypes.fields.name'), 'data' => 'name', 'searchable' => true]),
            'service' => new Column(['title' => 'Service', 'data' => 'service.name', 'name' => 'service.name', 'searchable' => true]),
            'icon' => new Column(['title' => __('models/serviceTypes.fields.icon'), 'data' => 'icon', 'searchable' => false]),
            'rank' => new Column(['title' => __('models/serviceTypes.fields.rank'), 'data' => 'rank', 'searchable' => true]),

            'price_type' => new Column(['title' => "Price Type", 'data' => 'price_type', 'searchable' => false]),
            'price' => new Column(['title' => __('models/serviceTypes.fields.price'), 'data' => 'price', 'searchable' => false]),
            'show_price' => new Column(['title' => "Show Price?", 'data' => 'show_price', 'searchable' => false]),
            'status' => new Column(['title' => __('models/serviceTypes.fields.status'), 'data' => 'status', 'searchable' => false]),
            'short_description' => new Column(['title' => __('models/serviceTypes.fields.short_description'), 'data' => 'short_description', 'searchable' => true]),
//            'description' => new Column(['title' => __('models/serviceTypes.fields.description'), 'data' => 'description', 'searchable' => true]),
//            'html_description' => new Column(['title' => __('models/serviceTypes.fields.html_description'), 'data' => 'html_description', 'searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_types_datatable_' . time();
    }
}
