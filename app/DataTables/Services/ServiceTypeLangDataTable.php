<?php

namespace App\DataTables\Services;

use App\DataTables\QueryFilter;
use App\Models\Services\ServiceTypeLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ServiceTypeLangDataTable extends DataTable
{
    use QueryFilter;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        foreach ($this->queryFilters as $queryFilter){
            $query->where($queryFilter[0],$queryFilter[1],$queryFilter[2]);
        }
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'services.service_type_langs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ServiceTypeLang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceTypeLang $model)
    {
        return $model->newQuery()->with(['lang','service_type']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => new Column(['title' => __('models/serviceTypeLangs.fields.id'), 'data' => 'id','searchable' => false]),
//            'service_type_id' => new Column(['title' => __('models/serviceTypeLangs.fields.service_type_id'), 'data' => 'service_type_id','searchable' => false]),
//            'lang_id' => new Column(['title' => __('models/serviceTypeLangs.fields.lang_id'), 'data' => 'lang_id','searchable' => false]),

            'service_type' => new Column(['title' => 'Service Type','data' => 'service_type.name', 'name' => 'service_type.name','searchable' => true]),
            'lang' => new Column(['title' => 'Lang', 'data' => 'lang.name', 'name' => 'lang.name','searchable' => true]),

            'name' => new Column(['title' => __('models/serviceTypeLangs.fields.name'), 'data' => 'name','searchable' => true]),
            'short_description' => new Column(['title' => __('models/serviceTypeLangs.fields.short_description'), 'data' => 'short_description','searchable' => true]),
            'description' => new Column(['title' => __('models/serviceTypeLangs.fields.description'), 'data' => 'description','searchable' => true]),
            'html_description' => new Column(['title' => __('models/serviceTypeLangs.fields.html_description'), 'data' => 'html_description','searchable' => true]),
            'created_at' => new Column(['title' => __('models/serviceTypeLangs.fields.created_at'), 'data' => 'created_at','searchable' => true]),
            'updated_at' => new Column(['title' => __('models/serviceTypeLangs.fields.updated_at'), 'data' => 'updated_at','searchable' => true])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'service_type_langs_datatable_' . time();
    }
}
