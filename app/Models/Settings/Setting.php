<?php

namespace App\Models\Settings;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Setting
 * @package App\Models\Setting
 * @version October 21, 2021, 5:35 pm +07
 *

 * @property string $key
 * @property string $operator
 * @property string $value
 * @property string $description
 * @property integer $setting_category_id
 */
class Setting extends Model
{
    use SoftDeletes;


    public $table = 'settings';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'key',
        'operator',
        'value',
        'description',
        'setting_category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'operator' => 'string',
        'value' => 'string',
        'setting_category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function setting_category()
    {
        return $this->belongsTo(SettingCategory::class);
    }

}
