<?php

namespace App\Models\Settings;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class SettingCategory
 * @package App\Models\Settings
 * @version October 21, 2021, 6:09 pm +07
 *
 * @property \Illuminate\Database\Eloquent\Collection $settings
 * @property string $name
 * @property string $value
 * @property string $description
 */
class SettingCategory extends Model
{
    use SoftDeletes;


    public $table = 'setting_categories';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'value',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'value' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function settings()
    {
        return $this->hasMany(\App\Models\Settings\Setting::class, 'setting_category_id', 'id');
    }
}
