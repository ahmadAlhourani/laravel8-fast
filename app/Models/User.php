<?php

namespace App\Models;



use App\Models\Directory\Country;
use App\Models\Directory\Lang;
use Doctrine\DBAL\Exception;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\HasApiTokens;
use Modules\AccountManager\Entities\UserDevice;

use Modules\AccountManager\Entities\UserVerification;
use App\Models\Directory\Log;
use Modules\Utility\Repositories\LogRepository;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Traits\HasPermissions;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version July 24, 2021, 2:31 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $phone
 */
class User extends Authenticatable
{
    use SoftDeletes, HasRoles
        , HasPermissions
        ,HasFactory, Notifiable
        ,HasApiTokens;

    //User  user-type
    public const ADMIN_USER_TYPE=1;
    public const PARTNER_USER_TYPE=2;
    public const NORMAL_USER_TYPE=3;

    //User Status
    public const GUEST_USER= 0;
    public const LOGGED_USER = 1;

    //User types
    public const EMAIL_TYPE= 1;
    public const PHONE_TYPE= 2;


    //User verified
    public const VERIFIED= 1;
    public const NON_VERIFIED= 0;

    public $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'social_id',
        'password',
        'status',
        'email_verified_at',
        'phone_verified_at',
        'type',
        'verified',
        'lang_id',
        'country_id',
        'gender',
        'birthday',
        'image_url',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|max:255|unique:users,email',
        'phone' => 'required|max:255|unique:users',
        'password' => 'required'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function isSuperAdmin()
    {
        return $this->hasRole(Role::SUPPER_ADMIN);
    }
    protected $appends = array('role_data', 'role_text', 'status_online', 'is_online');

    public function getRoleDataAttribute()
    {
        return $this->roles->pluck('id', 'name');
    }
    public function getRoleTextAttribute()
    {
        return $this->roles->pluck('name')->implode(',');
    }
    public function getStatusOnlineAttribute()
    {
        return $this->is_online ? "online" : "";
    }
    public function getIsOnlineAttribute()
    {
        $checkTime = $this->freshTimestamp()->copy()->subMinutes(config('fast.time_checkout'));
        return $this->attendances()->where('present', 1)->where('time_out', '>=', $checkTime)->exists();
    }
    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

    public function checkIn()
    {
        $now = $this->freshTimestamp();
        $attendances = $this->attendances()
            ->where('day', $now->format('Y-m-d'))
            ->where('present', 1)
            ->first();
        if ($attendances != null && $attendances->time_out > $now->copy()->subMinutes(config('fast.time_checkout'))) {
            $attendances->update([
                'time_out' => $now,
                'present' => 1
            ]);
            return $attendances;
        } else {
            $this->attendances()
                //->where('day', $now->format('Y-m-d'))
                ->where('present', 1)->update([
                    'present' => 0,
                ]);
        }

        return $this->attendances()->create([
            'day' => $now->format('Y-m-d'),
            'time_in' => $now,
            'ip' => request()->ip(),
            'present' => 1
        ]);
    }

    public function checkOut($present = 1)
    {
        $now = $this->freshTimestamp();
        $date = request()->session()->get(config('fast.key_checkin'));
        if ($date == null) {
            $date = $now;
        }
        $attendances = $this->attendances()
            ->where('day', $date->format('Y-m-d'))
            ->where('present', 1)
            ->where('ip', request()->ip())
            //->whereNull('time_out')
            ->first();
        if ($attendances == null) {
            $attendances = $this->checkIn();
        }
        return $attendances->update([
            'time_out' => $now,
            'present' => $present
        ]);
    }

    public function devices()
    {
        return $this->belongsToMany(UserDevice::class);

    }

    public function user_devices()
    {
        return $this->hasMany(UserDevice::class,);
    }
    public function lang(){
        return $this->belongsTo(Lang::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function checkUserPermission($guardName="api"){

        switch ($guardName){
            case "api":
                $access= $this->hasWildcardPermission(Route::current()->uri(),$guardName);
                if(!$access){
                    Log::saveLog(['message' => "Access Denied","user"=>$this,"line"=>__LINE__]
                        ,get_class($this),__FUNCTION__);
//                    return response()->json("Access Denied", 200);
                    return false;
                    throw new Exception('Access Denied');

                }
                break;

            case "web":
                if($this->isSuperAdmin())
                    return true;

                $access= $this->hasWildcardPermission(Route::getCurrentRoute()->action["as"],$guardName);
                if(!$access){
                    Log::saveLog(['message' => "Access Denied","user"=>$this,"line"=>__LINE__]
                        ,get_class($this),__FUNCTION__);
                    return false;
                    throw new Exception('Access Denied');

                }
                break;

        }


        return true;
    }


    public function user_verifications()
    {
        return $this->hasMany(UserVerification::class);
    }

    public static function generateCode()
    {
        $generator = "1357902468";
        $result = "";
        for ($i = 1; $i <= 6; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
        return $result;
    }

    function defaultLang(){
        return $this->lang_id??1;
    }

    static function sendEmail($mail, $message, $type, $subject)
    {
        try {
            Mail::to($mail)->send(new \App\Mail\SendMail($message, $type, $subject));
            if (count(Mail::failures()) > 0) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            $log = \Modules\Directory\Entities\Log::saveLog(['message' => $e->getMessage()], __CLASS__, __FUNCTION__);
            return true;
        }
    }


    /**
     * @param  $user
     * @return bool
     */
    function cloneTo( $user){

        //clone user devices

        foreach (  $this->user_devices()->get() as $userDevice){
            try{
                $user->user_devices()->create(["device_id"=>$userDevice->device_id]);
            }
            catch (\Illuminate\Database\QueryException $e) {
                Log::saveLog(['message' =>  $e->getMessage(),"user"=>$this,"line"=>__LINE__], __CLASS__, __FUNCTION__);

            }

        }

        //clone user etc


        return true;
    }

    function test()
    {
        return $this->user_devices()->get();

    }
}
