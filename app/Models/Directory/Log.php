<?php

namespace App\Models\Directory;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Log
 * @package App\Models\Directory
 * @version October 22, 2021, 6:00 pm +07
 *
 * @property string $msg
 * @property string $module
 * @property string $method
 */
class Log extends Model
{
    use SoftDeletes;


    public $table = 'logs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'msg',
        'module',
        'method'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'msg' => 'string',
        'module' => 'string',
        'method' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function saveLog($message, $module = 'database', $method = 'create')
    {
        return self::create(['msg' => json_encode($message), 'module' => $module, 'method' => $method]);
    }
}
