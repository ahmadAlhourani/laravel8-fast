<?php

namespace App\Models\Directory;

use Eloquent as Model;



/**
 * Class Country
 * @package App\Models\Directory
 * @version October 16, 2021, 2:46 pm +07
 *
 * @property \App\Models\Directory\User $country
 * @property string $iso
 * @property string $name
 * @property string $nicename
 * @property string $iso3
 * @property string $numcode
 * @property string $phonecode
 */
class Country extends Model
{


    public $table = 'countries';




    public $fillable = [
        'iso',
        'name',
        'nice_name',
        'iso3',
        'num_code',
        'phone_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'iso' => 'string',
        'name' => 'string',
        'nice_name' => 'string',
        'iso3' => 'string',
        'num_code' => 'string',
        'phone_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     **/
//    public function country()
//    {
//        return $this->belongsTo(\App\Models\Directory\User::class, 'country_id', 'id');
//    }
}
