<?php

namespace App\Models\Directory;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class AlertLang
 * @package App\Models\Directory
 * @version October 12, 2021, 1:32 pm +07
 *
 * @property integer $alert_id
 * @property integer $lang_id
 * @property string $name
 */
class AlertLang extends Model
{
    use SoftDeletes;


    public $table = 'alert_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'alert_id',
        'lang_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'alert_id' => 'integer',
        'lang_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function Lang (){
        return $this->belongsTo(Lang::class,"lang_id");
    }
    public function Alert (){
        return $this->belongsTo(Alert::class,"alert_id");
    }


}
