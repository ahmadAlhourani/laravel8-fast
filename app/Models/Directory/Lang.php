<?php

namespace App\Models\Directory;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Lang
 * @package App\Models\Directory
 * @version October 11, 2021, 8:40 pm +07
 *
 * @property string $name
 * @property string $iso
 */
class Lang extends Model
{
    use SoftDeletes;

    public const ENGLISH_IOS="en";
    public const ARABIC_IOS="ar";


    public $table = 'langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'iso'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'iso' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
