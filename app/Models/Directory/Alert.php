<?php

namespace App\Models\Directory;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpseclib3\Math\PrimeField\Integer;


/**
 * Class Alert
 * @package App\Models\Directory
 * @version October 11, 2021, 8:48 pm +07
 *
 * @property integer $state
 * @property string $module
 */
class Alert extends Model
{
    use SoftDeletes;


    public $table = 'alerts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'state',
        'module',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'state' => 'integer',
        'module' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function langs()
    {
        return $this->belongsToMany(Lang::class, 'alert_langs')
            ->withPivot('name');
    }

    public function alertLang()
    {
        return $this->hasMany(AlertLang::class);

    }





}
