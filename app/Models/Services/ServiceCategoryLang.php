<?php

namespace App\Models\Services;

use App\Models\Directory\Lang;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ServiceCategoryLang
 * @package App\Models\Services
 * @version October 23, 2021, 5:10 am +07
 *
 * @property integer $lang_id
 * @property string $name
 * @property string $description
 */
class ServiceCategoryLang extends Model
{
    use SoftDeletes;


    public $table = 'service_category_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service_category_id',
        'lang_id',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_category_id' => 'integer',
        'lang_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function Lang (){
        return $this->belongsTo(Lang::class,"lang_id");
    }

    public function service_category (){
        return $this->belongsTo(ServiceCategory::class);
    }





}
