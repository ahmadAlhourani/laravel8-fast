<?php

namespace App\Models\Services;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


/**
 * Class ServiceType
 * @package App\Models\Services
 * @version October 23, 2021, 1:02 am +07
 *
 * @property integer $service_id
 * @property string $name
 * @property string $icon
 * @property integer $rank
 * @property number $price
 * @property string $short_description
 * @property string $description
 * @property string $html_description
 */
class ServiceType extends Model
{
    use SoftDeletes;


    public $table = 'service_types';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service_id',
        'name',
        'icon',
        'rank',
        'status',
        'price_type',
        'show_price',
        'price',
        'short_description',
        'description',
        'html_description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_id' => 'integer',
        'name' => 'string',
        'rank' => 'integer',
        'price' => 'double',
        'status' => 'integer'
    ];

    protected $appends = array('current_service_type_lang','service_type_questions');

    public function getCurrentServiceTypeLangAttribute()
    {
        return $this->current_service_type_lang()->get();
    }

    public function getServiceTypeQuestionsAttribute()
    {
        return $this->service_type_questions()->get();
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public function service (){
        return $this->belongsTo(Service::class);
    }

    public function current_service_type_lang()
    {
        $user = Auth::user();
        return $this->hasMany(ServiceTypeLang::class, 'service_type_id', 'id')->where("lang_id",$user->defaultLang());

    }

    public function service_type_questions()
    {
        return $this->hasMany(ServiceTypeQuestion::class, 'service_type_id', 'id');

    }

}
