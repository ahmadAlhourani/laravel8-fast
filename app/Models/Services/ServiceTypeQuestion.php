<?php

namespace App\Models\Services;

use App\Models\Questions\QuestionType;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


/**
 * Class ServiceTypeQuestion
 * @package App\Models\Services
 * @version October 25, 2021, 9:06 pm +07
 *
 * @property string $question
 * @property integer $service_type_id
 * @property integer $question_type_id
 * @property integer $required
 * @property string $description
 */
class ServiceTypeQuestion extends Model
{
    use SoftDeletes;


    public $table = 'service_type_questions';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'question',
        'service_type_id',
        'question_type_id',
        'required',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_type_id' => 'integer',
        'question_type_id' => 'integer',
        'required' => 'integer'
    ];

    protected $appends = array('current_service_type_question_lang','service_type_question_options');

    public function getCurrentServiceTypeQuestionLangAttribute()
    {
        return $this->current_service_type_question_lang()->get();
    }

    public function getServiceTypeQuestionOptionsAttribute()
    {
        return $this->service_type_question_options()->get();
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public function question_type(){
        return $this->belongsTo(QuestionType::class);
    }
    public function service_type (){
        return $this->belongsTo(ServiceType::class);
    }


    public function current_service_type_question_lang()
    {
        $user = Auth::user();
        return $this->hasMany(ServiceTypeQuestionLang::class, 'service_type_question_id', 'id')->where("lang_id",$user->defaultLang());

    }

    public function service_type_question_options()
    {
        return $this->hasMany(ServiceTypeQuestionOption::class, 'service_type_question_id', 'id');
    }

}
