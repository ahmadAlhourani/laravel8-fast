<?php

namespace App\Models\Services;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


/**
 * Class Service
 * @package App\Models\Services
 * @version October 23, 2021, 12:46 am +07
 *
 * @property \App\Models\Services\ServiceCategory $serviceCategory
 * @property \Illuminate\Database\Eloquent\Collection $serviceTypes
 * @property integer $name
 * @property integer $service_category_id
 * @property string $short_description
 * @property string $icon
 * @property integer $rank
 * @property string $description
 * @property string $html_description
 * @property string $question_service_type
 * @property string $types_difference
 */
class Service extends Model
{
    use SoftDeletes;


    public $table = 'services';


    protected $dates = ['deleted_at'];


    protected $appends = array('current_service_lang');

    public function getCurrentServiceLangAttribute()
    {
        return $this->current_service_lang()->get();
    }

    public $fillable = [
        'name',
        'service_category_id',
        'short_description',
        'icon',
        'rank',
        'status',
        'description',
        'html_description',
        'question_service_type',
        'types_difference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'service_category_id' => 'integer',
        'rank' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];




    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function serviceTypes()
    {
        return $this->hasMany(\App\Models\Services\ServiceType::class, 'service_id', 'id');
    }


    public function current_service_lang()
    {
        $user = Auth::user();
        return $this->hasMany(ServiceLang::class, 'service_id', 'id')->where("lang_id",$user->defaultLang());

    }
        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function service_category (){
        return $this->belongsTo(ServiceCategory::class);
    }
}
