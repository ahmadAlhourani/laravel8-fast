<?php

namespace App\Models\Services;

use App\Models\Questions\QuestionTypeOption;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ServiceTypeQuestionOption
 * @package App\Models\Services
 * @version October 25, 2021, 9:11 pm +07
 *
 * @property integer $service_type_question_id
 * @property integer $question_type_option_id
 * @property string $description
 */
class ServiceTypeQuestionOption extends Model
{
    use SoftDeletes;


    public $table = 'service_type_question_options';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service_type_question_id',
        'question_type_option_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_type_question_id' => 'integer',
        'question_type_option_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected $appends = array('question_type_option');
    public function getQuestionTypeOptionAttribute()
    {
        return $this->question_type_option()->first()??null;
    }
    public function service_type_question(){
        return $this->belongsTo(ServiceTypeQuestion::class);
    }

    public function question_type_option(){
        return $this->belongsTo(QuestionTypeOption::class);
    }
}
