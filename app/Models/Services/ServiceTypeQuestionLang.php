<?php

namespace App\Models\Services;

use App\Models\Directory\Lang;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ServiceTypeQuestionLang
 * @package App\Models\Services
 * @version October 25, 2021, 9:09 pm +07
 *
 * @property string $question
 * @property integer $service_type_question_id
 * @property integer $lang_id
 * @property string $note
 * @property string $answer_example
 * @property string $instructions
 * @property string $default_answer
 */
class ServiceTypeQuestionLang extends Model
{
    use SoftDeletes;


    public $table = 'service_type_question_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'question',
        'service_type_question_id',
        'lang_id',
        'note',
        'answer_example',
        'instructions',
        'default_answer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_type_question_id' => 'integer',
        'lang_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function lang(){
        return $this->belongsTo(Lang::class);
    }

    public function service_type_question(){
        return $this->belongsTo(ServiceTypeQuestion::class);
    }
}
