<?php

namespace App\Models\Services;

use App\Models\Directory\Lang;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ServiceLang
 * @package App\Models\Services
 * @version October 23, 2021, 5:27 am +07
 *
 * @property integer $service_id
 * @property integer $lang_id
 * @property integer $name
 * @property integer $short_description
 * @property integer $description
 * @property integer $html_description
 * @property integer $question_service_type
 * @property integer $types_difference
 */
class ServiceLang extends Model
{
    use SoftDeletes;


    public $table = 'service_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service_id',
        'lang_id',
        'name',
        'short_description',
        'description',
        'html_description',
        'question_service_type',
        'types_difference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_id' => 'string',
        'lang_id' => 'integer',
        'name' => 'string',
        'short_description' => 'string',
        'description' => 'string',
        'html_description' => 'string',
        'question_service_type' => 'string',
        'types_difference' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public function Lang (){
        return $this->belongsTo(Lang::class,"lang_id");
    }

    public function service (){
        return $this->belongsTo(Service::class);
    }

}
