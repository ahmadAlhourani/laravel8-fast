<?php

namespace App\Models\Services;

use App\Models\Directory\Lang;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ServiceTypeLang
 * @package App\Models\Services
 * @version October 23, 2021, 5:33 am +07
 *
 * @property integer $service_type_id
 * @property integer $lang_id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property string $html_description
 */
class ServiceTypeLang extends Model
{
    use SoftDeletes;


    public $table = 'service_type_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'service_type_id',
        'lang_id',
        'name',
        'short_description',
        'description',
        'html_description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_type_id' => 'integer',
        'lang_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public function Lang (){
        return $this->belongsTo(Lang::class,"lang_id");
    }

    public function service_type (){
        return $this->belongsTo(ServiceType::class);
    }

}
