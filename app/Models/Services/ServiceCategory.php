<?php

namespace App\Models\Services;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


/**
 * Class ServiceCategory
 * @package App\Models\Services
 * @version October 23, 2021, 12:26 am +07
 *
 * @property \Illuminate\Database\Eloquent\Collection $services
 * @property string $name
 * @property integer $description
 * @property string $icon
 * @property integer $rank
 */
class ServiceCategory extends Model
{
    use SoftDeletes;


    public $table = 'service_categories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'icon',
        'status',
        'rank'
    ];

    protected $appends = array('current_service_category_lang');

    public function getCurrentServiceCategoryLangAttribute()
    {
        return $this->current_service_category_lang()->get();
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'rank' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function services()
    {
        return $this->hasMany(Service::class, 'service_category_id', 'id');
    }

    public function active_services()
    {
        return $this->hasMany(Service::class, 'service_category_id', 'id')->where("status",1)->orderby('rank','desc');
    }

    public function service_category_langs()
    {
        return $this->hasMany(ServiceCategoryLang::class, 'service_category_id', 'id');
    }

    public function current_service_category_lang()
    {
        $user = Auth::user();
        return $this->hasMany(ServiceCategoryLang::class, 'service_category_id', 'id')->where("lang_id",$user->defaultLang());

    }


    protected static function boot()
    {
        parent::boot();

        static::deleting(function($telco) {
            $relationMethods = ['services'];

            foreach ($relationMethods as $relationMethod) {
                if ($telco->$relationMethod()->count() > 0) {
                    return false;
                }
            }
        });
    }
}
