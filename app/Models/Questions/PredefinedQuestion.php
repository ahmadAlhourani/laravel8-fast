<?php

namespace App\Models\Questions;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PredefinedQuestion
 * @package App\Models\Questions
 * @version October 25, 2021, 8:47 pm +07
 *
 * @property integer $question_type_id
 * @property string $question
 * @property string $description
 */
class PredefinedQuestion extends Model
{
    use SoftDeletes;


    public $table = 'predefined_questions';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'question_type_id',
        'question',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'question_type_id' => 'integer',
        'question' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function question_type(){
        return $this->belongsTo(QuestionType::class);
    }
}
