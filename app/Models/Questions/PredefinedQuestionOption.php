<?php

namespace App\Models\Questions;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PredefinedQuestionOption
 * @package App\Models\Questions
 * @version October 25, 2021, 9:00 pm +07
 *
 * @property integer $predefined_question_id
 * @property integer $question_type_option_id
 * @property string $description
 */
class PredefinedQuestionOption extends Model
{
    use SoftDeletes;


    public $table = 'predefined_question_options';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'predefined_question_id',
        'question_type_option_id',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'predefined_question_id' => 'integer',
        'question_type_option_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function question_type_option(){
        return $this->belongsTo(QuestionTypeOption::class);
    }

    public function predefined_question(){
        return $this->belongsTo(PredefinedQuestion::class);
    }

}
