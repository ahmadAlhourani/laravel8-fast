<?php

namespace App\Models\Questions;

use App\Models\Directory\Lang;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PredefinedQuestionLang
 * @package App\Models\Questions
 * @version October 25, 2021, 8:58 pm +07
 *
 * @property integer $predefined_question_id
 * @property integer $lang_id
 * @property string $question
 * @property string $note
 * @property string $answer_example
 * @property string $instructions
 * @property string $default_answer
 */
class PredefinedQuestionLang extends Model
{
    use SoftDeletes;


    public $table = 'predefined_question_langs';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'predefined_question_id',
        'lang_id',
        'question',
        'note',
        'answer_example',
        'instructions',
        'default_answer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'predefined_question_id' => 'integer',
        'lang_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public function predefined_question(){
        return $this->belongsTo(PredefinedQuestion::class);
    }
    public function Lang (){
        return $this->belongsTo(Lang::class,"lang_id");
    }
}
