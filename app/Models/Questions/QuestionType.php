<?php

namespace App\Models\Questions;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class QuestionType
 * @package App\Models\Questions
 * @version October 25, 2021, 8:39 pm +07
 *
 * @property string $key
 * @property string $name
 * @property string $description
 */
class QuestionType extends Model
{
    use SoftDeletes;


    public $table = 'question_types';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'key',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
