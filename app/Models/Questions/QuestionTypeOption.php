<?php

namespace App\Models\Questions;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class QuestionTypeOption
 * @package App\Models\Questions
 * @version October 25, 2021, 8:44 pm +07
 *
 * @property integer $question_type_id
 * @property string $option
 * @property string $description
 */
class QuestionTypeOption extends Model
{
    use SoftDeletes;


    public $table = 'question_type_options';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'question_type_id',
        'option',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'question_type_id' => 'integer',
        'option' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    protected $appends = array('question_type');
    public function getQuestionTypeAttribute()
    {
        return $this->question_type()->first()??null;
    }
    public function question_type(){
        return $this->belongsTo(QuestionType::class);
    }



}
