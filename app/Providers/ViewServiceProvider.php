<?php

namespace App\Providers;

use App\Models\Directory\Alert;
use App\Models\Directory\Lang;


//use Illuminate\Support\ServiceProvider;
use App\Models\Questions\PredefinedQuestion;
use App\Models\Questions\QuestionType;
use App\Models\Questions\QuestionTypeOption;
use App\Models\Services\Service;
use App\Models\Services\ServiceCategory;
use App\Models\Services\ServiceType;
use App\Models\Services\ServiceTypeQuestion;
use App\Models\Settings\SettingCategory;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

//use Illuminate\View\ViewServiceProvider as ServiceProvider;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        die();
        //all
        View::composer(['*'], function ($view) {
            $langItems = Lang::pluck('name','id')->toArray();
            $view->with('langItems', $langItems);
        });


        //service_category_langs
        View::composer(['services.service_category_langs.fields'], function ($view) {
            $serviceCategoryItems = ServiceCategory::pluck('name','id')->toArray();
            $view->with('serviceCategoryItems', $serviceCategoryItems?? '');
        });

        //services
        View::composer(['services.services.fields'], function ($view) {
            $serviceCategoryItems = ServiceCategory::pluck('name','id')->toArray();
            $view->with('serviceCategoryItems', $serviceCategoryItems?? '');
        });

        //service_langs
        View::composer(['services.service_langs.fields'], function ($view) {
            $serviceItems = Service::pluck('name','id')->toArray();
            $view->with('serviceItems', $serviceItems?? '');
        });

        //service_types
        View::composer(['services.service_types.fields'], function ($view) {
            $serviceItems = Service::pluck('name','id')->toArray();
            $view->with('serviceItems', $serviceItems?? '');
        });
        //service_type_langs
        View::composer(['services.service_type_langs.fields'], function ($view) {
            $serviceTypeItems = ServiceType::pluck('name','id')->toArray();
            $view->with('serviceTypeItems', $serviceTypeItems?? '');
        });


        //service_type_questions
        View::composer(['services.service_type_questions.fields'], function ($view) {
            $serviceTypeItems = ServiceType::pluck('name','id')->toArray();
            $questionTypeItems = QuestionType::pluck('name','id')->toArray();
            $view->with('serviceTypeItems', $serviceTypeItems?? '')->with('questionTypeItems', $questionTypeItems?? '');
        });

        //service_type_question_lang
        View::composer(['services.service_type_question_langs.fields'], function ($view) {
            $serviceTypeQuestionItems = ServiceTypeQuestion::pluck('question','id')->toArray();
            $view->with('serviceTypeQuestionItems', $serviceTypeQuestionItems?? '');
        });

        //service_type_question_options
        View::composer(['services.service_type_question_options.fields'], function ($view) {
            $serviceTypeQuestionItems = ServiceTypeQuestion::pluck('question','id')->toArray();
            $questionTypeOptionItems = QuestionTypeOption::pluck('option','id')->toArray();

            $view->with('serviceTypeQuestionItems', $serviceTypeQuestionItems?? '')->with('questionTypeOptionItems', $questionTypeOptionItems?? '');
        });


        //question_type_options,predefined_questions
        View::composer(['questions.question_type_options.fields','questions.predefined_questions.fields'], function ($view) {
            $questionTypeItems = QuestionType::pluck('name','id')->toArray();
            $view->with('questionTypeItems', $questionTypeItems?? '');
        });


        //predefined_question_options
        View::composer(['questions.predefined_question_options.fields'], function ($view) {
            $predefinedَQuestionItems = PredefinedQuestion::pluck('question','id')->toArray();
            $questionTypeOptionItems = QuestionTypeOption::pluck('option','id')->toArray();
            $view->with('predefinedَQuestionItems', $predefinedَQuestionItems?? '')->with('questionTypeOptionItems', $questionTypeOptionItems?? '');
        });

        //predefined_question_options
        View::composer(['questions.predefined_question_langs.fields'], function ($view) {
            $predefinedَQuestionItems = PredefinedQuestion::pluck('question','id')->toArray();

            $view->with('predefinedَQuestionItems', $predefinedَQuestionItems?? '');
        });
        View::composer(['settings.settings.fields'], function ($view) {
            $settingCategoryItems = SettingCategory::pluck('name','id')->toArray();
            $view->with('settingCategoryItems', $settingCategoryItems?? '');
        });
        View::composer(['directory.alert_langs.fields'], function ($view) {
            $langItems = Lang::pluck('name', 'id')->toArray();
            $alertItems = Alert::pluck('state', 'id')->toArray();

            $view->with('langItems', $langItems ?? '')->with('alertItems', $alertItems ?? '');
        });
        View::composer(['tests.fields'], function ($view) {
            $langItems = Lang::pluck('name')->toArray();
            $view->with('langItems', $langItems);
        });
//        View::composer(['tests.fields'], function ($view) {
//            $langItems = Lang::pluck('name')->toArray();
//            $view->with('langItems', $langItems);
//        });
        //
    }
}
