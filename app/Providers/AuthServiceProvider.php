<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Gate;
use App\Models\Role;
use Laravel\Passport\Passport;
use phpDocumentor\Reflection\Types\True_;
use PhpOffice\PhpSpreadsheet\Calculation\Financial\TreasuryBill;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
//        die();
//        var_dump("{}");
//        var_dump($this->app->routesAreCached());
        $this->registerPolicies($gate);
//        Passport::routes(); // Add this
        if (! $this->app->routesAreCached()) {
            Passport::routes();
        }
        $guest_perrmission = config('fast.guest_perrmission');
        Gate::before(function ($user, $ability) use ($guest_perrmission) {

//            var_dump($user);
//            var_dump($ability);
            if (in_array($ability, $guest_perrmission))
                return true;

            if ($user->isSuperAdmin())
                return true;

            return null;
        });
    }
}
