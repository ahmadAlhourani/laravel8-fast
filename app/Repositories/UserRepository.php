<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Directory\LangRepository;
use Illuminate\Container\Container as Application;
use Illuminate\Support\Facades\Hash;
use Modules\AccountManager\Repositories\UserDeviceRepository;
use Modules\AccountManager\Repositories\UserPasswordRepository;


class UserRepository extends BaseRepository
{
    /** @var  UserPasswordRepository */
    private $userPasswordRepository;

    /** @var  UserDeviceRepository */
    private $userDeviceRepository;
    /** @var  LangRepository */
    private $langRepository;
    public function __construct(Application $app,UserPasswordRepository $userPasswordRepository,
                                UserDeviceRepository $userDeviceRepository,LangRepository $langRepository)
    {
        parent::__construct($app);
        $this->userPasswordRepository = $userPasswordRepository;
        $this->userDeviceRepository = $userDeviceRepository;
        $this->langRepository = $langRepository;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'phone',
        'social_id',
        'password',
        'status',
        'email_verified_at',
        'phone_verified_at',
        'type',
        'verified',
        'lang_id',
        'country_id',
        'gender',
        'birthday',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function create($input)
    {
        $originPass = $input["password"];
        $input["password"] = Hash::make($originPass);

        //custom default lang
        if(!isset($input["lang_id"])){
            $defaultLang=$this->langRepository->getLangByISO();
            $input["lang_id"]=$defaultLang->id;
        }

        if(!isset($input["user_type"])){
            $input["user_type"]=User::NORMAL_USER_TYPE;
        }
        $user = parent::create($input);

        //trigger user Password
        $this->userPasswordRepository->create(
            [
                "user_id"=>$user->id,
                "password"=>$originPass
            ]
        );

        return $user;
    }


    /**
     * @param array $data
     * @return
     */
    public  function assignGuestUserToDevice(int $deviceId)
    {
        try {

            $guestUser = $this->model->with('user_devices')->where("status",User::GUEST_USER)->whereHas('user_devices',function($query)use($deviceId){
                $query->where('device_id',$deviceId);
            })->first();


            if ($guestUser) {
                return $guestUser;
            }
            return $this->createGuestUserToDevice($deviceId);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }


    public  function createGuestUserToDevice(int $deviceId)
    {
        try {
            $allUsers=$this->model->count()+1;
            $gustUser = $this->create([
                'name' => 'Guest',
                'email' => $deviceId.'guest'.$allUsers.'@test.com',
                'status' => User::GUEST_USER,
                'password' => '123@12',
            ]);


            $this->userDeviceRepository->create(
                [
                    "user_id"=>$gustUser->id,
                    "device_id"=>$deviceId
                ]
            );

           return $gustUser;
        } catch (\Exception $ex) {
//            return $ex->getMessage();
            return null;
        }
    }


}
