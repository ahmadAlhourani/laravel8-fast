<?php

namespace App\Repositories\Settings;

use App\Models\Settings\SettingCategory;
use App\Repositories\BaseRepository;

/**
 * Class SettingCategoryRepository
 * @package App\Repositories\Settings
 * @version October 21, 2021, 6:09 pm +07
*/

class SettingCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'value',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SettingCategory::class;
    }
}
