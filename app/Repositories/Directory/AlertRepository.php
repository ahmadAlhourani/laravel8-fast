<?php

namespace App\Repositories\Directory;

use App\Models\Directory\Alert;
use App\Repositories\BaseRepository;
use Illuminate\Container\Container as Application;
/**
 * Class AlertRepository
 * @package App\Repositories\Directory
 * @version October 11, 2021, 8:48 pm +07
*/

class AlertRepository extends BaseRepository
{
    /** @var  AlertLangRepository */
    private $alertLangRepository;
    public function __construct(Application $app,AlertLangRepository $alertLangRepository
                         )
    {
        parent::__construct($app);
        $this->alertLangRepository = $alertLangRepository;

    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'state',
        'module',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Alert::class;
    }


    /**
     * @param array $input
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(  $input)
    {



        //alert langs
        if(isset($input["alert_langs"])&&$input["alert_langs"]){
            $alertLangs=$input["alert_langs"];
            unset($input["alert_langs"]);
            $alert = parent::create($input);

            foreach ($alertLangs as $alertLang){
                $alert->alertLang()->create($alertLang);
            }

            return $alert;
        }
        return parent::create($input);

    }
}
