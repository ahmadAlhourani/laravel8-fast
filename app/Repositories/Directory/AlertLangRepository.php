<?php

namespace App\Repositories\Directory;

use App\Models\Directory\AlertLang;
use App\Repositories\BaseRepository;

/**
 * Class AlertLangRepository
 * @package App\Repositories\Directory
 * @version October 12, 2021, 1:32 pm +07
*/

class AlertLangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'alert_id',
        'lang_id',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AlertLang::class;
    }
}
