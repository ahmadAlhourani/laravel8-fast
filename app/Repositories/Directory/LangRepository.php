<?php

namespace App\Repositories\Directory;

use App\Models\Directory\Lang;
use App\Repositories\BaseRepository;

/**
 * Class LangRepository
 * @package App\Repositories\Directory
 * @version October 11, 2021, 8:40 pm +07
*/

class LangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'iso'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Lang::class;
    }

    public function getLangByISO($langISO=Lang::ENGLISH_IOS){
        return $this->model->where('iso', $langISO)->first();
    }
}
