<?php

namespace App\Repositories\Directory;

use App\Models\Directory\Log;
use App\Repositories\BaseRepository;

/**
 * Class LogRepository
 * @package App\Repositories\Directory
 * @version October 22, 2021, 6:00 pm +07
*/

class LogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'msg',
        'module',
        'method'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Log::class;
    }
}
