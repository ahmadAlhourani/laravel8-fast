<?php

namespace App\Repositories\Questions;

use App\Models\Questions\QuestionType;
use App\Repositories\BaseRepository;

/**
 * Class QuestionTypeRepository
 * @package App\Repositories\Questions
 * @version October 25, 2021, 8:39 pm +07
*/

class QuestionTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QuestionType::class;
    }
}
