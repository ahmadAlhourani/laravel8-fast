<?php

namespace App\Repositories\Questions;

use App\Models\Questions\PredefinedQuestionLang;
use App\Repositories\BaseRepository;

/**
 * Class PredefinedQuestionLangRepository
 * @package App\Repositories\Questions
 * @version October 25, 2021, 8:58 pm +07
*/

class PredefinedQuestionLangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'predefined_question_id',
        'lang_id',
        'question',
        'note',
        'answer_example',
        'instructions',
        'default_answer'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PredefinedQuestionLang::class;
    }
}
