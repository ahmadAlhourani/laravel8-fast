<?php

namespace App\Repositories\Questions;

use App\Models\Questions\PredefinedQuestionOption;
use App\Repositories\BaseRepository;

/**
 * Class PredefinedQuestionOptionRepository
 * @package App\Repositories\Questions
 * @version October 25, 2021, 9:00 pm +07
*/

class PredefinedQuestionOptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'predefined_question_id',
        'question_type_option_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PredefinedQuestionOption::class;
    }
}
