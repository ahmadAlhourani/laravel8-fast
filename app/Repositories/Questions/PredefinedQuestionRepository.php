<?php

namespace App\Repositories\Questions;

use App\Models\Questions\PredefinedQuestion;
use App\Repositories\BaseRepository;

/**
 * Class PredefinedQuestionRepository
 * @package App\Repositories\Questions
 * @version October 25, 2021, 8:47 pm +07
*/

class PredefinedQuestionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question_type_id',
        'question',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PredefinedQuestion::class;
    }
}
