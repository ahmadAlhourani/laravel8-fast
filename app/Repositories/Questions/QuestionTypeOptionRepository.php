<?php

namespace App\Repositories\Questions;

use App\Models\Questions\QuestionTypeOption;
use App\Repositories\BaseRepository;

/**
 * Class QuestionTypeOptionRepository
 * @package App\Repositories\Questions
 * @version October 25, 2021, 8:44 pm +07
*/

class QuestionTypeOptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question_type_id',
        'option',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QuestionTypeOption::class;
    }
}
