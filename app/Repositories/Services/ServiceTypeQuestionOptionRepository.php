<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceTypeQuestionOption;
use App\Repositories\BaseRepository;

/**
 * Class ServiceTypeQuestionOptionRepository
 * @package App\Repositories\Services
 * @version October 25, 2021, 9:11 pm +07
*/

class ServiceTypeQuestionOptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service_type_question_id',
        'question_type_option_id',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceTypeQuestionOption::class;
    }
}
