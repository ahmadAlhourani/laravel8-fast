<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceTypeLang;
use App\Repositories\BaseRepository;

/**
 * Class ServiceTypeLangRepository
 * @package App\Repositories\Services
 * @version October 23, 2021, 5:33 am +07
*/

class ServiceTypeLangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service_type_id',
        'lang_id',
        'name',
        'short_description',
        'description',
        'html_description',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceTypeLang::class;
    }
}
