<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceTypeQuestion;
use App\Repositories\BaseRepository;

/**
 * Class ServiceTypeQuestionRepository
 * @package App\Repositories\Services
 * @version October 25, 2021, 9:06 pm +07
*/

class ServiceTypeQuestionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question',
        'service_type_id',
        'question_type_id',
        'required',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceTypeQuestion::class;
    }
}
