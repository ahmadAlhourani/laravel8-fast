<?php

namespace App\Repositories\Services;


use App\Models\Services\ServiceCategoryLang;
use App\Repositories\BaseRepository;

/**
 * Class ServiceCategoryLangRepository
 * @package App\Repositories\Services
 * @version October 23, 2021, 5:10 am +07
*/

class ServiceCategoryLangRepository extends BaseRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'lang_id',
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceCategoryLang::class;
    }
}
