<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceType;
use App\Repositories\BaseRepository;

/**
 * Class ServiceTypeRepository
 * @package App\Repositories\Services
 * @version October 23, 2021, 1:02 am +07
*/

class ServiceTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service_id',
        'name',
        'icon',
        'rank',
        'price',
        'short_description',
        'description',
        'html_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceType::class;
    }
}
