<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceLang;
use App\Repositories\BaseRepository;

/**
 * Class ServiceLangRepository
 * @package App\Repositories\Services
 * @version October 23, 2021, 5:27 am +07
*/

class ServiceLangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service_id',
        'lang_id',
        'name',
        'short_description',
        'description',
        'html_description',
        'question_service_type',
        'types_difference',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceLang::class;
    }
}
