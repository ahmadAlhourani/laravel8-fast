<?php

namespace App\Repositories\Services;

use App\Models\Services\ServiceTypeQuestionLang;
use App\Repositories\BaseRepository;

/**
 * Class ServiceTypeQuestionLangRepository
 * @package App\Repositories\Services
 * @version October 25, 2021, 9:09 pm +07
*/

class ServiceTypeQuestionLangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question',
        'service_type_question_id',
        'lang_id',
        'note',
        'answer_example',
        'instructions',
        'default_answer'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceTypeQuestionLang::class;
    }
}
