<?php

namespace App\Repositories\Services;

use App\Models\Services\Service;
use App\Repositories\BaseRepository;

/**
 * Class ServiceRepository
 * @package App\Repositories\Services
 * @version October 23, 2021, 12:46 am +07
*/

class ServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'service_category_id',
        'short_description',
        'icon',
        'rank',
        'description',
        'html_description',
        'question_service_type',
        'types_difference'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Service::class;
    }
}
